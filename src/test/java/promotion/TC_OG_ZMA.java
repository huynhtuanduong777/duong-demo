package promotion;


import apis.login.Authentication;
import apis.promotion.Promotion;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import runner.Runner;
import utils.XLSWorker;

import java.util.ArrayList;
import java.util.List;

public class TC_OG_ZMA extends BaseTest {
	public TC_OG_ZMA() {
		super.sheetName = "OG_ZMA";
	}
	int statusColorTab = 0;

	@Parameters("refs")
	@Test
	public void OG_ZMA(@Optional("All") String reference) {
		Authentication authen = new Authentication();
		Promotion promotion = new Promotion();
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
			if (reference.equals("All") || reference.equals(refs)) {
				String token = authen.getToken(i, sheetName);
				boolean result = false;
				String statuscode = "";
				String method = "";
				String endpoint = "";
				String expectResult = "";
				String header="";
				String excelBody = "";
				String response = "";
				statuscode = XLSWorker.getValueFromExcel(i, "StatusCode", sheetName);
				method = XLSWorker.getValueFromExcel(i, "Method", sheetName);
				endpoint = XLSWorker.getValueFromExcel(i, "EndPoint", sheetName);
				expectResult = XLSWorker.getValueFromExcel(i, "ExpectedResult", sheetName);
				excelBody = XLSWorker.getValueFromExcel(i, "Body", sheetName);
				header = XLSWorker.getValueFromExcel(i, "Header", sheetName);
				if (excelBody.contains("WAIT10")) {
					utils.Helper.sleep(10000);
				} else {
					// Nếu là phương thức GET
					if (method.equals("GET")) {
						// Lấy response
						response = promotion.getResponse(endpoint, method, token, "",header);
					}
					// Nếu là phương thức POST
					else {
						if(excelBody.contains("upfile")) {
							// Lấy response
							String[] fileUpload = excelBody.split(":");
							List<String> contents = new ArrayList<>();
							contents.add("START");
							contents.add("Content-Disposition: form-data; name=\"Type\"");
							contents.add("CRLF");
							contents.add("102");
							contents.add("START");
							contents.add("Content-Disposition: form-data; name=\"name\"");
							contents.add("CRLF");
							contents.add(fileUpload[1].trim());
							contents.add("START");
							contents.add("Content-Disposition: form-data; name=\"file\"; filename=\"" + fileUpload[1].trim() + "\"");
							contents.add("Content-Type: " + "multipart/form-data");
							contents.add("CRLF");
							contents.add("DATA");
							contents.add("CRLF");
							contents.add("END");
							response = promotion.uploadWithCustomHeader(fileUpload[1].trim(), "moxieboundary", contents, endpoint, header);
						}

						else
							response = promotion.getResponse(endpoint, method, token, excelBody,header);
					}

					// Nếu response trả về khác rỗng, khác null, thì kiểm tra status code và
					// expected Result
					if (response != null && !response.equals("")) {
						// Nếu status code khác rỗng, kiểm tra status code
						if (!statuscode.equals("")) {
							result = promotion.verifyResponseNew(statuscode);
							// Nếu expected Result khác rỗng, thì kiểm tra expected Result
							if (!expectResult.equals("") && result) {							
									result = promotion.verifyResponseNew(expectResult);
							}
						}
						// Nếu status code là rỗng thì lấy nguyên response so sánh với expected result
						else {
							result = promotion.verifyFullResponse(expectResult);
						}
					}

					if (result == false) {
						statusColorTab++;
						totalFailCase++;
						reportToExcel(result, i, refs, promotion.getResultMessage());

					} else {
						totalPassCase++;
						reportToExcel(result, i, refs, "");

					}
				}
			}
		}
		if (statusColorTab > 0) {
			XLSWorker.colorTabSheet(sheet, "red");
		} else {
			XLSWorker.colorTabSheet(sheet, "green");
		}			
	}

}
