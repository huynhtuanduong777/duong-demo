package promotion;


import apis.login.Authentication;
import apis.promotion.Promotion;
import object.BaseObject;
import object.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import runner.Runner;
import utils.XLSWorker;

public class TC_SaleEvent_Category extends BaseTest {
	public TC_SaleEvent_Category() {
		super.sheetName = "SaleEvent_Category";
	}
	int countFailCase = 0;
	int countPassCase = 0;
	@Parameters("refs")
	@Test
	public void promotionSaleEventCategory(@Optional("All") String reference) {
		Authentication authen = new Authentication();
		Promotion promotion = new Promotion();
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			String refs = XLSWorker.getValueFromExcel(i, "Refs", sheetName);
			if (reference.equals("All") || reference.equals(refs)) {
				String token = XLSWorker.getValueFromExcel(i, "Token", sheetName);
				boolean result = false;
				String statuscode = "";
				String method = "";
				String endpoint = "";
				String expectResult = "";
				// String expectResultExtend="";
				String excelBody = "";
				String response = "";
				statuscode = XLSWorker.getValueFromExcel(i, "StatusCode", sheetName);
				method = XLSWorker.getValueFromExcel(i, "Method", sheetName);
				endpoint = XLSWorker.getValueFromExcel(i, "EndPoint", sheetName);
				expectResult = XLSWorker.getValueFromExcel(i, "ExpectedResult", sheetName);
				excelBody = XLSWorker.getValueFromExcel(i, "Body", sheetName);
				if (excelBody.contains("WAIT5")) {
					utils.Helper.sleep(5000);				
				} else {
					// Nếu là phương thức GET
					if (method.equals("GET")) {
						// Lấy response
						response = promotion.getResponse(endpoint, method, token, "");
					}
					// Nếu là phương thức POST
					else {
						// Lấy response
						response = promotion.getResponse(endpoint, method, token, excelBody);
					}

					// Nếu response trả về khác rỗng, khác null, thì kiểm tra status code và
					// expected Result
					if (response != null && !response.equals("")) {
						// Nếu status code khác rỗng, kiểm tra status code
						if (!statuscode.equals("")) {
							result = promotion.verifyReponse(statuscode, statuscode);
							// Nếu expected Result khác rỗng, thì kiểm tra expected Result
							if (!expectResult.equals("") && result) {							
									result = promotion.verifyReponse(expectResult, expectResult);																							
							}
						}
						// Nếu status code là rỗng thì lấy nguyên response so sánh với expected result
						else {
							result = promotion.verifyFullResponse(expectResult);
						}
					}

					if (result == false) {
						countFailCase++;
						totalFailCase++;
						reportToExcel(result, i, refs, promotion.getResultMessage());
						reportToExcel2(result, i, promotion.getJsonInput(),
								BaseObject.keyObj + "= " + BaseObject.jsonActualResult);
//						reportJsonInputToExcel(method, result, i, promotion.getJsonInput());
					} else {
						countPassCase++;
						totalPassCase++;
						reportToExcel(result, i, refs, promotion.getResultMessage());
						reportToExcel2(result, i, promotion.getJsonInput(), "");
//						reportJsonInputToExcel(method, result, i, promotion.getJsonInput());
					}
				}
			}
		}
		if (countFailCase > 0) {
			XLSWorker.colorTabSheet(sheet, "red");
			Runner.setTotalFailSheet();
		} else {
			XLSWorker.colorTabSheet(sheet, "green");
			Runner.setTotalPassSheet();
		}			
	}

}
