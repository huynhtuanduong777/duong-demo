import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import freemarker.core.ReturnInstruction.Return;

public class test1 {

	private static final boolean JSONObject = false;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		JSONObject objA = new JSONObject(a);
//		JSONObject objB = new JSONObject(b);
//		JSONArray arrA = objA.getJSONObject("result").getJSONArray("categories");
//		JSONArray arrB = objB.getJSONObject("result").getJSONArray("categories");
//		boolean x = compareArr(arrA, arrB);
//		System.out.println(x);

		JSONObject objA = new JSONObject(test);
		JSONArray arrA = objA.getJSONArray("result");
		JSONArray a = returnSearch(arrA,"city","florida");
		System.out.println(a);
	}

	static String test = "{\"result\":[{\"name\":\"Minh\",\"city\":\"florida\",\"age\":\"22\"},{\"name\":\"Tuan\",\"city\":\"florida\",\"age\":\"35\"},{\"name\":\"Manh\",\"city\":\"vegas\",\"age\":\"18\"},{\"name\":\"Nam\",\"city\":\"Florida\",\"age\":\"19\"}]}";

	public static JSONArray returnSearch(JSONArray array,String searchKey, String searchValue) {
		JSONArray filtedArray = new JSONArray();
		for (int i = 0; i < array.length(); i++) {
			JSONObject obj = null;
			try {
				obj = array.getJSONObject(i);
				if (obj.getString(searchKey).equals(searchValue)) {
					filtedArray.put(obj);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
//		String result = filtedArray.toString();
		return filtedArray;
	}
	
	static String a ="{\"result\":{\"categories\":[{\"cate_path\":\"Thời Trang Nam  > Áo thun, áo polo nam  > Áo thun ngắn tay\",\"cate4\":310,\"description\":null,\"path\":\"1/2/94/95/310\",\"is_required_certificate_file\":false,\"senmall_description\":null,\"is_mall_restricted\":false},{\"cate_path\":\"Đồng Hồ  > Đồng hồ nữ  > Đồng hồ thời trang\",\"cate4\":1374,\"description\":null,\"path\":\"1/2/1366/1372/1374\",\"is_required_certificate_file\":false,\"senmall_description\":null,\"is_mall_restricted\":false},{\"cate_path\":\"Đồng Hồ  > Đồng hồ nam  > Đồng hồ kim\",\"cate4\":1370,\"description\":null,\"path\":\"1/2/1366/1367/1370\",\"is_required_certificate_file\":false,\"senmall_description\":null,\"is_mall_restricted\":false},{\"cate_path\":\"Giày Dép  > Giày sneaker/thể thao nam  > Giày sneaker/thể thao nam cổ thấp\",\"cate4\":4167,\"description\":\"Hướng dẫn shop đăng sản phẩm đúng ngành hàng: <a href=\\\"http://bit.ly/wikigiaydep\\\" target=\\\"_blank\\\" style=\\\"color: #35b8fe\\\">Xem tại đây</a>\",\"path\":\"1/2/1686/4166/4167\",\"is_required_certificate_file\":false,\"senmall_description\":null,\"is_mall_restricted\":false},{\"cate_path\":\"Thời Trang Nam  > Áo sơ mi nam  > Áo sơ mi nam họa tiết khác\",\"cate4\":2900,\"description\":null,\"path\":\"1/2/94/1666/2900\",\"is_required_certificate_file\":false,\"senmall_description\":null,\"is_mall_restricted\":false}]},\"success\":true,\"error\":null,\"statusCode\":200,\"totalRecords\":null,\"params\":null}";
	static String b ="{\"result\":{\"categories\":[{\"cate_path\":\"Thời Trang Nam  > Áo thun, áo polo nam  > Áo thun ngắn tay\",\"cate4\":310,\"description\":null,\"path\":\"1/2/94/95/310\",\"is_required_certificate_file\":false,\"senmall_description\":null,\"is_mall_restricted\":false},{\"cate_path\":\"Thời Trang Nam  > Áo sơ mi nam  > Áo sơ mi nam họa tiết khác\",\"cate4\":2900,\"description\":null,\"path\":\"1/2/94/1666/2900\",\"is_required_certificate_file\":false,\"senmall_description\":null,\"is_mall_restricted\":false},{\"cate_path\":\"Đồng Hồ  > Đồng hồ nam  > Đồng hồ kim\",\"cate4\":1370,\"description\":null,\"path\":\"1/2/1366/1367/1370\",\"is_required_certificate_file\":false,\"senmall_description\":null,\"is_mall_restricted\":false},{\"cate_path\":\"Đồng Hồ  > Đồng hồ nữ  > Đồng hồ thời trang\",\"cate4\":1374,\"description\":null,\"path\":\"1/2/1366/1372/1374\",\"is_required_certificate_file\":false,\"senmall_description\":null,\"is_mall_restricted\":false},{\"cate_path\":\"Giày Dép  > Giày sneaker/thể thao nam  > Giày sneaker/thể thao nam cổ thấp\",\"cate4\":4167,\"description\":\"Hướng dẫn shop đăng sản phẩm đúng ngành hàng: <a href=\\\"http://bit.ly/wikigiaydep\\\" target=\\\"_blank\\\" style=\\\"color: #35b8fe\\\">Xem tại đây</a>\",\"path\":\"1/2/1686/4166/4167\",\"is_required_certificate_file\":false,\"senmall_description\":null,\"is_mall_restricted\":false}]},\"success\":true,\"error\":null,\"statusCode\":200,\"totalRecords\":null,\"params\":null}";
	
	public static boolean compareArr(JSONArray arrayA, JSONArray arrayB) {
		boolean matches = true;

		for (int i=0;i<arrayA.length();i++)
		{
		   boolean matchesInternal = false;
		   
		   for (int j=0;j<arrayB.length();j++)
		   {
			  System.out.println("A "+arrayA.getJSONObject(i));
			  System.out.println("B "+arrayB.getJSONObject(j));
		      if (utils.JSON.areEqual(arrayB.getJSONObject(j), arrayA.getJSONObject(i)))
		      {
		          matchesInternal = true;
		          break;
		      }
		   }

		   if (!matchesInternal) {
		      matches = false;
		      break;
		   }
		}

		return matches;
		
	}
	
}
