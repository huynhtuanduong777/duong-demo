package logger;

import org.apache.logging.log4j.*;
import java.lang.invoke.MethodHandles;
import org.slf4j.LoggerFactory;
//import org.slf4j.Logger;

public class MyLogger{

    private static Logger logger = LogManager.getLogger(MyLogger.class.getName());
//    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static String failedMessage;
    private MyLogger(){
    }

    public static void info(String message){
        logger.info(message);
    }

    public static void warn(String message){
        logger.warn(message);
        failedMessage = message;
    }

    public static void error(String message){
        logger.error(message);
        failedMessage = message;
    }

    public static void error2(String message){
        logger.error(message);
    }

    public static void fatal(String message){
        logger.fatal(message);
    }

    public static void debug(String message){
        logger.debug(message);
    }

    public static void debug(Object message, Throwable t){
        logger.debug(message,t);
    }

    public static String getFailedMessage(){
        return failedMessage;
    }
}