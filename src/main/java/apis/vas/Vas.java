package apis.vas;

import object.BaseObject;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.APIController;
import utils.Constants;
import utils.XLSWorker;

import java.util.ArrayList;
import java.util.List;

public class Vas extends BaseObject {
    List<Integer> attributeIds;
    List<Integer> optionIds;

    public XSSFWorkbook header;

    public Vas(){
        header = XLSWorker.getInternalWorkbook(Constants.INPUT_PATH + "header_vas.xlsx");
        attributeIds = new ArrayList<>();
        optionIds = new ArrayList<>();
    }
}

