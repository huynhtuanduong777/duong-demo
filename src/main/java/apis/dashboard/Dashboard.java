package apis.dashboard;

import org.json.JSONArray;
import org.json.JSONObject;

import object.BaseObject;

public class Dashboard extends BaseObject {
	public int getRevenueSKU(String response, String typeResult) {
		JSONArray revenue_categories = new JSONObject(response).getJSONObject("result").getJSONArray("statistics");	
		JSONArray data = null;
		switch (typeResult) {
		case "categories":
			data = revenue_categories.getJSONObject(0).getJSONArray("data");
			break;
		case "sources":
			data = revenue_categories.getJSONObject(1).getJSONArray("data");
			break;
		case "promotions":
			data = revenue_categories.getJSONObject(2).getJSONArray("data");
			break;			
		default:
			break;
		}
				
		int totalRevenue = 0;
		for (Object i : data) {
			JSONObject itemArr = (JSONObject) i;
			int revenue = itemArr.getInt("revenue");
			totalRevenue = totalRevenue+revenue;
		}
		return totalRevenue;
	}
	
	// Report Revenue Mobile/Web
	public String getRevenueOrder(String response, String typeRevenue) {
		JSONObject revenue_statistic = new JSONObject(response).getJSONObject("result").getJSONObject("revenue_order_statistic");
		JSONObject detailRevenue = revenue_statistic.getJSONObject(typeRevenue);
		return detailRevenue.toString();
	}
}
