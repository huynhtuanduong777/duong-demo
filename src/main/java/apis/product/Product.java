package apis.product;

import object.BaseObject;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.APIController;
import utils.Constants;
import utils.XLSWorker;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Product extends BaseObject {
    List<Integer> attributeIds;
    List<Integer> optionIds;
    int totalQuantityProduct;

    public XSSFWorkbook header;

    public Product(){
        header = XLSWorker.getInternalWorkbook(Constants.INPUT_PATH + "header_sku.xlsx");
        attributeIds = new ArrayList<>();
        optionIds = new ArrayList<>();
    }

    public int getCategoryByRoot(String cateName, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_WEB_GETCATEGORYROOT");
        String response = run("GET", source, "", token);
        System.out.println("Response: " + response);
        if(response == null)
            return -1;
        try{
            JSONObject obj = new JSONObject(response);
            System.out.println("Obj: " + obj.toString());
            JSONArray result = (JSONArray) obj.get("result");
            for(int i=0; i<result.length(); i++){
                JSONObject cate = (JSONObject) result.get(i);
                String name = cate.getString("name");
                if(cateName.equals(name))
                    return (int) cate.get("id");
            }
        }catch (Exception e){

        }

        return -1;
    }

    public int getCategoryByRootOnMobile(String cateName, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_MOBILE_GETCATEGORYROOT");
        String response = run("GET", source, "", token);
        if(response == null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        for(int i=0; i<result.length(); i++){
            JSONObject cate = (JSONObject) result.get(i);
            String name = (String) cate.get("name");
            if(cateName.equals(name))
                return (int) cate.get("id");
        }
        return -1;
    }

    public int getCategoryByRootOnPartner(String cateName, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_PARTNER_GETCATEGORYROOT");
        String response = run("GET", source, "", token);
        if(response == null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        for(int i=0; i<result.length(); i++){
            JSONObject cate = (JSONObject) result.get(i);
            String name = (String) cate.get("name");
            if(cateName.equals(name))
                return (int) cate.get("id");
        }
        return -1;
    }

    public int getCategoryByParentId(int id, String cateName, String token){
        if(id==-1)
            return -1;
        String source = getSellerUrl(header,"ENDPOINT_API_WEB_GETCATEBYPARENTID") + id;
        String response = run("GET", source, "", token);
        if(response == null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        for(int i=0; i<result.length(); i++){
            JSONObject cate = (JSONObject) result.get(i);
            String name = (String) cate.get("name");
            if(cateName.equals(name))
                return (int) cate.get("id");
        }
        return -1;
    }

    public int getCategoryByParentIdOnMobile(int id, String cateName, String token){
        if(id==-1)
            return -1;
        String source = getSellerUrl(header,"ENDPOINT_API_MOBILE_GETCATEBYPARENTID") + id;
        String response = run("GET", source, "", token);
        if(response == null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        for(int i=0; i<result.length(); i++){
            JSONObject cate = (JSONObject) result.get(i);
            String name = (String) cate.get("name");
            if(cateName.equals(name))
                return (int) cate.get("id");
        }
        return -1;
    }

    public int getCategoryByParentIdOnPartner(int id, String cateName, String token){
        if(id==-1)
            return -1;
        String source = getSellerUrl(header,"ENDPOINT_API_PARTNER_GETCATEBYPARENTID") + id;
        String response = run("GET", source, "", token);
        if(response == null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        for(int i=0; i<result.length(); i++){
            JSONObject cate = (JSONObject) result.get(i);
            String name = (String) cate.get("name");
            if(name.contains(cateName))
                return (int) cate.get("id");
        }
        return -1;
    }

    public JSONObject getCateIds(int cat2Id, int cat3Id, int cat4Id){
        JSONObject obj = new JSONObject();
        obj.put("Cat2Id", cat2Id);
        obj.put("Cat3Id", cat3Id);
        obj.put("Cat4Id", cat4Id);
        return obj;
    }

    public String getCattegoryByName(String name, String token){
        String method = "GET";
        String source = getSellerUrl(header,"ENDPOINT_API_WEB_CATEGORY") + name;
        return run(method, source, "N/A", token);
    }

    public String getCattegoryByNameOnMobile(String name, String token){
        String method = "GET";
        String source = getSellerUrl(header,"ENDPOINT_API_MOBILE_CATEGORY") + name;
        return run(method, source, "N/A", token);
    }

    public String getCate4(String response){
        return readValueByJsonPath(response, XLSWorker.getValueFromExcel(header,"Cate4Id", 1, "JsonPath"));
    }

    public int getBrandIdByName(int cateId, String brandName, String token){
        String method = "GET";
        String source = getSellerUrl("ENDPOINT_API_WEB_BRAND") + cateId;
        String response = run(method, source, "N/A", token);
        if(response==null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONObject result = (JSONObject) obj.get("result");
        JSONArray brands = (JSONArray) result.get("brands");
        for(int i=0; i < brands.length(); i++){
            JSONObject brand = (JSONObject) brands.get(i);
            System.out.println("BrandName: " + brandName);
            String name = (String) brand.get("name");
            System.out.println("Name: " + name);
            if(brandName.equals(name))
                return (int) brand.get("id");
        }
        return -1;
    }

    public int getBrandIdByNameOnMobile(int cateId, String brandName, String token){
        String method = "GET";
        String source = getSellerUrl("ENDPOINT_API_MOBILE_BRAND") + cateId;
        String response = run(method, source, "N/A", token);
        if(response==null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONObject result = (JSONObject) obj.get("result");
        JSONArray brands = (JSONArray) result.get("brands");
        for(int i=0; i < brands.length(); i++){
            JSONObject brand = (JSONObject) brands.get(i);
            System.out.println("BrandName: " + brandName);
            String name = (String) brand.get("name");
            System.out.println("Name: " + name);
            if(brandName.equals(name))
                return (int) brand.get("id");
        }
        return -1;
    }

    public JSONArray getCertificatesByBrandId(int cat4Id, int brandId, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_WEB_GETCERTIFICATES") + cat4Id + "/" + brandId;
        String response = run("GET", source, "N/A", token);
        if(response==null)
            return null;
        JSONObject obj = new JSONObject(response);
        return (JSONArray) obj.get("result");
    }

    public JSONArray getCertificatesByBrandIdOnMobile(int cat4Id, int brandId, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_MOBILE_GETCERTIFICATES") + cat4Id + "/" + brandId;
        String response = run("GET", source, "N/A", token);
        if(response==null)
            return null;
        JSONObject obj = new JSONObject(response);
        return (JSONArray) obj.get("result");
    }

    public String getProductAttributes(String cateId, String token, JSONObject attribute3){
        attributeIds = new ArrayList<>();
        optionIds = new ArrayList<>();

        String method = "GET";
        String source = getSellerUrl(header, "ENDPOINT_API_WEB_BRAND") + cateId;
        String getAttrBrand = run(method, source, "N/A", token);

        JSONArray attributes = new JSONArray();
        JSONObject getResponse = new JSONObject(getAttrBrand);
        JSONArray getAttributes = getResponse.getJSONObject("result").getJSONArray("attributeCollections");
        for(int i=0; i< getAttributes.length(); i++){
            JSONObject getAttribute = (JSONObject) getAttributes.get(i);
            int getId = (int) getAttribute.get("id");
            String getControlType = (String) getAttribute.get("controlType");
            String getName = (String) getAttribute.get("name");
            Boolean getIsCheckout = (Boolean) getAttribute.get("isCheckout");

            JSONObject attribute = new JSONObject();
            attribute.put("ID",getId);
            attribute.put("Name",getName);
            attribute.put("ControlType",getControlType);
            attribute.put("IsCheckout",getIsCheckout);

            JSONArray getAttributeValues = getAttribute.getJSONArray("attributeValues");
            JSONArray attributeValues = new JSONArray();

            for(int j = 0; j < 1; j++){
                try{
                    JSONObject getAttributeValue;
                    if(getControlType.equals("TextBox")) {
                        getAttributeValue = (JSONObject) getAttributeValues.get(0);
                        int getIdValue = (int) getAttributeValue.get("id");

                        JSONObject attributeValue = new JSONObject();
                        attributeValue.put("ID",getIdValue);
                        attributeValue.put("Value","Test");
                        attributeValue.put("IsSelected",(Boolean) null);
                        attributeValues.put(attributeValue);
                    }else{
                        getAttributeValue = (JSONObject) getAttributeValues.get(j);
                        int getIdValue = (int) getAttributeValue.get("id");
                        String getValue = (String) getAttributeValue.get("value");

                        JSONObject attributeValue = new JSONObject();
                        attributeValue.put("ID",getIdValue);
                        attributeValue.put("Value",getValue);
                        attributeValue.put("IsSelected",true);
                        attributeValues.put(attributeValue);

                        if(getIsCheckout){
                            attributeIds.add(getId);
                            optionIds.add(getIdValue);
                        }
                    }
                }catch (Exception e){

                }

            }

            attribute.put("AttributeValues",attributeValues);
            attributes.put(attribute);
        }

        if(attribute3!=null && attributeIds.size()==0)
            attributes.put(attribute3);
        return attributes.toString();
    }

    public String getProductAttributesOnMobile(String cateId, String token, JSONObject attribute3){
        attributeIds = new ArrayList<>();
        optionIds = new ArrayList<>();

        String method = "GET";
        String source = getSellerUrl(header, "ENDPOINT_API_MOBILE_BRAND") + cateId;
        String getAttrBrand = run(method, source, "N/A", token);

        JSONArray attributes = new JSONArray();
        JSONObject getResponse = new JSONObject(getAttrBrand);
        JSONArray getAttributes = getResponse.getJSONObject("result").getJSONArray("attributeCollections");
        for(int i=0; i< getAttributes.length(); i++){
            JSONObject getAttribute = (JSONObject) getAttributes.get(i);
            int getId = (int) getAttribute.get("id");
            String getControlType = (String) getAttribute.get("controlType");
            String getName = (String) getAttribute.get("name");
            Boolean getIsCheckout = (Boolean) getAttribute.get("isCheckout");

            JSONObject attribute = new JSONObject();
            attribute.put("ID",getId);
            attribute.put("Name",getName);
            attribute.put("ControlType",getControlType);
            attribute.put("IsCheckout",getIsCheckout);

            JSONArray getAttributeValues = getAttribute.getJSONArray("attributeValues");
            JSONArray attributeValues = new JSONArray();

            for(int j = 0; j < 1; j++){
                try{
                    JSONObject getAttributeValue;
                    if(getControlType.equals("TextBox")) {
                        getAttributeValue = (JSONObject) getAttributeValues.get(0);
                        int getIdValue = (int) getAttributeValue.get("id");

                        JSONObject attributeValue = new JSONObject();
                        attributeValue.put("ID",getIdValue);
                        attributeValue.put("Value","Test");
                        attributeValue.put("IsSelected",(Boolean) null);
                        attributeValues.put(attributeValue);
                    }else{
                        getAttributeValue = (JSONObject) getAttributeValues.get(j);
                        int getIdValue = (int) getAttributeValue.get("id");
                        String getValue = (String) getAttributeValue.get("value");

                        JSONObject attributeValue = new JSONObject();
                        attributeValue.put("ID",getIdValue);
                        attributeValue.put("Value",getValue);
                        attributeValue.put("IsSelected",true);
                        attributeValues.put(attributeValue);

                        if(getIsCheckout){
                            attributeIds.add(getId);
                            optionIds.add(getIdValue);
                        }
                    }
                }catch (Exception e){

                }

            }

            attribute.put("AttributeValues",attributeValues);
            attributes.put(attribute);
        }

        if(attribute3!=null && attributeIds.size()==0)
            attributes.put(attribute3);
        return attributes.toString();
    }

    public String getProductAttributesOnPartner(String cateId, String token, JSONObject attribute3){
        attributeIds = new ArrayList<>();
        optionIds = new ArrayList<>();

        String method = "GET";
        String source = getSellerUrl(header, "ENDPOINT_API_PARTNER_GETATTRIBUTEBYCATEID") + cateId + "/attribute";
        String getAttrBrand = run(method, source, "N/A", token);

        JSONArray attributes = new JSONArray();
        JSONObject getResponse = new JSONObject(getAttrBrand);
        JSONArray getAttributes = getResponse.getJSONObject("result").getJSONArray("attribute");
        for(int i=0; i< getAttributes.length(); i++){
            JSONObject getAttribute = (JSONObject) getAttributes.get(i);
            int getId = (int) getAttribute.get("id");
            String getControlType = (String) getAttribute.get("controlType");
            String getName = (String) getAttribute.get("name");
            Boolean getIsCheckout = (Boolean) getAttribute.get("isCheckout");

            JSONObject attribute = new JSONObject();
            attribute.put("ID",getId);
            attribute.put("Name",getName);
            attribute.put("ControlType",getControlType);
            attribute.put("IsCheckout",getIsCheckout);

            JSONArray getAttributeValues = getAttribute.getJSONArray("attributeValues");
            JSONArray attributeValues = new JSONArray();

            for(int j = 0; j < 1; j++){
                try{
                    JSONObject getAttributeValue;
                    if(getControlType.equals("TextBox")) {
                        getAttributeValue = (JSONObject) getAttributeValues.get(0);
                        int getIdValue = (int) getAttributeValue.get("id");

                        JSONObject attributeValue = new JSONObject();
                        attributeValue.put("ID",getIdValue);
                        attributeValue.put("Value","Test");
                        attributeValue.put("IsSelected",(Boolean) null);
                        attributeValues.put(attributeValue);
                    }else{
                        getAttributeValue = (JSONObject) getAttributeValues.get(j);
                        int getIdValue = (int) getAttributeValue.get("id");
                        String getValue = (String) getAttributeValue.get("value");

                        JSONObject attributeValue = new JSONObject();
                        attributeValue.put("ID",getIdValue);
                        attributeValue.put("Value",getValue);
                        attributeValue.put("IsSelected",true);
                        attributeValues.put(attributeValue);

                        if(getIsCheckout){
                            attributeIds.add(getId);
                            optionIds.add(getIdValue);
                        }
                    }
                }catch (Exception e){

                }

            }

            attribute.put("AttributeValues",attributeValues);
            attributes.put(attribute);
        }

        if(attribute3!=null && attributeIds.size()==0)
            attributes.put(attribute3);
        return attributes.toString();
    }

    public JSONObject getAttribute3(int row, String sheetName, JSONObject currentVariants){

        int attributeId;
        int optionId;
        if(currentVariants!=null){
            JSONObject getVariant = (JSONObject) currentVariants.getJSONObject("result").getJSONArray("variants").get(0);
            JSONObject getAttribute = (JSONObject) getVariant.getJSONArray("attributes").get(0);
            attributeId = getAttribute.getInt("attributeId");
            optionId = getAttribute.getInt("optionId");
        }else{
            optionId = 7001;
            attributeId = 7000;
        }

        JSONArray attributes = new JSONArray();

        String attrName = XLSWorker.getValueFromExcel(row, "AttrName", sheetName);
        String attrValueName = XLSWorker.getValueFromExcel(row, "AttrValueName", sheetName);
        System.out.println("Add attribute3 to attributes");

        JSONObject attribute3 = new JSONObject();
        attribute3.put("ID", attributeId);
        attribute3.put("Name", attrName);
        attribute3.put("ControlType", "CheckBox");
        attribute3.put("IsCheckout", true);
        attribute3.put("IsCustom", true);

        JSONArray attributeValues = new JSONArray();

        JSONObject attributeValue = new JSONObject();
        attributeValue.put("ID", optionId);
        attributeValue.put("Value", attrValueName);
        attributeValue.put("IsSelected", true);

        attributeValues.put(attributeValue);
        attribute3.put("AttributeValues", attributeValues);

        attributes.put(attribute3);
        return attribute3;
    }

    public JSONObject getCurrentVariants(String productId, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_WEB_GETVARIANTS") + productId;
        String response = run("GET", source, "N/A", token);
        return new JSONObject(response);
    }

//    public JSONObject getCurrentVariantsOnMobile(String productId, String token){
//        String response = getProductDetailOnMobile(productId, token);
//        return new JSONObject(response).getJSONObject("result").getJSONArray("variants").(JSONObject) get(0);
//    }

    public JSONArray getVariant(int row, String sheetName, JSONObject currentVariants){
        String attrSKU = XLSWorker.getValueFromExcel(row, "AttrSKU", sheetName);
        String attrPrice = XLSWorker.getValueFromExcel(row, "AttrPrice", sheetName);
        String attrSpecialPrice = XLSWorker.getValueFromExcel(row, "AttrSpecialPrice", sheetName);
        String attrQuantity = XLSWorker.getValueFromExcel(row, "AttrQuantity", sheetName);
        String attrPromotionStartDate = XLSWorker.getValueFromExcel(row, "AttrPromotionStartDate", sheetName);
        String attrPromotionEndDate = XLSWorker.getValueFromExcel(row, "AttrPromotionStartDate", sheetName);

        JSONArray variants = new JSONArray();

        JSONObject variant = new JSONObject();
        if(attrSKU.equals("GetSkuRandom"))
            attrSKU = "SKU" + getRandom();
        variant.put("SkuUser",attrSKU);
        variant.put("Price", attrPrice);
        variant.put("SpecialPrice",attrSpecialPrice);
        variant.put("Qty",attrQuantity);
        variant.put("PromotionStartDate",getPromotionDate(attrPromotionStartDate));
        variant.put("PromotionEndDate",getPromotionDate(attrPromotionEndDate));

        JSONArray variantAttributes = new JSONArray();
        if(attributeIds.size()>0){
            for(int i=0; i<attributeIds.size(); i++){
                JSONObject variantAttribute = new JSONObject();
                variantAttribute.put("OptionId",optionIds.get(i));
                variantAttribute.put("AttributeId",attributeIds.get(i));
                variantAttribute.put("IsCustom",false);
                variantAttributes.put(variantAttribute);
            }
        }else{
            if(currentVariants!=null){
                JSONObject getVariant = (JSONObject) currentVariants.getJSONObject("result").getJSONArray("variants").get(0);
                JSONObject getAttribute = (JSONObject) getVariant.getJSONArray("attributes").get(0);
                int attributeId = getAttribute.getInt("attributeId");
                int optionId = getAttribute.getInt("optionId");

                for(int i=0; i<1; i++){
                    JSONObject variantAttribute = new JSONObject();
                    variantAttribute.put("OptionId",optionId);
                    variantAttribute.put("AttributeId",attributeId);
                    variantAttribute.put("IsCustom",true);
                    variantAttributes.put(variantAttribute);
                }
            }else{
                for(int i=0; i<1; i++){
                    JSONObject variantAttribute = new JSONObject();
                    variantAttribute.put("OptionId",7001);
                    variantAttribute.put("AttributeId",7000);
                    variantAttribute.put("IsCustom",true);
                    variantAttributes.put(variantAttribute);
                }
            }

        }

        variant.put("attributes",variantAttributes);
        variants.put(variant);

        return variants;
    }


    public String getProductDetail(String productId, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_WEB_PRODUCT") + "/" + productId;
        String method = "GET";
        return run(method, source, "", token);
    }

    public String getProductDetailOnMobile(String productId, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_MOBILE_PRODUCT") + "/" + productId;
        String method = "GET";
        return run(method, source, "", token);
    }

    public String getProductDetailOnPartner(String productId, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_PARTNER_PRODUCT") + "/" + productId;
        String method = "GET";
        return run(method, source, "", token);
    }

    public String getVersionNo(String productId, String token){
        String response = getProductDetail(productId, token);
        String jsonPath = XLSWorker.getValueFromExcel(header,"VersionNo",1,"JsonPath");
        return readValueByJsonPath(response, jsonPath);
    }

    public String getVersionNoOnMobile(String productId, String token){
        String response = getProductDetailOnMobile(productId, token);
        String jsonPath = XLSWorker.getValueFromExcel(header,"VersionNo",1,"JsonPath");
        return readValueByJsonPath(response, jsonPath);
    }

    public String getVersionNoOnPartner(String productId, String token){
        String response = getProductDetailOnPartner(productId, token);
        String jsonPath = XLSWorker.getValueFromExcel(header,"VersionNo",1,"JsonPath");
        return readValueByJsonPath(response, jsonPath);
    }

    public String getCurrentImage(String productId, String token){
        String response = getProductDetail(productId, token);
        String jsonPath = XLSWorker.getValueFromExcel(header,"ProductImage",1,"JsonPath");
        return readValueByJsonPath(response, jsonPath);
    }

    public String getCurrentImageOnMobile(String productId, String token){
        String response = getProductDetailOnMobile(productId, token);
        String jsonPath = XLSWorker.getValueFromExcel(header,"ProductImage",1,"JsonPath");
        return readValueByJsonPath(response, jsonPath);
    }

    public String getCurrentImageOnPartner(String productId, String token){
        String response = getProductDetailOnPartner(productId, token);
        String jsonPath = XLSWorker.getValueFromExcel(header,"ProductImage",1,"JsonPath");
        return readValueByJsonPath(response, jsonPath);
    }

    public int getProductId(String response){
        JSONObject obj = new JSONObject(response);
        return obj.getJSONObject("result").getInt("id");
    }

    public int getProductId(String key, String response){
        JSONObject obj = new JSONObject(response);
        return obj.getInt(key);
    }

    public void changeStock(int row, String productId, String token){
        JSONObject data = new JSONObject();

        data.put("productId", productId);

        String categoryName = XLSWorker.getValueFromExcel(row, "Category", "EditProduct");
        String cateResponse = getCattegoryByName(categoryName, token);
        data.put("cateResponse", cateResponse);

        String cate4 = getCate4(cateResponse);
        String attributes = getProductAttributes(cate4, token, null);
        data.put("attributes", attributes);

        String currentImage = getCurrentImage(productId, token);
        data.put("productImage", currentImage);

        String versionNo = getVersionNo(productId, token);
        data.put("versionNo", versionNo);

        run(row, token, "EditProduct", data);
    }

    public String getCloneProductId(String response){
        String jsonPath = XLSWorker.getValueFromExcel(header,"CloneId",1,"JsonPath");
        return readValueByJsonPath(response, jsonPath);
    }

    public int productCount(String token){
        String source = getSellerUrl(header,"ENDPOINT_API_WEB_PRODUCT_COUNT");
        String response = run("POST", source, "", token);
        if(response==null)
            return -1;
        JSONObject obj = new JSONObject(response);
        return (int) obj.get("result");
    }

    public String copyProduct(String productId, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_WEB_COPYPRODUCT") + productId;
        String method = "GET";
        return run(method, source, "", token);
    }

    public boolean verifyProductInList(int productId, String responseList){
        try{
            JSONObject fullResponse = new JSONObject(responseList);
            JSONObject result = (JSONObject) fullResponse.get("result");
            JSONArray datas = (JSONArray) result.get("data");
            for(int i=0; i<datas.length(); i++){
                JSONObject data = (JSONObject) datas.get(i);
                int id = data.getInt("id");
                if(productId == id) {
                    return true;
                }
            }
        }catch (Exception e){
            return false;
        }
        message = "Cannot find product Id in product List!";
        return false;
    }

    public boolean verifyProductNotInList(int productId, String responseList){
        try{
            JSONObject fullResponse = new JSONObject(responseList);
            JSONObject result = (JSONObject) fullResponse.get("result");
            JSONArray datas = (JSONArray) result.get("data");
            for(int i=0; i<datas.length(); i++){
                JSONObject data = (JSONObject) datas.get(i);
                int id = data.getInt("id");
                if(productId == id) {
                    message = "Product " + productId + " is still in list";
                    return false;
                }
            }
        }catch (Exception e){
            return false;
        }
        return true;

    }


    public boolean verifyCount(int firstCount, int secondCount){
        if(secondCount!=firstCount+1){
            message = "Product count is not correct!";
            return false;
        }
        return true;
    }

    public int getTotalRecords(String response){
        String jsonPath = XLSWorker.getValueFromExcel(header, "TotalRecords",1,"JsonPath");
        if(readValueByJsonPath(response, jsonPath)==null)
            return 0;
        return Integer.valueOf(readValueByJsonPath(response, jsonPath));
    }

    public boolean verifyCount(String token){
        int totalRecords = getTotalRecords(run(1, token, "ProductList", null));
        int totalInstock = getTotalRecords(run(7, token, "ProductList", null));
        int totalOutstock = getTotalRecords(run(8, token, "ProductList", null));
        System.out.println(totalRecords);
        System.out.println(totalInstock);
        System.out.println(totalOutstock);
        if(totalRecords != (totalInstock + totalOutstock)){
            message = "Product count is not correct! " + totalRecords + "-" + (totalInstock + totalOutstock);
            return false;
        }
        return true;
    }

    public boolean verifyProductList(int row, String response, String token){
        String payload = getPayloadFromExcel(row, "ProductList", null);
        JSONObject payloadObj = new JSONObject(payload);
        String productName = (String) payloadObj.get("ProductName");
        String productStatus = (String) payloadObj.get("ProductStatus");
        Object productStock = payloadObj.get("ProductStock");
        String storeSKU = (String) payloadObj.get("StoreSKU");
        JSONObject productObj = new JSONObject(response);
        JSONObject result = (JSONObject) productObj.get("result");
        JSONArray products = (JSONArray) result.get("data");
        for(int i=0; i<products.length(); i++){
            JSONObject product = (JSONObject) products.get(i);
            String name = (String) product.get("productName");
            if(!productName.equals("") && !name.contains(productName)){
                message = "Product name is not matched!";
                return false;
            }
            int status = (int) product.get("productStatus");
            if(!productStatus.equals("") && !productStatus.equals(String.valueOf(status))){
                message = "Product status is not matched!";
                return false;
            }
            boolean stock = (boolean) product.get("productStock");
            if(!String.valueOf(productStock).equals("") && productStock.equals(String.valueOf(stock))){
                message = "Product stock is not matched!";
                return false;
            }
            String sku = (String) product.get("productSKU");
            if(!storeSKU.equals("") && !storeSKU.equals(sku)){
                message = "Store sku is not matched!";
                return false;
            }
        }

        if(productName.equals("") && productStatus.equals("") && String.valueOf(productStock).equals("") && storeSKU.equals("")){
            return verifyCount(token);
        }
        return true;
    }

    public Boolean verifyCopiedProduct(String productId, JSONObject response){
        if(productId==null)
            return false;
        int id = response.getJSONObject("result").getInt("id");
        if(Integer.valueOf(productId) != id){
            message = "Clone id is not in response";
            return false;
        }
        return true;
    }


    public boolean delete(int productId, String token){
        JSONArray array = new JSONArray();
        array.put(productId);
        String source = getSellerUrl(header,"ENDPOINT_API_WEB_PRODUCT");
        String response = run("DELETE", source, array.toString(), token);
        return response!=null;
    }

    public boolean deleteOnMobile(int productId, String token){
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        array.put(productId);
        object.put("productIds", array);
        String source = getSellerUrl(header,"ENDPOINT_API_MOBILE_PRODUCT");
        String response = run("DELETE", source, object.toString(), token);
        return response!=null;
    }

    public String delete(int row, String sheetName, String token, JSONArray payload){
        String method = "DELETE";
        String baseUrl = getSellerUrl(header,"BASE_URL");
        String a = XLSWorker.getValueFromExcel(row, "EndPoint", sheetName);
        String endPoint = getSellerUrl(a);
        String source = baseUrl + endPoint;
        return APIController.sendApiRequest(method, source, payload.toString(), token);
    }

    public int getImportId(String response){
        String jsonPath = XLSWorker.getValueFromExcel(header,"Result",1,"JsonPath");
        return Integer.valueOf(readValueByJsonPath(response, jsonPath));
    }

    public int getExportId(String response){
        String jsonPath = XLSWorker.getValueFromExcel(header,"Result",1,"JsonPath");
        return Integer.valueOf(readValueByJsonPath(response, jsonPath));
    }

    public boolean checkExportStatus(int exportId, int expectStatus, int expectType, String token){
        String fileName = null;
        switch (expectType){
            case 2:
                fileName = "gia_tonkho";
                break;
            case 3:
                fileName = "thongtin_vanchuyen";
                break;
            case 1:
                fileName = "thongtin_macdinh";
                break;
        }
        for(int i=0; i<5; i++){
            try {
                String method = "GET";
                String source = getSellerUrl(header,"ENDPOINT_API_PRODUCTEXPORT_CHECKSTATUS") + exportId;
                String response = APIController.sendApiRequest(method, source, "", token);
                String jsonPath = XLSWorker.getValueFromExcel(header, "StatusCode",1,"JsonPath");
                String statusCode = readValueByJsonPath(response, jsonPath);
                if(!statusCode.equals("200")){
                    message = "Check status is failed!";
                    return false;
                }
                int status = Integer.valueOf(readValueByJsonPath(response, XLSWorker.getValueFromExcel(header, "Status",1,"JsonPath")));
                System.out.println(status + " - "  + expectStatus);
                String files = readValueByJsonPath(response, XLSWorker.getValueFromExcel(header, "Files",1,"JsonPath"));
                System.out.println(files);
                int type = Integer.valueOf(readValueByJsonPath(response, XLSWorker.getValueFromExcel(header,"Type",1,"JsonPath")));
                System.out.println(type + " - "  + expectType);
                if(status==expectStatus && files.contains(fileName) && type==expectType)
                    return true;
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        message = "Check status is failed!";
        return false;
    }

    public boolean checkImportStatus(int importId, int expectStatus, String expectFileName, int expectType, String token){
        for(int i=0; i<5; i++){
            try {
                String method = "GET";
                String source = getSellerUrl(header,"ENDPOINT_API_PRODUCTIMPORT_CHECKSTATUS") + importId;
                String response = APIController.sendApiRequest(method, source, "", token);
                String jsonPath = XLSWorker.getValueFromExcel(header, "StatusCode",1,"JsonPath");
                String statusCode = readValueByJsonPath(response, jsonPath);
                if(!statusCode.equals("200")){
                    message = "Check status is failed!";
                    return false;
                }
                int status = Integer.valueOf(readValueByJsonPath(response, XLSWorker.getValueFromExcel(header,"Status",1,"JsonPath")));
                System.out.println(status + " - "  + expectStatus);
                String fileName = readValueByJsonPath(response, XLSWorker.getValueFromExcel(header,"FileName",1,"JsonPath"));
                System.out.println(fileName);
                int type = Integer.valueOf(readValueByJsonPath(response, XLSWorker.getValueFromExcel(header,"Type",1,"JsonPath")));
                System.out.println(type + " - "  + expectType);
                if(status==expectStatus && expectFileName.equals(fileName) && type==expectType)
                    return true;
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        message = "Check status is failed!";
        return false;
    }

    public String getProductImports(String token){
        String source = getSellerUrl(header,"ENDPOINT_API_PRODUCTIMPORT_GETPRODUCTIMPORTS");
        JSONObject obj = new JSONObject();
        obj.put("pageindex",1);
        obj.put("pagesize",10);
        return APIController.sendApiRequest("POST", source, obj.toString(), token);
    }

    public String getProductExport(String token){
        String source = getSellerUrl(header,"ENDPOINT_API_PRODUCTEXPORT_GETREQUESTEXPORT");
        JSONObject obj = new JSONObject();
        obj.put("pageindex",1);
        obj.put("pagesize",10);
        return APIController.sendApiRequest("POST", source, obj.toString(), token);
    }

    public String getProductImportDetail(int importId, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_PRODUCTIMPORT_GETPRODUCTIMPORTDETAIL");
        JSONObject obj = new JSONObject();
        obj.put("importId",importId);
        obj.put("pageindex",1);
        obj.put("pagesize",10);
        return APIController.sendApiRequest("POST", source, obj.toString(), token);
    }

    public boolean verifyProductImportDetail(int importId, String response){
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        if(result.length() == 0){
            message = "Failed to load product list";
            return false;
        }
        for(int i=0; i<result.length(); i++){
            JSONObject product = (JSONObject) result.get(i);
            int id = (int) product.get("importId");
            if(importId != id)
                return false;
        }
        return true;
    }

    public boolean verifyExportHistory(int expectId, int expectStatus, int expectType, String response){
        String fileName = "";
        switch (expectType){
            case 2:
                fileName = "gia_tonkho";
                break;
            case 3:
                fileName = "thongtin_vanchuyen";
                break;
            case 1:
                fileName = "thongtin_macdinh";
                break;
        }

        JSONObject obj = new JSONObject(response);
        JSONArray results = (JSONArray) obj.get("result");
        for(int i=0; i<results.length(); i++){
            JSONObject result = (JSONObject) results.get(i);
            int id = (int) result.get("id");
            if(id==expectId){
                int status = (int) result.get("status");
                JSONArray files = (JSONArray) result.get("files");
                String file = (String) files.get(0);
                int type = (int) result.get("type");
                System.out.println(id);
                System.out.println(status);
                System.out.println(file);
                System.out.println(type);
                if(status==expectStatus && file.contains(fileName) && type==expectType)
                    return true;
            }
        }
        message = "Danh sach lich su export khong dung!";
        return false;
    }

    public boolean verifyImportHistory(int expectId, int expectStatus, String fileName, int expectType, String response){
        JSONObject obj = new JSONObject(response);
        JSONArray results = (JSONArray) obj.get("result");
        for(int i=0; i<results.length(); i++){
            JSONObject result = (JSONObject) results.get(i);
            int id = (int) result.get("id");
            if(id==expectId){
                int status = (int) result.get("status");
                String file = (String) result.get("fileName");
                int type = (int) result.get("type");
                System.out.println(id);
                System.out.println(status);
                System.out.println(file);
                System.out.println(type);
                if(status==expectStatus && file.equals(fileName) && type==expectType)
                    return true;
            }
        }
        message = "Danh sach lich su import khong dung!";
        return false;
    }


    public boolean cancelImport(int id, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_PRODUCTIMPORT_CANCELIMPORT");
        JSONObject obj = new JSONObject();
        obj.put("id",id);
        String response =  APIController.sendApiRequest("POST", source, obj.toString(), token);
        obj = new JSONObject(response);
        if((int) obj.get("statusCode") != 200){
            message = "Cancel import is failed!";
            return false;
        }
        return true;
    }

    public boolean applyImport(int id, String token){
        String source = getSellerUrl(header,"ENDPOINT_API_PRODUCTIMPORT_EXECUTE");
        JSONObject obj = new JSONObject();
        obj.put("id",id);
        String response =  APIController.sendApiRequest("POST", source, obj.toString(), token);
        obj = new JSONObject(response);
        if((int) obj.get("statusCode") != 200){
            message = "Apply import is failed!";
            return false;
        }
        return true;
    }

    public void verifyApplyImport(String response){

    }

    public boolean exportProduct(int row, String sheetName, String token){
        String source = getSellerUrl(header,"ENDPOINT_PRODUCT_EXPORTPRODUCTS");
        source = source + getParamsFromExcel("?", row, sheetName, null);
        String response = run("POST", source, "", token);
        if(response!=null)
            return true;
        message = "Export products is failed!";
        return false;
    }

    public int getTotalQuantity(String response) {
    	JSONObject obj = new JSONObject(response);
    	return obj.getInt("quantity");
    }
    
    public int getEndDateVarriantPromo(String response) {
    	JSONObject obj = new JSONObject(response);
    	double date = obj.getJSONObject("result").getJSONArray("variants").getJSONObject(0).getDouble("variant_promotion_end_date_timestamp");
    	int intDate = (int) date;
    	return intDate;
    }
    
    public int getEndDatePromo(String response) {
    	JSONObject obj = new JSONObject(response);
    	double date = obj.getJSONObject("result").getDouble("promotion_to_date_timestamp");
    	int intDate = (int) date;
    	return intDate;
    }
    
    public boolean currentBeforeCompareTime(int dateCompare) {		
		long tsNow = System.currentTimeMillis()/1000;
		int tsCompare = dateCompare;
		Timestamp now = new Timestamp(tsNow);
		Timestamp com = new Timestamp(tsCompare);
		if(now.before(com)) {
			return true;
		}
		else
			return false;
	}	
    
      
}

