package apis.oms;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.XLSWorker;

public class Oms extends BaseObject {
    private String baseApi;
    private String endPoint;   

    public Oms(){
//        baseApi = getSellerUrl("BASE_OMS_URL");
    }

    public String getOrderNumber(){
        return orderNumber;
    }

    public void setOrderNumber(String _orderNumber){
        orderNumber = _orderNumber;
    }

    public void resetOrderNumber(){
        orderNumber = null;
    }

    public boolean verifyOrder(JSONArray array){
        System.out.println(array);
        for(int i=0; i<array.length(); i++){
            JSONObject obj = (JSONObject) array.get(0);
            String orderNumber = obj.getString("OrderNumber");
            int _orderStatus = obj.getInt("OrderStatus");
            int _deliveryStatus = obj.getInt("DeliveryStatus");
            int _paymentStatus = obj.getInt("PaymentStatus");

            String source = getSellerUrl("ENDPOINT_API_SALESORDER_GETORDERDETAIL") + orderNumber;
            String response = run("GET", source, "", "");
            JSONObject object = new JSONObject(response);
            int orderStatus = object.getJSONObject("Data").getJSONObject("OrderDetail").getInt("OrderStatus");
            if(_orderStatus != orderStatus)
                return false;
            int paymentStatus = object.getJSONObject("Data").getJSONObject("OrderDetail").getInt("PaymentStatus");
            if(_paymentStatus != paymentStatus)
                return false;
            if(_deliveryStatus != -1){
                int deliveryStatus = object.getJSONObject("Data").getJSONObject("OrderDetail").getInt("DeliveryStatus");
                if(_deliveryStatus != deliveryStatus)
                    return false;
            }
        }
        return true;
    }

}
