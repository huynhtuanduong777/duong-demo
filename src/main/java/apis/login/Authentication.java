package apis.login;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.Constants;
import utils.JSON;
import utils.XLSWorker;

import java.util.Iterator;

public class Authentication extends BaseObject {
    public Authentication(){
    }

    public String login(int row, String sheetName){
        String source = getSellerUrl("ENDPOINT_API_WEB_TOKEN");
        String payload = XLSWorker.getValueFromExcel(row, "Token", sheetName);
        return run("POST", source, payload, "");
    }

    public boolean verifyUserContext(JSONObject parentResponse, JSONObject childResponse){
        Iterator<String> keys = childResponse.keys();

        while(keys.hasNext()) {
            String key = keys.next();
            System.out.println(parentResponse.get(key) + "-" + childResponse.get(key));

            if(parentResponse.get(key)!=null && !childResponse.get(key).equals("")){
                if(parentResponse.get(key)!=childResponse.get(key)){
                    message = "Key " + key + " in usercontext is not correct";
                    return false;
                }
            }
        }
        return true;
    }

    public boolean verifyRoles(JSONArray parentResponse, JSONArray childResponse){
        boolean result = JSON.JSONArrayEquals(parentResponse, childResponse);
        if(!result){
            message = "Value in roles is not correct!";
            return false;
        }
        return true;
    }

    public boolean verifySendoId(String sendoId_1, String sendoId_2){
        if(!sendoId_1.equals(sendoId_2)){
            message = "SendoId is not correct!";
            return false;
        }
        return true;
    }

    public boolean verifyMenu(JSONArray parentResponse, JSONArray childResponse){
        boolean result = JSON.JSONArrayEquals(parentResponse, childResponse);
        if(!result){
            message = "Value in menu is not correct!";
            return false;
        }
        return true;
    }

    public boolean verifyStoreAccess(JSONArray parentResponse, JSONArray childResponse){
        boolean result = JSON.JSONArrayEquals(parentResponse, childResponse);
        if(!result){
            message = "Value in storeAccess is not correct!";
            return false;
        }
        return true;
    }

    public String getApiKey(String token){
        String source = getSellerUrl("BASE_WEB_URL") + "/Shop/GetShopInfo";
        String response = run("POST", source, "", token);
        String key = new JSONObject(response).getJSONObject("Data").getString("ApiKey");
        return key;
    }

    public String getBearerToken(String token){
        String bearer;
        String apiKey = getApiKey(token);
        String source = getSellerUrl("BASE_SAPI_URL") + "/shop/authentication";
        String payload = "grant_type=password&username=" + apiKey + "&password=9397f820941f37f2d28383cc419ea34632022b54";
        String response = run("POST", source, payload, "", "application/x-www-form-urlencoded");
        bearer = "bearer " + new JSONObject(response).getString("access_token");
        return bearer;
    }

}
