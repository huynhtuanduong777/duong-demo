package apis.shop;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.XLSWorker;

public class Album extends BaseObject {
    private String versionNo;

    public Album(){

    }

    public int getAlbumId(String response){
        JSONObject obj = new JSONObject(response);
        int result = (int) obj.get("result");
        return result;
    }

    public String getAlbumList(String token){
        String source = getSellerUrl("ENDPOINT_ALBUM_GETLIST");
        return run("GET", source, "N/A",token);
    }

    public String getAlbumDetail(int albumId, String token){
        String source = getSellerUrl("ENDPOINT_ALBUM_GETIMAGES") + "?albumId=" + albumId + "&pageSize=20&pageIndex=0";
        return run("GET", source, "N/A", token);
    }

    public boolean verifyAlbumIdInList(int albumId, String response){
        if(response==null)
            return false;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        for(int i=0; i<result.length(); i++){
            JSONObject album = (JSONObject) result.get(i);
            int id = (int) album.get("Id");
            String name = (String) album.get("Name");
            if(albumId == id){
                versionNo = (String) album.get("VersionNo");
                return true;
            }
        }
        message = "Cannot find album Id in list";
        return false;
    }

    public boolean verifyAlbumIdInDetail(int albumId, String response){
        if(response==null)
            return false;
        JSONObject obj = new JSONObject(response);
        JSONObject result = (JSONObject) obj.get("result");
        JSONArray list = (JSONArray) result.get("List");
        for(int i=0 ; i<list.length(); i++){
            JSONObject image = (JSONObject) list.get(i);
            int id = (int) image.get("AlbumId");
            if(albumId!=id){
                message = "Image id is not correct!";
                return false;
            }
        }
        return true;
    }

    public boolean deleteAlbum(int albumId, String versionNo, String token){
        String source = getSellerUrl("ENDPOINT_ALBUM_DELETEALBUM") + "?albumid=" + albumId + "&versionNo=" + versionNo;
        String response = run("POST",source, "", token);
        if(response!=null)
            return true;
        message = "Delete album is failed!";
        return false;
    }

    public boolean verifyAlbumNotInList(int albumId, String response){
        if(response==null)
            return false;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        for(int i=0; i<result.length(); i++){
            JSONObject album = (JSONObject) result.get(i);
            int id = (int) album.get("Id");
            String name = (String) album.get("Name");
            if(albumId == id){
                message = "Album is still exists!";
                return false;
            }

        }
        return true;
    }

    public String getVersionNo(){
        return versionNo;
    }

    public int getImageId(String response){
        JSONObject obj = new JSONObject(response);
        return (int) obj.get("result");
    }

    public boolean verifyImageInAlbum(int imageId, String response){
        if(response==null)
            return false;
        JSONObject obj = new JSONObject(response);
        JSONObject result = (JSONObject) obj.get("result");
        JSONArray list = (JSONArray) result.get("List");
        for(int i=0 ; i<list.length(); i++){
            JSONObject image = (JSONObject) list.get(i);
            int id = (int) image.get("Id");
            System.out.println(imageId + "-" + id);
            if(imageId==id)
                return true;
        }
        message = "Cannot find image in album";
        return false;
    }

    public boolean verifyImageNotInAlbum(int imageId, String response){
        if(response==null)
            return false;
        JSONObject obj = new JSONObject(response);
        JSONObject result = (JSONObject) obj.get("result");
        JSONArray list = (JSONArray) result.get("List");
        for(int i=0 ; i<list.length(); i++){
            JSONObject image = (JSONObject) list.get(i);
            int id = (int) image.get("Id");
            if(imageId==id){
                message = "Image still in album";
                return false;
            }
        }
        return true;
    }

    public boolean moveImage(int imageId, int albumId, String token){
        String source = getSellerUrl("ENDPOINT_ALBUM_MOVEIMAGE");
        JSONObject obj = new JSONObject();
        JSONArray ids = new JSONArray();
        ids.put(imageId);
        obj.put("PictureIds", ids);
        obj.put("albumid", albumId);
        String response = run("POST", source, obj.toString(), token);
        if(response!=null)
            return true;
        message = "Move image is failed!";
        return false;
    }

    public boolean deleteImage(int imageId, int albumId, String token){
        String source = getSellerUrl("ENDPOINT_ALBUM_DELETEIMAGE");
        JSONArray array = new JSONArray();
        JSONObject obj = new JSONObject();
        obj.put("Id", imageId);
        obj.put("AlbumId", albumId);
        array.put(obj);
        String response = run("POST", source, array.toString(), token);
        if(response!=null)
            return true;
        message = "Delete image is failed!";
        return false;
    }

}
