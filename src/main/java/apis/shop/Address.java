package apis.shop;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;

public class Address extends BaseObject {
    private String baseApi;
    private String region;
    private String district;
    private String ward;

    public Address(){
        baseApi = getSellerUrl("BASE_URL");
        region = getSellerUrl("ENDPOINT_API_ADDRESS_REGION");
        district = getSellerUrl("ENDPOINT_API_ADDRESS_DISTRICT");
        ward = getSellerUrl("ENDPOINT_API_ADDRESS_WARD");
    }

    public String getRegionList(String token){
        String source = baseApi + region;
        System.out.println(source);
        return run("GET", source, "N/A", token);
    }

    public String getDistrictListByRegion(int regionId, String token){
        String source = baseApi + region;
        return run("GET", source, "N/A", token);
    }

    public String getWardListByDistrict(int districtId, String token){
        String source = baseApi + region;
        return run("GET", source, "N/A", token);
    }

    public boolean verifyList(String response){
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        if(result.length() > 0)
            return true;
        message = "Cannot load region list";
        return false;
    }



    public int getRegionIdByName(String name, String token){
        String source = baseApi + region + "?name=" + "Hồ Chí Minh";
        String response = run("GET", source, "N/A", token);
        if(response==null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        JSONObject region = (JSONObject) result.get(0);
        return (int) region.get("id");
    }

    public int getDistrictIdByName(String name, String token){
        String source = baseApi + district + "?name=" + name;
        String response = run("GET", source, "N/A", token);
        if(response==null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        JSONObject region = (JSONObject) result.get(0);
        return (int) region.get("id");
    }

    public int getWardIdByName(String name, String token){
        String source = baseApi + district + "?name=" + name;
        String response = run("GET", source, "N/A", token);
        if(response==null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        JSONObject region = (JSONObject) result.get(0);
        return (int) region.get("id");
    }
}
