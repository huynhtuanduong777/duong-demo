package apis.shop;

import object.BaseObject;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;
import utils.JSON;

public class ShopConfig extends BaseObject {
//    private String base_api;
//
//    public ShopConfig(){
//        base_api = getSellerUrl("BASE_URL");
//    }
	
	public String getResponseMultiLine(String url, String method, String token, String excelbody) {
		try {			
			// Nếu có truyền body dùng phương thức POST
			if (method.contains("POST")) {				
				response = runGetMultiLine("POST", url, excelbody, token);
			}
			// Nếu không truyền body dùng phương thức GET
			else if (method.contains("GET")) {							
				response = runGetMultiLine("GET", url, "N/A", token);
			} else if (method.contains("PUT")) {				
				response = run("PUT", url, excelbody, token);
			}			
			else if (method.contains("DELETE")) {												
				response = run("DELETE", url, excelbody, token);
			}
			
		} catch (Exception ex) {
			jsonActualResult = ex.toString();
			System.out.print("Error send request: " + jsonActualResult);
		}
		return response;
	}
	
    public boolean loadInstallment(String token){ //tra gop
        String source = getSellerUrl("ENDPOINT_API_WEB_INSTALLMENT");
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load installment is failed!";
            return false;
        }
        JSONObject obj = new JSONObject(response);
        JSONObject result = (JSONObject) obj.get("result");
        JSONArray terms = (JSONArray) result.get("installmentTerms");
        if(terms.length() > 0)
            return true;
        message = "Load installment terms is failed!";
        return false;
    }

    public boolean loadPayment(String token){ //hinh thuc thanh toan
        String source = getSellerUrl("ENDPOINT_API_WEB_PAYMENT");
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load payment is failed!";
            return false;
        }
        return true;
    }

    public boolean loadShippingConfig(String token){ //mien phi van chuyen
        String source = getSellerUrl("ENDPOINT_API_WEB_SUBSCRIBESHIPPINGCONFIG");
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load shipping config is failed!";
            return false;
        }
        return true;
    }

    public boolean loadPaymentOnline(String token){ //ho tro thanh toan truc tuyen
        String source = getSellerUrl("ENDPOINT_API_WEB_SUPPORTPAYMENTONLINE");
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load payment is failed!";
            return false;
        }
        return true;
    }

    public boolean loadMobilePayment(String token){ //ho tro giam gia mua hang qua mobile
        String source = getSellerUrl("ENDPOINT_API_WEB_MOBIPAYMENT");
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load mobile payment is failed!";
            return false;
        }
        return true;
    }

    public boolean verifyInstallment(){
        return true;
    }
    
    public boolean loadAllowCheck(String code, String token){ //kiem hang
        String source = getSellerUrl("ENDPOINT_API_WEB_STOREALLOWCHECK") + code;
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load allow check is failed!";
            return false;
        }
        return true;
    }
    
    public boolean verifyAllowCheck(String token,boolean expectedResult){ //verify cau hinh kiem hang thanh cong
        String source = getSellerUrl("ENDPOINT_API_WEB_STOREALLOWCHECK");
        String response = run("GET", source, "N/A", token);
        JSONObject obj = new JSONObject(response);
        boolean actualResult = obj.getJSONObject("result").getBoolean("allowCheck");
        return (actualResult==expectedResult);
    }
    
    public boolean loadStoreConfigPartner(String token){ //partner dang ky su dung api
        String source = getSellerUrl("ENDPOINT_API_PARTNER_STORECONFIGPARTNER");
        String response = run("GET", source, "N/A", token);
        if(response == null){
            message = "Load StoreConfigPartner is failed!";
            return false;
        }
        return true;
    }
    
    public int getTimeConfigAppAfter7Days(String response) {
    	JSONObject obj = new JSONObject(response);
    	double updatedDate = obj.getJSONObject("data").getDouble("updated_date");
    	double newDate = updatedDate+691200;
    	int intNewDate = (int) newDate;
    	return intNewDate;
    }
      
    public boolean currentBeforeCompareTime(int dateCompare) {		
		long tsNow = System.currentTimeMillis()/1000;
		int tsCompare = dateCompare;
		Timestamp now = new Timestamp(tsNow);
		Timestamp com = new Timestamp(tsCompare);
		if(now.before(com)) {
			return true;
		}
		else
			return false;
	}

    public int getTimeConfigInstallmentAfter7Days(String response) {
    	long time=0;
    	try {
    	JSONObject obj = new JSONObject(response);
    	String str_updatedDate = obj.getJSONObject("result").getString("updatedDate");
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	Date updatedDate = df.parse(str_updatedDate);
    	Calendar c = Calendar.getInstance();
        c.setTime(updatedDate);
        c.add(Calendar.DATE,8);
        time = c.getTimeInMillis()/1000;
		} catch (ParseException e) {
			// TODO Auto-generated catch block				
			e.printStackTrace();
		}
    	int intTime = (int) time;
        System.out.println("Time allow update installment "+intTime);
    	return intTime;  	
    }
    
    public long getCurrentTimeStamp() {
    	long tsNow = System.currentTimeMillis()/1000;
    	return tsNow;
    }

	public int getTimeConfigPayLaterAfter7Days(String response) {
		// TODO Auto-generated method stub
		long time=0;
    	try {
    	JSONObject obj = new JSONObject(response);
    	String str_updatedDate = obj.getJSONObject("result").getString("updated_date");
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	Date updatedDate = df.parse(str_updatedDate);
    	Calendar c = Calendar.getInstance();
        c.setTime(updatedDate);
        c.add(Calendar.DATE,8);
        time = c.getTimeInMillis()/1000;
		} catch (ParseException e) {
			// TODO Auto-generated catch block				
			e.printStackTrace();
		}
    	int intTime = (int) time;
    	return intTime;  	
	}
	
	public int getTimeExpiredShopPlus(String response) {
		// TODO Auto-generated method stub
		long time=0;
    	try {
    	JSONObject obj = new JSONObject(response);
    	String str_updatedDate = obj.getJSONObject("result").getString("expire_date");
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	Date updatedDate = df.parse(str_updatedDate);
    	Calendar c = Calendar.getInstance();
        c.setTime(updatedDate);
        time = c.getTimeInMillis()/1000;
		} catch (ParseException e) {
			// TODO Auto-generated catch block				
			e.printStackTrace();
		}
    	int expiredTime = (int) time;
    	System.out.println("Cancel expired time shop plus"+expiredTime);
    	return expiredTime;
	}

	public String getCarrierConfig(String expect,String response){
        String[] arrExpect = expect.split("\":\"");
        arrExpect[0] = arrExpect[0].replace("\"", "");
        arrExpect[1] = arrExpect[1].replace("\"", "");
        JSONObject obj = new JSONObject(response);
        return JSON.searchInArray(obj.getJSONArray("result"),arrExpect[0],arrExpect[1]).toString();
    }

    public JSONArray getBroadcastConfig(String expect,String response){
        String[] arrExpect = expect.split("\":\"");
        arrExpect[0] = arrExpect[0].replace("\"", "");
        arrExpect[1] = arrExpect[1].replace("\"", "");
        JSONObject obj = new JSONObject(response);
        JSONArray arrBroadcastMsg = JSON.searchInArray(obj.getJSONArray("result"),arrExpect[0],arrExpect[1]);
        if(arrBroadcastMsg.length()==0){
            System.out.println("Fail to get broadcast config");
            return null;
        }else {
            return arrBroadcastMsg;
        }
    }

    public int getTimeAllowSendBroadcastAfter7Days(JSONArray arr) {
        long time=0;
        try {
            String dateSchedule = arr.getJSONObject(0).getString("schedule_date");
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date updatedDate = df.parse(dateSchedule);
            Calendar c = Calendar.getInstance();
            c.setTime(updatedDate);
            c.add(Calendar.DATE,7);
            c.add(Calendar.HOUR,7);
            time = c.getTimeInMillis()/1000;

        }catch (ParseException e){
            e.printStackTrace();
        }
        int intTime = (int) time;
        System.out.println("Time allow new send broadcast message: "+intTime);
        return intTime;
    }

    public void setMessage(String message){
        this.message=message;
    }
}
