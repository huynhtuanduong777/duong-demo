package apis.shop;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Parameters;
import utils.Constants;
import utils.XLSWorker;

public class Blog extends BaseObject {
    String sheetName;

    public Blog(){
        sheetName = "Blog";
    }

    public String getBlogId(String actualResponse){
        String jsonPath = XLSWorker.getValueFromExcel(header, "Id",1,"JsonPath");
        return readValueByJsonPath(actualResponse, jsonPath);
    }

    public String loadBlogList(String token){
        String method = "GET";
        String source = getSellerUrl("ENDPOINT_API_WEB_BLOG") + "/SearchBlog";
        return run(method, source, "", token);
    }

    public String loadBlogDetail(String blogId, String token){
        String method = "GET";
        String source = getSellerUrl("ENDPOINT_API_WEB_BLOG") + "/" + blogId;
        return run(method, source, "", token);
    }

    public String getVersionNo(String actualResponse){
        String jsonPath = XLSWorker.getValueFromExcel(header,"VersionNo",1,"JsonPath");
        return readValueByJsonPath(actualResponse, jsonPath);
    }

    public boolean verify(String blogId, String response){
        if(blogId==null){
            message = "Cannot get blog Id";
            return false;
        }
        if(response==null){
            message = "Cannot get blog list";
            return false;
        }
        JSONObject fullResponse = new JSONObject(response);
        JSONObject result = (JSONObject) fullResponse.get("result");
        JSONArray datas = (JSONArray) result.get("data");
        for(int i=0; i<datas.length(); i++){
            JSONObject data = (JSONObject) datas.get(i);
            String id = data.get("id").toString();
            if(blogId.equals(id))
                return true;
        }
        message = "Cannot find blog Id in blog List!";
        return false;
    }

    public boolean deleteBlog(String blogId, String token){
        String source = getSellerUrl("ENDPOINT_API_WEB_BLOG");
        JSONArray blog = new JSONArray();
        blog.put(blogId);
        String resDelBlog = run("DELETE",source, blog.toString(), token);
        if(resDelBlog!=null)
            return true;
        message = "Delete Blog is failed!";
        return false;
    }
}