package apis.shop;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.XLSWorker;

public class ShopInfo extends BaseObject {
    String sheetName;

    public ShopInfo(){

    }

    public String getShopInfo(String token){
        String source = getSellerUrl("ENDPOINT_GETSHOPINFO");
        return run("POST", source, "{}",token);
    }

    public String getShopInfoOnMobile(String token){
        String source = getSellerUrl("ENDPOINT_MOBILE_STORE");
        return run("GET", source, "{}",token);
    }

    public boolean verifyShopInfo(String response){
        if(response==null){
            message = "Load shopinfo is failed";
            return false;
        }
        return true;
    }

    public String getShopId(String actualResponse){
        String jsonPath = XLSWorker.getValueFromExcel(header, "ShopId",1,"JsonPath");
        return readValueByJsonPath(actualResponse, jsonPath);
    }

    public String getShopId(String key, String actualResponse){
        String jsonPath = XLSWorker.getValueFromExcel(header, key,1,"JsonPath");
        return readValueByJsonPath(actualResponse, jsonPath);
    }

    public String getVersionNo(String actualResponse){
        String jsonPath = XLSWorker.getValueFromExcel(header, "ShopVersionNo",1,"JsonPath");
        return readValueByJsonPath(actualResponse, jsonPath);
    }

    public String getVersionNo(String key, String actualResponse){
        String jsonPath = XLSWorker.getValueFromExcel(header, key,1,"JsonPath");
        return readValueByJsonPath(actualResponse, jsonPath);
    }

    public int getRegionIdByName(String regionName, String token){
        String source = getSellerUrl("ENDPOINT_API_ADDRESS_REGION");
        String response = run("GET", source, "", token);
        if(response==null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        for(int i=0; i<result.length(); i++){
            JSONObject region = (JSONObject) result.get(i);
            String name = (String) region.get("name");
            if(regionName.equals(name))
                return (int) region.get("id");
        }
        message = "Cannot get region id";
        return -1;
    }

    public int getDictrictIdByName(int regionId, String districtName, String token){
        String source = getSellerUrl("ENDPOINT_API_ADDRESS_DISTRICT") + "?id=" + regionId;
        String response = run("GET", source, "", token);
        if(response==null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        for(int i=0; i<result.length(); i++){
            JSONObject district = (JSONObject) result.get(i);
            String name = (String) district.get("name");
            if(districtName.equals(name))
                return (int) district.get("id");
        }
        message = "Cannot get district id";
        return -1;
    }

    public int getWardIdByName(int districtId, String wardName, String token){
        String source = getSellerUrl("ENDPOINT_API_ADDRESS_WARD") + "?id=" + districtId;
        String response = run("GET", source, "", token);
        if(response==null)
            return -1;
        JSONObject obj = new JSONObject(response);
        JSONArray result = (JSONArray) obj.get("result");
        for(int i=0; i<result.length(); i++){
            JSONObject ward = (JSONObject) result.get(i);
            String name = (String) ward.get("name");
            if(wardName.equals(name))
                return (int) ward.get("id");
        }
        message = "Cannot get ward id";
        return -1;
    }
}
