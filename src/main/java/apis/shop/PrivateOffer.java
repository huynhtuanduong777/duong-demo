package apis.shop;

import object.BaseObject;
import org.json.JSONArray;
import org.json.JSONObject;

public class PrivateOffer extends BaseObject {

    public PrivateOffer(){

    }

    public String getListPagingGroupPrivateOfferWithProduct(String token){
        String source = getSellerUrl("ENDPOINT_PRIVATEOFFER_GETLISTINGPAGINGGROUP");
        JSONObject page = new JSONObject();
        page.put("CurrentPage", 1);
        page.put("PageSize", 10);
        JSONObject obj = new JSONObject();
        obj.put("Page", page);

        return run("POST", source, obj.toString(), token);
    }

    public boolean verifyListPagingGroupPrivateOfferWithProduct(int buyerSendoId, JSONObject response){
        JSONObject data = response.getJSONObject("Data");
        JSONArray datas = data.getJSONArray("Data");
        for(int i=0; i<datas.length(); i++){
            JSONObject product = (JSONObject) datas.get(i);
            int buyer_sendo_id = product.getInt("BuyerSendoId");
            if(buyerSendoId == buyer_sendo_id)
                return true;
        }
        message = "Private Offer is not correct in listing group!";
        return false;
    }

    public int getPrivateOfferId(int buyerSendoId, JSONObject response){
        JSONObject data = response.getJSONObject("Data");
        JSONArray datas = data.getJSONArray("Data");
        for(int i=0; i<datas.length(); i++){
            JSONObject product = (JSONObject) datas.get(i);
            int buyer_sendo_id = product.getInt("BuyerSendoId");
            if(buyerSendoId == buyer_sendo_id){
                int id = product.getInt("Id");
                return id;
            }
        }
        return -1;
    }

    public String getListPagingPrivateOfferWithProduct(int buyerSendoId, String token){
        String source = getSellerUrl("ENDPOINT_PRIVATEOFFER_GETLISTINGPAGING");
        JSONObject page = new JSONObject();
        page.put("CurrentPage", 1);
        page.put("PageSize", 10);
        JSONObject obj = new JSONObject();
        obj.put("Page", page);
        obj.put("BuyerSendoId", buyerSendoId);

        return run("POST", source, obj.toString(), token);
    }

    public boolean verifyListPagingPrivateOfferWithProduct(int poId, JSONObject response){
        JSONObject data = response.getJSONObject("Data");
        JSONArray datas = data.getJSONArray("Data");
        for(int i=0; i<datas.length(); i++){
            JSONObject product = (JSONObject) datas.get(i);
            int id = product.getInt("Id");
            if(poId == id)
                return true;
        }
        message = "Private Offer is not correct in listing!";
        return false;
    }

    public String getPrivateOfferDetailById(int poId, String token){
        String source = getSellerUrl("ENDPOINT_PRIVATEOFFER_GETDETAIL");
        JSONObject obj = new JSONObject();
        obj.put("id", poId);

        return run("POST", source, obj.toString(), token);
    }

    public boolean verifyPrivateOfferDetailById(int poId, JSONObject response){
        JSONObject data = response.getJSONObject("Data");
        int id = data.getInt("Id");
        if(id == poId)
            return true;
        message = "";
        return false;
    }

}
