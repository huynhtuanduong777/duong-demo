package apis.promotion;

import object.BaseObject;

public class Promotion extends BaseObject{
    public String productSaleEvent;
    public String getPromotionResponse(String url, String method, String token, String excelbody) {
        try {
            switch (method) {
                case "POST":
                    if (excelbody.contains("SET_PRODUCT_SALEEVENT")) {
                        excelbody = excelbody.replace("SET_PRODUCT_SALEEVENT",productSaleEvent);
                    }
//				excelbody = replaceTextDateTime(excelbody,"SKU_DATE_SUB1",-1,"yyyy-MM-dd");
                    if (token==null) {
                        System.out.println("Sai thông tin đăng nhập");
                        jsonActualResult = "Sai thông tin đăng nhập";
                        return "Invalid Credetial";
                    }
                    jsonInput = excelbody;
                    response = run("POST", url, excelbody, token);
                    break;
                case "GET":
                    if (url.contains("CurrentEpochDate+600")) {
                        String date = String.valueOf(getDaysEpoch(600));
                        url = url.replace("CurrentEpochDate+600", date);
                    }
                    if (url.contains("CurrentEpochDate+900")) {
                        String date = String.valueOf(getDaysEpoch(900));
                        url = url.replace("CurrentEpochDate+900", date);
                    }
                    if (url.contains("CurrentEpochDate+30")) {
                        String date = String.valueOf(getDaysEpoch(30));
                        url = url.replace("CurrentEpochDate+30", date);
                    }
                    if (url.contains("CurrentEpochDate")) {
                        String date = String.valueOf(getDaysEpoch(0));
                        url = url.replace("CurrentEpochDate", date);
                    }
                    response = run("GET", url, "N/A", token);
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            jsonActualResult = ex.toString();
            response = ex.toString();
            System.out.print("Error send request: " + jsonActualResult);
        }
        return response;
    }
}
