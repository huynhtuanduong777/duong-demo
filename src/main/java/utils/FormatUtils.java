package utils;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

import java.text.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class FormatUtils {

    public static String formatDate(Date date, String format) {
        DateFormat simpleDate = new SimpleDateFormat(format);
        String formattedDate = simpleDate.format(date);
        return formattedDate;
    }

    public static String getCurrentTimeByTimezoneOffset(int timezoneOffset, String format) {
        ZoneOffset offset = ZoneOffset.ofHours(timezoneOffset);
        OffsetDateTime odt = OffsetDateTime.now(offset);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        String output = odt.format(formatter);
        return output;
    }

    public static String getCurrentDateTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        return formatter.format(date);
    }

    public static String getCurrentDateTime2(){
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMHHmmss");
        Date date = new Date();
        return formatter.format(date);
    }
    
    public static String getCurrentDate() {
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        Calendar c = Calendar.getInstance(); 
        c.setTime(date); 
        c.add(Calendar.DATE, 1);
        date = c.getTime();
        String output = formatter.format(date);
        return output;
    }
    
    public static String getToday() {
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String output = formatter.format(date);
        return output;
    }

    public static String getToday2() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String output = formatter.format(date);
        return output;
    }

    public static String getToday3(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String output = formatter.format(date);
        return output;
    }

    public static Integer GenerateNumber(int limit)
    {
        Random rand = new Random();
        int Low = 1;
        int High = limit;
        int Result = rand.nextInt(High-Low) + Low;
        return Result;
    }
    
    public static String getSaltString(int limit) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < limit) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    /*public String getCurrentDateTime(){
        Calendar cal = Calendar.getInstance();
        String time = cal.getTime().toString().replace(MyConstants.SLASH,"-").replace(MyConstants.SPACE,"_").replace(":","-");
        MyLogger.info("Current time is:" + MyConstants.SPACE + time);
        return time;
    }*/

    public static String matchAndReplaceNonEnglishChar(String subjectString)
    {
        subjectString = Normalizer.normalize(subjectString, Normalizer.Form.NFD);
        String resultString = subjectString.replaceAll("[^\\x00-\\x7F]", "").replaceAll(" ","-").replaceAll(",-","/").toLowerCase();
        return resultString;
    }

    public static String formatNumber(String number, String format){
        NumberFormat formatter = new DecimalFormat(format);
        return formatter.format(Long.valueOf(number));
    }
    
    public static String removeSpecialChar(String text) { 
    	String newText = text.replaceAll("[\"{}\"]","").replaceAll("\\s+","");
    	return newText;
    }
    public static String removeDoubleQuote(String text) { 
    	String newText = text.replaceAll("\"","");
    	return newText;
    }

    @Test
    public static Boolean checkDate(String from, String to, String date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date fromDate = null;
        Date toDate = null;
        Date productDate = null;
        try {
            fromDate = sdf.parse(from);
            toDate = sdf.parse(to);
            productDate = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return fromDate.compareTo(productDate) * productDate.compareTo(toDate) >= 0;
    }
}
