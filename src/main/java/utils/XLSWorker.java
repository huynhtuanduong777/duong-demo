package utils;

import org.apache.poi.xssf.usermodel.*;

import java.io.*;

public class XLSWorker {
    private static FileInputStream fis = null;
    private static FileOutputStream fos = null;
    private static XSSFWorkbook workbook = null;


    public XLSWorker(){

    }

    public static XSSFWorkbook getWorkbook(String filePath){
        if(workbook!=null){
            return workbook;
        }
        try {
            fis = new FileInputStream(filePath);
            workbook = new XSSFWorkbook(fis);
            fis.close();
            return workbook;
        } catch (NullPointerException npointer) {
            npointer.printStackTrace();
            System.out.println("Cannot open the file because the current file path is null.");
        } catch (FileNotFoundException nfound){
            nfound.printStackTrace();
            System.out.println("Didn't find the file " + filePath + ". Please check file path or file name.");
        } catch (IOException io){
            io.printStackTrace();
        }
        return null;
    }

    public static XSSFWorkbook getWorkbook2(String filePath, String sheetName){
        try {
            if(workbook!=null){
                for(int i=0; i<workbook.getNumberOfSheets(); i++){
                    if(workbook.getSheetAt(i).getSheetName().equals(sheetName))
                        return workbook;
                }
            }
            fis = new FileInputStream(filePath);
            workbook = new XSSFWorkbook(fis);
            fis.close();
            return workbook;
        } catch (NullPointerException npointer) {
            npointer.printStackTrace();
            System.out.println("Cannot open the file because the current file path is null.");
        } catch (FileNotFoundException nfound){
            nfound.printStackTrace();
            System.out.println("Didn't find the file " + filePath + ". Please check file path or file name.");
        } catch (IOException io){
            io.printStackTrace();
        }
        return null;
    }

    public static XSSFWorkbook getInternalWorkbook(String filePath){
        XSSFWorkbook workbook = null;
        try {
            fis = new FileInputStream(filePath);
            workbook = new XSSFWorkbook(fis);
            fis.close();
        } catch (NullPointerException npointer) {
            npointer.printStackTrace();
            System.out.println("Cannot open the file because the current file path is null.");
        } catch (FileNotFoundException nfound){
            nfound.printStackTrace();
            System.out.println("Didn't find the file " + filePath + ". Please check file path or file name.");
        } catch (IOException io){
            io.printStackTrace();
        }
        return workbook;
    }

    public static XSSFSheet getSheet(XSSFWorkbook workbook, String sheetName){
        if(workbook==null)
            return null;
        try{
            return workbook.getSheetAt(workbook.getSheetIndex(sheetName));
        }catch (IllegalArgumentException illegal) {
            illegal.printStackTrace();
            System.out.println("Didn't find sheet name " + sheetName + ". Please check sheet name.");
        }
        return null;
    }

    public static Object[][] getData(XSSFWorkbook workbook, String sheetName){
        Object[][] objects = null;

        XSSFSheet actionSheet = XLSWorker.getSheet(workbook,sheetName);
        if(actionSheet==null)
            return null;
        int totalRows = actionSheet.getLastRowNum() - actionSheet.getFirstRowNum() + 1;
        int totalCols = actionSheet.getRow(0).getLastCellNum();
        objects = new Object[totalRows][totalCols];

        for(int i=0; i< totalRows; i++){
            for(int j=0; j < totalCols; j++){
                objects[i][j] = actionSheet.getRow(i).getCell(j).toString();
            }
        }
        return objects;
    }

    public static Object[][] getJsonPathData(XSSFWorkbook workbook, String sheetName){
        Object[][] objects = null;

        XSSFSheet actionSheet = XLSWorker.getSheet(workbook,sheetName);
        if(actionSheet==null)
            return null;
        int totalRows = actionSheet.getLastRowNum() - actionSheet.getFirstRowNum() + 1;
        int totalCols = actionSheet.getRow(0).getLastCellNum();
        objects = new Object[totalRows][totalCols];

        for(int i=0; i< totalRows; i++){
            for(int j=0; j < totalCols; j++){
                objects[i][j] = actionSheet.getRow(i).getCell(j).toString();
            }
        }
        return objects;
    }

    public static int getNumberOfColumns(XSSFSheet sheet){
        return sheet.getRow(0).getLastCellNum()-2;
    }

    public static Object[][] getFirstRow(XSSFWorkbook workbook, String sheetName){
        Object[][] objects = null;
        XSSFSheet actionSheet = XLSWorker.getSheet(workbook,sheetName);
        if(actionSheet==null)
            return null;

        int totalCols = actionSheet.getRow(0).getLastCellNum() - 2;
        objects = new Object[1][totalCols];

        for(int j=0; j < totalCols; j++){
            objects[0][j] = actionSheet.getRow(0).getCell(j).toString();
        }
        return objects;
    }

    public static void writeExcel(String filePath, XSSFWorkbook workbook){
        try{
            fos = new FileOutputStream(new File(filePath));
            workbook.write(fos);
            fos.close();
            System.out.println("Excel written completed. Please chect at: " + filePath);
        }catch (Exception e){
            System.out.println("Please close the result file before testing!");
        }
    }


    public static void updateExcel(XSSFSheet sheet, int rowNum, int colNum, String data){
        XSSFCell cell = null;
        XSSFRow row = sheet.getRow(rowNum);
        if(row == null)
            row = sheet.createRow(rowNum);
        cell = row.getCell(colNum);
        if(cell == null){
            cell = row.createCell(colNum);
        }
        cell.setCellValue(data);
    }

    public static void updateExcelByColumnName(XSSFSheet sheet, int rowNum, String colName, String data){
        XSSFCell cell = null;
        XSSFRow row = sheet.getRow(rowNum);
        int colNum = XLSWorker.getColumnNumByName(colName,sheet.getSheetName());
        if(row == null)
            row = sheet.createRow(rowNum);
        cell = row.getCell(colNum);
        if(cell == null){
            cell = row.createCell(colNum);
        }
        cell.setCellValue(data);
    }

    public static int getLastCellNum(XSSFSheet sheet){
        return sheet.getRow(0).getLastCellNum()-1;
    }

    public static int getLastRowNum(XSSFSheet sheet){
        return sheet.getLastRowNum();
    }

    public static int getColumnNumByName(String colName, String sheetName){
        XSSFSheet sheet = XLSWorker.getSheet(workbook,sheetName);
        XSSFRow row = sheet.getRow(0);
        int colNum = -1;
        for(int i=0; i < row.getLastCellNum();i++){
            if(row.getCell(i).getStringCellValue().trim().equals(colName))
                colNum = i;
        }
        return colNum;
    }

    public static String getValueFromExcel(int row, String colName, String sheetName){
        int colNum = XLSWorker.getColumnNumByName(colName, sheetName);
        Object[][] object = XLSWorker.getData(workbook,sheetName);
        if(object == null)
            return null;
        return Helper.decodeUnicodeString(object[row][colNum].toString());
    }

    public static String getValueFromExcel(String rowName, int col, String sheetName){
        int rowNum = getRowNumByName(rowName, sheetName);
        Object[][] object = XLSWorker.getJsonPathData(workbook,sheetName);
        if(object == null)
            return null;
        return Helper.decodeUnicodeString(object[rowNum][col].toString());
    }

    public static String getValueFromExcel(XSSFWorkbook workbook, String rowName, int col, String sheetName){
        int rowNum = getRowNumByName(workbook, rowName, sheetName);
        Object[][] object = XLSWorker.getJsonPathData(workbook,sheetName);

        if(object == null)
            return null;
        return Helper.decodeUnicodeString(object[rowNum][col].toString());
    }

    public static String getValueFromExcel(int row, int col, String sheetName){
        Object[][] object = XLSWorker.getData(workbook,sheetName);
        if(object == null)
            return null;
        return Helper.decodeUnicodeString(object[row][col].toString());
    }

    public static int getRowNumByName(String rowName, String sheetName){
        XSSFSheet sheet = XLSWorker.getSheet(workbook,sheetName);
        for(int i=0; i <= sheet.getLastRowNum();i++){
            if(sheet.getRow(i).getCell(0).getStringCellValue().trim().equals(rowName))
                return i;
        }
        return -1;
    }

    public static int getRowNumByName(String rowName, String colName, String sheetName){
        int colNum = getColumnNumByName(colName, sheetName);
        System.out.println("Col Num: " + colNum);
        XSSFSheet sheet = XLSWorker.getSheet(workbook,sheetName);
        for(int i=0; i <= sheet.getLastRowNum();i++){
            System.out.println(sheet.getRow(i).getCell(colNum).getStringCellValue().trim());
            System.out.println(rowName);
            if(sheet.getRow(i).getCell(colNum).getStringCellValue().trim().equals(rowName))
                return i;
        }
        return -1;
    }

    private static int getRowNumByName(XSSFWorkbook workbook, String rowName, String sheetName){
        XSSFSheet sheet = XLSWorker.getSheet(workbook,sheetName);
        for(int i=0; i <= sheet.getLastRowNum();i++){
            if(sheet.getRow(i).getCell(0).getStringCellValue().trim().equals(rowName))
                return i;
        }
        System.out.println("Cannot see the rowName: " + rowName + "in sheetName: " + sheetName);
        return -1;
    }
    
    public static void colorTabSheet(XSSFSheet sheet, String color) {
    	switch (color) {   	
		case "red":
			sheet.setTabColor(2);
			break;
		case "green":
			sheet.setTabColor(3);
			break;
		default:
			break;
		}  	
    }
    
    public static void setHeightRow(XSSFSheet sheet,int rownum) {
    	sheet.getRow(rownum).setHeightInPoints(20);
    }

}
