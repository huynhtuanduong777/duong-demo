package utils;

import logger.MyLogger;
import object.BaseObject;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

//import com.google.api.client.googleapis.notifications.json.JsonNotificationCallback;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;


public class APIController {

    public static String upload(String fileName, String boundaryName, List<String> contents, String source, String authorizationCode){
        System.out.println("************************************************");
        System.out.println("Start to request with authorization ...");
        System.out.println("FileName: " + fileName);
        System.out.println("Source: " + source);
        System.out.println("BoundaryName: " + boundaryName);
        System.out.println("Token: " + authorizationCode);
        assert (source != "" && source != null) : "API link is empty";

        String CRLF = "\r\n"; // Line separator required by multipart/form-data.

        // Task attachments endpoint
        File theFile = new File(Constants.IMAGE_PATH + fileName);

        HttpURLConnection connection = null;


        // A unique boundary to use for the multipart/form-data
        String boundaryNumber = Long.toHexString(System.currentTimeMillis());
        String boundaryValue = boundaryName + boundaryNumber;

        BufferedReader bufReader = null;
        String response = "";
        PrintWriter writer = null;
        try {
            connection = (HttpURLConnection) new URL(source).openConnection();

            // Construct the body of the request
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=----" + boundaryValue);

            // Set basic auth header
            connection.setRequestProperty("Authorization", authorizationCode);

            // Indicate a POST request
            connection.setDoOutput(true);

            if(authorizationCode != null) {
                connection.setRequestProperty("Authorization", "fhEKW1aHhxBOD6yG3+aOP0JP8Wlh3eSaUAS3P97cO7bWpeG2qp8qlktVkzXStNt2OzY+L68VVMKEtrdAjcp2FcTJPZOJjCKzu5IPd/GqrmOxXCzYr3AF9AMeW/CwpNd/py+yFSWvGaqxVaRSLyjI7L4raVrSnPgpwa4as3DExpY=");
                connection.setRequestProperty("Cookie","token_seller_api=" + authorizationCode);
            }

            OutputStream output = connection.getOutputStream();
            writer = new PrintWriter(new OutputStreamWriter(output, "UTF-8"));

            for(int i=0; i<contents.size(); i++){
                switch (contents.get(i)){
                    case "START":
                        writer.append("------" + boundaryValue).append(CRLF);
                        break;
                    case "END":
                        writer.append("------" + boundaryValue + "--").append(CRLF).flush();
                        break;
                    case "CRLF":
                        writer.append(CRLF).flush();
                        break;
                    case "DATA":
                        Files.copy(theFile.toPath(), output);
                        break;
                    default:
                        writer.append(contents.get(i)).append(CRLF);
                        break;
                }
            }

            writer.close();

            System.out.println(connection.getResponseCode()); // Should be 200
            System.out.println(connection.getResponseMessage());

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                bufReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            } else {
                bufReader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
        } finally {
            try {
                response = bufReader.readLine();
                if (bufReader != null) {
                    bufReader.close();
                }
                connection.disconnect();
            }catch (Exception e){
            }
        }
        if(!response.contains("\"")) {
            response = StringEscapeUtils.unescapeJava(response);
        }
        System.out.println("Get the response after run: " + response);
        System.out.println("************************************************");

        return response;
    }

    public static String uploadWithCustomHeader(String fileName, String boundaryName, List<String> contents, String source, String header){
        System.out.println("************************************************");
        System.out.println("Start to request with authorization ...");
        System.out.println("FileName: " + fileName);
        System.out.println("Source: " + source);
        System.out.println("BoundaryName: " + boundaryName);
//        System.out.println("Token: " + authorizationCode);
        assert (source != "" && source != null) : "API link is empty";

        String CRLF = "\r\n"; // Line separator required by multipart/form-data.

        // Task attachments endpoint
        File theFile = new File(Constants.IMAGE_PATH + fileName);

        HttpURLConnection connection = null;


        // A unique boundary to use for the multipart/form-data
        String boundaryNumber = Long.toHexString(System.currentTimeMillis());
        String boundaryValue = boundaryName + boundaryNumber;

        BufferedReader bufReader = null;
        String response = "";
        PrintWriter writer = null;
        try {
            connection = (HttpURLConnection) new URL(source).openConnection();

            // Construct the body of the request
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=----" + boundaryValue);

            //Set header request
            if(header.contains(";")) // Multi param header
            {
                String arrMultiParamHeader[] = header.split(";");
                for(String item:arrMultiParamHeader){
                    List<String> lstHeader = getValueParamHeader(item);
                    if(lstHeader!=null) {
                        connection.setRequestProperty(lstHeader.get(0).trim(), lstHeader.get(1).trim());
                        MyLogger.info("Header added: " + lstHeader.get(0).trim() + ": " + lstHeader.get(1).trim());
                    }
                }
            }else { //One param header
                List<String> lstHeader = getValueParamHeader(header);
                if(lstHeader!=null) {
                    connection.setRequestProperty(lstHeader.get(0).trim(), lstHeader.get(1).trim());
                    MyLogger.info("Header added: " + lstHeader.get(0).trim() + ": " + lstHeader.get(1).trim());
                }
            }

            // Indicate a POST request
            connection.setDoOutput(true);


            OutputStream output = connection.getOutputStream();
            writer = new PrintWriter(new OutputStreamWriter(output, "UTF-8"));

            for(int i=0; i<contents.size(); i++){
                switch (contents.get(i)){
                    case "START":
                        writer.append("------" + boundaryValue).append(CRLF);
                        break;
                    case "END":
                        writer.append("------" + boundaryValue + "--").append(CRLF).flush();
                        break;
                    case "CRLF":
                        writer.append(CRLF).flush();
                        break;
                    case "DATA":
                        Files.copy(theFile.toPath(), output);
                        break;
                    default:
                        writer.append(contents.get(i)).append(CRLF);
                        break;
                }
            }

            writer.close();

            System.out.println(connection.getResponseCode()); // Should be 200
            System.out.println(connection.getResponseMessage());

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                bufReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            } else {
                bufReader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
        } finally {
            try {
                response = bufReader.readLine();
                if (bufReader != null) {
                    bufReader.close();
                }
                connection.disconnect();
            }catch (Exception e){
            }
        }
        if(!response.contains("\"")) {
            response = StringEscapeUtils.unescapeJava(response);
        }
        System.out.println("Get the response after run: " + response);
        System.out.println("************************************************");

        return response;
    }

    public static String ReadBigStringIn(BufferedReader buffIn) throws IOException {
        StringBuilder everything = new StringBuilder();
        String line;
        while( (line = buffIn.readLine()) != null) {
           everything.append(line);
        }
        return everything.toString();
    }
    
    public static String sendApiRequestGetMultiline(String method, String source, String payload, String authorizationCode) {
        Helper.sleep(1000);
            MyLogger.info("************************************************");
            MyLogger.info("Start to request with authorization ...");
            MyLogger.info("Method: " + method);
            MyLogger.info("Source: " + source);
            MyLogger.info("Payload: " + payload);
            MyLogger.info("Token: " + authorizationCode);
            assert (source != "" && source != null) : "API link is empty";

            String response = "";
            URL url = null;
            BufferedReader bufReader = null;
            HttpURLConnection conn = null;
            try {
                url = new URL(source);
                conn = (HttpURLConnection) url.openConnection();

                conn.setRequestMethod(method);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept-Charset", "UTF-8");
                conn.setRequestProperty("ReCaptchaResponse", "smc@123456");
                //conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
                if(authorizationCode != null) {
                    conn.setRequestProperty("Authorization", authorizationCode);
                    conn.setRequestProperty("Cookie","token_seller_api=" + authorizationCode);
                }
                conn.setDoOutput(true);
                conn.setDoInput(true);
                if(!method.equals("GET")){
                    if(!payload.contains("N/A")){
                        DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
                        outStream.write(payload.getBytes());
                        outStream.flush();
                        outStream.close();
                    }
                }
                MyLogger.info("ResponseCode: " + conn.getResponseCode());
//                System.out.println("ResponseCode: " + conn.getResponseCode());

                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                } else {
                    bufReader = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
                BaseObject.jsonActualResult=ioe.toString();
                System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
            } finally {
                try {               
                	response = ReadBigStringIn(bufReader).replace(" ", "");
//                    response = bufReader.readLine();
                    if (bufReader != null) {
                        bufReader.close();
                    }

                    if(true){
                        MyLogger.info("Get the response after run: " + response);
                        MyLogger.info("************************************************");
                    }

                    conn.disconnect();
                }catch (Exception e){
                	BaseObject.jsonActualResult=e.toString();
                }
            }
            if(!response.contains("\"")) {
                response = StringEscapeUtils.unescapeJava(response);
            }


        return response;
    }
    
    public static String sendApiRequest(String method, String source, String payload, String authorizationCode) {
        Helper.sleep(2000);
            MyLogger.info("************************************************");
            MyLogger.info("Start to request with authorization ...");
            MyLogger.info("Method: " + method);
            MyLogger.info("Source: " + source);
            MyLogger.info("Payload: " + payload);
            MyLogger.info("Token: " + authorizationCode);
            assert (source != "" && source != null) : "API link is empty";

            String response = "";
            URL url = null;
            BufferedReader bufReader = null;
            HttpURLConnection conn = null;
            try {
                url = new URL(source);
                conn = (HttpURLConnection) url.openConnection();

                conn.setRequestMethod(method);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept-Charset", "UTF-8");
                conn.setRequestProperty("ReCaptchaResponse", "smc@123456");
                //conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
                if(authorizationCode != null) {
                    conn.setRequestProperty("Authorization", authorizationCode);
                    conn.setRequestProperty("Cookie","token_seller_api=" + authorizationCode);
                    conn.setRequestProperty("Cookie","sendo_setting_token="+authorizationCode);
                }
                conn.setDoOutput(true);
                conn.setDoInput(true);
                if(!method.equals("GET")){
                    if(!payload.contains("N/A")){
                        DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
                        outStream.write(payload.getBytes());
                        outStream.flush();
                        outStream.close();
                    }
                }
                MyLogger.info("ResponseCode: " + conn.getResponseCode());
//                System.out.println("ResponseCode: " + conn.getResponseCode());

                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                } else {
                    bufReader = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
                BaseObject.jsonActualResult=ioe.toString();
                System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
            } finally {
                try {
                    response = bufReader.readLine();
                    if (bufReader != null) {
                        bufReader.close();
                    }

                    if(true){
                        MyLogger.info("Get the response after run: " + response);
                        MyLogger.info("************************************************");
                    }

                    conn.disconnect();
                }catch (Exception e){
                	BaseObject.jsonActualResult=e.toString();
                }
            }
            if(!response.contains("\"")) {
                response = StringEscapeUtils.unescapeJava(response);
            }


        return response;
    }

    public static String sendApiRequest(String method, String source, String payload, String authorizationCode, String contentType) {
        MyLogger.info("************************************************");
        MyLogger.info("Start to request with authorization ...");
        MyLogger.info("Method: " + method);
        MyLogger.info("Source: " + source);
        MyLogger.info("Payload: " + payload);
        MyLogger.info("Token: " + authorizationCode);
        assert (source != "" && source != null) : "API link is empty";

        String response = "";
        URL url = null;
        BufferedReader bufReader = null;
        HttpURLConnection conn = null;
        try {
            url = new URL(source);
            conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod(method);
            conn.setRequestProperty("Content-Type", contentType);
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("ReCaptchaResponse", "smc@123456");
            //conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
            if(authorizationCode != null) {
                conn.setRequestProperty("Authorization", authorizationCode);
                conn.setRequestProperty("Cookie","token_seller_api=" + authorizationCode);
            }
            conn.setDoOutput(true);
            conn.setDoInput(true);
            if(!method.equals("GET")){
                if(!payload.contains("N/A")){
                    DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
                    outStream.write(payload.getBytes(StandardCharsets.UTF_8));
                    outStream.flush();
                    outStream.close();
                }
            }
            MyLogger.info("ResponseCode: " + conn.getResponseCode());
//            System.out.println("ResponseCode: " + conn.getResponseCode());

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            } else {
                bufReader = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
        } finally {
            try {
                response = bufReader.readLine();
                if (bufReader != null) {
                    bufReader.close();
                }
                conn.disconnect();
            }catch (Exception e){
            }
        }
        if(!response.contains("\"")) {
            response = StringEscapeUtils.unescapeJava(response);
        }

        MyLogger.info("Get the response after run: " + response);
        MyLogger.info("************************************************");
        return response;
    }

    public static List<String> getValueParamHeader(String strHeader){
        if(!strHeader.equals("")){
            String arrParamHeader[] = strHeader.split(":");
            List<String> lstHeader = new ArrayList<>(Arrays.asList(arrParamHeader));
            return lstHeader;
        }else
            return null;
    }


    public static String sendApiRequestWithCustomHeader(String method, String source, String payload, String authorizationCode, String header) {
        Helper.sleep(1000);
        MyLogger.info("************************************************");
        MyLogger.info("Start to request with authorization ...");
        MyLogger.info("Method: " + method);
        MyLogger.info("Source: " + source);
        MyLogger.info("Payload: " + payload);
        MyLogger.info("Token: " + authorizationCode);
        assert (source != "" && source != null) : "API link is empty";

        String response = "";
        URL url = null;
        BufferedReader bufReader = null;
        HttpURLConnection conn = null;
        try {
            url = new URL(source);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(method);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("ReCaptchaResponse", "smc@123456");

            if(header.contains(";")) // Multi param header
            {
                String arrMultiParamHeader[] = header.split(";");
                for(String item:arrMultiParamHeader){
                    List<String> lstHeader = getValueParamHeader(item);
                    if(lstHeader!=null) {
                        conn.setRequestProperty(lstHeader.get(0).trim(), lstHeader.get(1).trim());
                        MyLogger.info("Header added: " + lstHeader.get(0).trim() + ": " + lstHeader.get(1).trim());
                    }
                }
            }else { //One param header
                List<String> lstHeader = getValueParamHeader(header);
                if(lstHeader!=null) {
                    conn.setRequestProperty(lstHeader.get(0).trim(), lstHeader.get(1).trim());
                    MyLogger.info("Header added: " + lstHeader.get(0).trim() + ": " + lstHeader.get(1).trim());
                }
            }
            conn.setDoOutput(true);
            conn.setDoInput(true);
            if(!method.equals("GET")){
                if(!payload.contains("N/A")){
                    DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
                    outStream.write(payload.getBytes());
                    outStream.flush();
                    outStream.close();
                }
            }
            MyLogger.info("ResponseCode: " + conn.getResponseCode());

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            } else {
                bufReader = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            BaseObject.jsonActualResult=ioe.toString();
            System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
        } finally {
            try {
                response = bufReader.readLine();
                if (bufReader != null) {
                    bufReader.close();
                }

                if(true){
                    MyLogger.info("Get the response after run: " + response);
                    MyLogger.info("************************************************");
                }

                conn.disconnect();
            }catch (Exception e){
                BaseObject.jsonActualResult=e.toString();
            }
        }
        if(!response.contains("\"")) {
            response = StringEscapeUtils.unescapeJava(response);
        }


        return response;
    }
    public static String getAuthorizationCode(String source, String payload){
        System.out.println(source);
        System.out.println(payload);
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost(source);
            request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            request.setHeader(HttpHeaders.ACCEPT_CHARSET, "UTF-8");
            request.setHeader("ReCaptchaResponse", "smc@123456");
            StringEntity entity = new StringEntity(payload);
            request.setEntity(entity);
            HttpResponse response = httpClient.execute(request);
            String entityResponse = EntityUtils.toString(response.getEntity());
            Object obj = JSONValue.parse(entityResponse);
            JSONObject fullResponse = (JSONObject) obj;
            if(fullResponse.containsKey("result")){
                JSONObject result = (JSONObject) fullResponse.get("result");
//                if(result.containsKey("token"))
//                    return (String) result.get("token");
//                else
//                    return (String) result.get("access_token");
                if(result.containsKey("metaData")) {
                	JSONObject metadata = (JSONObject) result.get("metaData");
                	return (String) metadata.get("token");
                }else
                	return (String) result.get("token");               	               	                            
            }else{
                return (String) fullResponse.get("access_token");
            }

        }catch (Exception e){
            e.printStackTrace();
            BaseObject.jsonActualResult=e.toString();
        }
        return null;
    }
    
    public static String getAuthorizationCodeWeb(String source, String token){
        System.out.println(source);
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost(source);
            request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            request.setHeader(HttpHeaders.ACCEPT_CHARSET, "UTF-8");
            request.setHeader("ReCaptchaResponse", "smc@123456");
            request.setHeader("Cookie", "tracking_id=dd6f92e8-9b55-4697-891b-eba5133c8166; G_ENABLED_IDPS=google; gcl_au=1.1.1991234645.1609730295; ga=GA1.3.981336953.1607414564; fbp=fb.1.1609730295414.1506993376; strs=1614921563.utmcsr=netcore|utmccn=8_wm_0_PBU-flashsale-050321-1130|utmcmd=wm|utmctr=|utmcct=Wholesale; _utmz=147100981.1614933882.25.3.utmcsr=netcore|utmccn=8_wm_0_PBU-flashsale-050321-1130|utmcmd=wm|utmcct=Wholesale; hjid=2f89e867-f9e3-4c13-b38f-0da1bc518d0d; login_type=fosp; ga_28PQS3WTN0=GS1.1.1615519852.23.1.1615521197.60; gid=GA1.2.267286508.1615775593; login_type=fosp; mp_7ee9ac5438d5ed68c57319eb6bf3821f_mixpanel=%7B%22distinct_id%22%3A%20%22176f5a24794496-02c3574f9acd2d-3a3d530a-15f900-176f5a24795856%22%2C%22%24device_id%22%3A%20%22176f5a24794496-02c3574f9acd2d-3a3d530a-15f900-176f5a24795856%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fban.sendo.vn%2Fbang-tin%22%2C%22%24initial_referring_domain%22%3A%20%22ban.sendo.vn%22%7D; ga_8688HJGYCZ=GS1.1.1615882433.18.0.1615882433.60; mp_80081f125ae907ba44a53ed079af9bbd_mixpanel=%7B%22distinct_id%22%3A%20%22176cba6d072624-0a99a508d3564a-5c19341b-15f900-176cba6d074a4f%22%2C%22%24device_id%22%3A%20%22176cba6d072624-0a99a508d3564a-5c19341b-15f900-176cba6d074a4f%22%2C%22%24initial_referrer%22%3A%20%22http%3A%2F%2Fban.test.sendo.vn%2Fquan-ly-chung-tu%22%2C%22%24initial_referring_domain%22%3A%20%22ban.test.sendo.vn%22%7D; ga_KH63BV91KF=GS1.1.1615883196.99.1.1615883220.0; ga_4CDLT0PH1B=GS1.1.1615890485.86.1.1615890485.0; inside_seller_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VyTmFtZSI6ImFuaGR0bjhAc2VuZG8udm4iLCJleHAiOjE2MTU5OTQxMjMsIm9yaWdfaWF0IjoxNjE1OTY1MzIzfQ.T17bpXaNWnRvP36HTdymfMq6M4CsM4xVUn05bO79GHE; client_id=c92c79e6-06be-483e-a262-4edf9bfa7c52; _utma=147100981.981336953.1607414564.1615877939.1616039156.32; _utmc=147100981; gid=GA1.3.267286508.1615775593; _utmv=147100981.|1=01686114139%200386114139=Old%20Buyer=1; session_id=fQhYXYeozrHUrQDUOclKTMbwWAGxLBHl; csrf_token=278T7xW8luleidOJk2pV1qDqpPfPlHYlHacK+G88hY0=; ga_N328JCXCZE=GS1.1.1616039153.11.1.1616039180.0; _utmb=147100981.16.9.1616039180368; gat_gtag_UA_109320013_1=1; ga=GA1.1.981336953.1607414564; ga_977EYDESHS=GS1.1.1616040185.227.1.1616040199.0; access_token="+token);           
            HttpResponse response = httpClient.execute(request);
            String entityResponse = EntityUtils.toString(response.getEntity());
            Object obj = JSONValue.parse(entityResponse);
            JSONObject fullResponse = (JSONObject) obj;
            if(fullResponse.containsKey("result")){
                JSONObject result = (JSONObject) fullResponse.get("result");

                return (String) result.get("token");
            }else{
                return (String) fullResponse.get("access_token");
            }

        }catch (Exception e){
            e.printStackTrace();
            BaseObject.jsonActualResult=e.toString();
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
	public static String getAuthorizationCodeMobile(String source, String token){
        System.out.println(source);
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost(source);
            request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
            request.setHeader(HttpHeaders.ACCEPT_CHARSET, "UTF-8");
            request.setHeader("ReCaptchaResponse", "smc@123456");
            JSONObject body = new JSONObject();
            body.put("tokensendoid", token);           
            StringEntity entity = new StringEntity(body.toString());
            request.setEntity(entity);
            HttpResponse response = httpClient.execute(request);
            String entityResponse = EntityUtils.toString(response.getEntity());
            Object obj = JSONValue.parse(entityResponse);
            JSONObject fullResponse = (JSONObject) obj;
            if(fullResponse.containsKey("result")){
                JSONObject result = (JSONObject) fullResponse.get("result");
                JSONObject userContextDto = (JSONObject) result.get("userContextDto");
                return (String) userContextDto.get("token");
            }else{
                return (String) fullResponse.get("access_token");
            }

        }catch (Exception e){
            e.printStackTrace();
            BaseObject.jsonActualResult=e.toString();
        }
        return null;
    }
    
    
    
    public static Object getValue(String url, String id, String authorizationCode, String key){
        try{
            String source = url + id;
            String response = sendApiRequest("GET",source,"", authorizationCode);
            Object obj = JSONValue.parse(response);
            JSONObject fullResponse = (JSONObject) obj;
            JSONObject result = (JSONObject) fullResponse.get("result");
            return result.get(key);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


}