package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Helper {

    public static String decodeUnicodeString(String input) {
        String unicodeRegex = "\\\\u([0-9a-f]{4})";
        Pattern unicodePatern = Pattern.compile(unicodeRegex);
        Matcher matcher = unicodePatern.matcher(input);
        StringBuffer decodedMessage = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(decodedMessage, String.valueOf((char) Integer.parseInt(matcher.group(1), 16)));
        }
        matcher.appendTail(decodedMessage);
        return decodedMessage.toString();
    }

    public static void sleep(long miliseconds){
        try {
            System.out.println("Sleep in: " + miliseconds);
            Thread.sleep(miliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
