package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.chat.v1.HangoutsChat;
import com.google.api.services.chat.v1.model.Message;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import logger.MyLogger;

public class GoogleHangouts {
	public static final String APPLICATION_NAME = "Automation";
	public static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	public static final List<String> SCOPES = Collections.singletonList("https://www.googleapis.com/auth/chat.bot");
	public static final String SERVICE_ACCOUNT_FILE = System.getProperty("user.dir") + "//data//google_api//service_account//sendo-automation.json";

	public static GoogleCredentials getCredentials() {
		try {
			GoogleCredentials credential = GoogleCredentials.fromStream(new FileInputStream(SERVICE_ACCOUNT_FILE))
					.createScoped(SCOPES);
			return credential;
		} catch (IOException e) {
			MyLogger.error(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	public static HangoutsChat GoogleHangoutsService(GoogleCredentials credentials) {
		HttpRequestInitializer requestInitializer = new HttpCredentialsAdapter(credentials);
        HangoutsChat chatService;
		try {
			chatService = new HangoutsChat.Builder(GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance(), requestInitializer)
			        .setApplicationName(APPLICATION_NAME)
			        .build();
			return chatService;
		} catch (GeneralSecurityException | IOException e) {
			MyLogger.error(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	public static void sendMessage(HangoutsChat hangoutsChat, String roomId, Message message) {
		System.out.println("Sending Hangouts message...");
		try {
			hangoutsChat.spaces().messages().create("spaces/" + roomId, message).execute();
		} catch (IOException e) {
			MyLogger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void sendHangoutsMessage(String roomId, com.google.api.services.chat.v1.model.Message message) {
		GoogleCredentials credentials = GoogleHangouts.getCredentials();
		HangoutsChat hangoutsChat = GoogleHangouts.GoogleHangoutsService(credentials);
		GoogleHangouts.sendMessage(hangoutsChat, roomId, message);
	}
}
