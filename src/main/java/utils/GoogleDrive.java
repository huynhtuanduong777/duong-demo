package utils;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.Permission;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import logger.MyLogger;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

public class GoogleDrive {
	public static final String APPLICATION_NAME = "Sendo Ban";
	public static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	public static final String CREDENTIALS_FOLDER = System.getProperty("user.dir")
			+ "//data//google_api//googledrive//credentials//";

	public static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE_FILE);
	public static final String CLIENT_SECRET_DIR = System.getProperty("user.dir")
			+ "//client_secret.json";

	public static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		InputStream in = new FileInputStream(new java.io.File(CLIENT_SECRET_DIR));
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(CREDENTIALS_FOLDER)))
						.setAccessType("offline").build();
		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}

	public static void setPermission(Drive service, String fileId, String type, String role, String domain) {
		JsonBatchCallback<Permission> callback = new JsonBatchCallback<Permission>() {
			@Override
			public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) throws IOException {
				System.err.println(e.getMessage());
			}

			@Override
			public void onSuccess(Permission permission, HttpHeaders responseHeaders) throws IOException {
				System.out.println("Permission ID: " + permission.getId());
			}
		};
		
		BatchRequest batch = service.batch();
		Permission domainPermission = new Permission().setType("domain").setRole("writer").setDomain("sendo.vn");
		try {
			service.permissions().create(fileId, domainPermission).setFields("id").queue(batch, callback);
			batch.execute();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public static void setPhotoPermission(){
		Drive service = getDriveService();
		String photoId = "1-buFscLylWileNTRnpGy1mDFO6KE0PeB";
		setPermission(service, photoId, "domain", "writer", "sendo.vn");
	}

	public static void setExcelPermission(){
		Drive service = getDriveService();
//		String photoId = "1Hb1lWVOOFbZ4ZWZBtrLl83OP4MfvgNuh";
		String excelId = "1xPPxtg3mf3nux0Yf00yHtuZqI18CUqEC";
		setPermission(service, excelId, "domain", "writer", "sendo.vn");
	}

	public static Drive getDriveService() {
		NetHttpTransport HTTP_TRANSPORT;
		Drive service = null;
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME)
                    .build();
		} catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return service;
	}

	public static Sheets getSheetsService() throws IOException {
		NetHttpTransport HTTP_TRANSPORT;
		Sheets service = null;
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
					.setApplicationName(APPLICATION_NAME)
					.build();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return service;
	}

	public static List<List<Object>> getSpreadSheetRecords(String spreadsheetId, String range) throws IOException {
		Sheets service = getSheetsService();
		ValueRange response = service.spreadsheets().values()
				.get(spreadsheetId, range)
				.execute();
		List<List<Object>> values = response.getValues();
		if (values != null && values.size() != 0) {
			return values;
		} else {
			System.out.println("No data found.");
			return null;
		}
	}
	
	public static String uploadFile(Drive service, String fileName, String filePath) {
		File fileMetadata = new File();
		fileMetadata.setName(fileName);
		fileMetadata.setMimeType("application/vnd.google-apps.spreadsheet");

		java.io.File file = new java.io.File(filePath);
		FileContent mediaContent = new FileContent("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
				file);
		File uploadedFile = null;
		try {
			uploadedFile = service.files().create(fileMetadata, mediaContent).setFields("id").execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return uploadedFile.getId();
	}
	
	public static String uploadExcelFile(String filePath) {
		Drive service = getDriveService();
		String folderId = "1xPPxtg3mf3nux0Yf00yHtuZqI18CUqEC";
		String fileName = "REPORT_" + FormatUtils.getCurrentDateTime();

		File fileMetadata = new File();
		fileMetadata.setName(fileName);
		fileMetadata.setParents(Collections.singletonList(folderId));
		fileMetadata.setMimeType("application/vnd.google-apps.spreadsheet");

		java.io.File file = new java.io.File(filePath);
		FileContent mediaContent = new FileContent("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
				file);
		File uploadedFile = null;
		try {
			uploadedFile = service.files().create(fileMetadata, mediaContent).setFields("id, parents").execute();
		} catch (IOException e) {
			MyLogger.error("The error occurs during uploading excel file to google drive!");
			System.out.println(e);
		}
		MyLogger.info("Start to upload to google drive with file: " + filePath);
		return uploadedFile.getId();
	}



	public static String uploadScreenshot(String filePath){

		Drive service = getDriveService();
		String folderId = "12ngX-Q5SAu5Nv1Ywsqmu48FwBOv8z_d8";
		String fileName = "SCREENSHOT_" + FormatUtils.getCurrentDateTime();

		File fileMetadata = new File();
		fileMetadata.setName(fileName);
		fileMetadata.setParents(Collections.singletonList(folderId));
		java.io.File file = new java.io.File(filePath);
		FileContent mediaContent = new FileContent("image/jpeg", file);
		File uploadFile = null;
		try {
			uploadFile = service.files().create(fileMetadata, mediaContent)
					.setFields("id")
					.execute();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return uploadFile.getId();
	}

	public static String uploadLog(String filePath){

		Drive service = getDriveService();
		String folderId = "1ZVUxkUAqtqfd8bzPFLbCYZEyHDRrED-6";
		String fileName = "LOG_" + FormatUtils.getCurrentDateTime();

		File fileMetadata = new File();
		fileMetadata.setName(fileName);
		fileMetadata.setParents(Collections.singletonList(folderId));
		java.io.File file = new java.io.File(filePath);
		FileContent mediaContent = new FileContent("text/plain", file);
		File uploadFile = null;
		try {
			uploadFile = service.files().create(fileMetadata, mediaContent)
					.setFields("id")
					.execute();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		String logId = uploadFile.getId();
		setPermission(service, logId, "domain", "writer", "sendo.vn");
		return logId;
	}

	public static void main(String[] args) throws GeneralSecurityException, IOException {
		/*String folderId = "0B5bXg5Yj_gbCbEx4d05WcGZWa0E";
		Drive service = getDriveService();
		//String fileId = uploadFile(service, folderId, "Order Info", "D://OrderInfo_20-07-2018_08.56.29.xlsx");
		//setPermission(service, fileId, "domain", "writer", "sendo.vn");
		String photoId = uploadScreenshot("D://sendo.jpg");
		setPermission(service, photoId, "domain", "writer", "sendo.vn");
		System.out.println(photoId);
		String photoUrl = "https://drive.google.com/open?id=" + photoId;
		System.out.println("URL: " + photoUrl);*/

		String spreadsheetId = "109QBuoNyXYdz6SGCw2kTkYd9wWa3YozcTfSeseVmtnE";
		String range = "'Sheet1'!A1:B2";
		List<List<Object>> values = getSpreadSheetRecords(spreadsheetId, range);
		for (List<Object> row : values) {
			System.out.println(row.get(0));
		}
	}
}
