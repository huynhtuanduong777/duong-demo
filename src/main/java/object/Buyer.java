package object;

import org.apache.http.HttpResponse;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Test;
import utils.APIController;
import utils.Constants;
import utils.XLSWorker;

public class Buyer extends BaseObject {
    XSSFWorkbook header = null;
    String username = "0796518143";
    String password = "123456";

    public Buyer(){
        header = XLSWorker.getInternalWorkbook(Constants.INPUT_PATH + "header_buyer.xlsx");
    }

    public String login(JSONObject input) {
        //String source = "https://mapi-stg.sendo.vn/mob/user/login";
        String source = getSellerUrl(header, "ENDPOINT_MAPI_USER_LOGIN");
        return run("POST", source, input.getJSONObject("login").toString(), "");
    }

    public String addToCart(JSONObject input, String token){

        String hash = "";
        JSONObject productInfo = new JSONObject();
        productInfo.put("from_source", 3);
        productInfo.put("shop_id", input.get("shop_id"));
        productInfo.put("buynow", 1);
        productInfo.put("device_id", "38857E45-2D4C-4FFC-AB9A-5533C28EFFB7");
        productInfo.put("qty", input.get("qty"));
        productInfo.put("product_id", input.get("product_id"));

//        String source = "https://stg.sendo.vn/m/wap_v2/cart/add";
        String source = getSellerUrl(header, "ENDPOINT_WAP_CART_ADD");

        String output = run("POST", source, productInfo.toString(), token);

        try {
            hash = new JSONObject(output).getJSONObject("result").getJSONObject("data").getString("hash");
        } catch (Exception e) {
            hash = "";
        }

        return hash;
    }

    public JSONObject getCheckoutInfo(JSONObject cart_input, String hash, String token) {
        JSONObject orderInput = new JSONObject();
        JSONArray productHash = new JSONArray();
        productHash.put(hash);
        orderInput.put("product_hashes", productHash);
        orderInput.put("shop_id", cart_input.get("shop_id"));
        orderInput.put("sendo_platform", "desktop2");

        String source = getSellerUrl(header, "ENDPOINT_API_CHECKOUT_INFO");

        String response = run("POST", source, orderInput.toString(), token);

        return new JSONObject(response);

    }

    public String saveOrder(int shop_id, int product_id, int final_price) {

        JSONObject testCaseInput = new JSONObject(
                "  {\"login\":{\"password\":" + password + ",\"username\":" + username + "},\"order\":{\"hash\":\"\",\"shop_id\":" + shop_id + ",\"qty\":1,\"product_id\":" + product_id + ",\"final_price\":" + final_price + "}}");

        String token = new JSONObject(login(testCaseInput)).getString("access_token");

        JSONObject cart_input = testCaseInput.getJSONObject("order");

        String hash = addToCart(cart_input, token);

        JSONObject checkout_info = getCheckoutInfo(cart_input, hash, token);

        String curent_address = getValueOfJson(checkout_info, "current_address_id").get(0);

        String current_carrier = getValueOfJson(checkout_info, "current_carrier").get(0);

        JSONObject order_input = new JSONObject();
        order_input.put("sendo_platform", "desktop2");
        order_input.put("device_id", "38857E45-2D4C-4FFC-AB9A-5533C28EFFB7");
        JSONObject order_input_payment = new JSONObject("{\"method\":\"cod_payment\"}");
        order_input.put("current_payment_method", order_input_payment);
        order_input.put("shop_id", cart_input.get("shop_id"));
        order_input.put("current_carrier", current_carrier);
        order_input.put("current_address_id", Integer.valueOf(curent_address));
        JSONObject order_input_product = new JSONObject();
        order_input_product.put("product_id", cart_input.get("product_id"));
        order_input_product.put("final_price", cart_input.get("final_price"));
        order_input_product.put("hash", hash);
        order_input.put("version", 3.3);

        JSONArray order_input_product_array = new JSONArray();
        order_input_product_array.put(order_input_product);
        order_input.put("current_products", order_input_product_array);

        String source = getSellerUrl(header,"ENDPOINT_API_CHECKOUT_SAVEORDER");

        String response = run("POST", source, order_input.toString(), token);

        return response;
    }

    public void likeProduct(int product_id){
        JSONObject testCaseInput = new JSONObject(
                "  {\"login\":{\"password\":\"123456\",\"username\":\"newemail08@gmail.com\"}}");

        String token = new JSONObject(login(testCaseInput)).getString("access_token");

        String source = getSellerUrl(header,"ENDPOINT_WAP_PRODUCT_LIKE") + product_id;

        String response = run("GET", source, "N/A", token);
    }

    public String getUserId(){
        JSONObject testCaseInput = new JSONObject(
                "  {\"login\":{\"password\":\"123456\",\"username\":\"newemail08@gmail.com\"}}");
        return new JSONObject(login(testCaseInput)).getString("user_id");
    }

}
