package object;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

import automationLibrary.actions.ActionGen;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.ArraySizeComparator;
import utils.APIController;
import utils.Constants;
import utils.Helper;
import utils.JSON;
import utils.XLSWorker;
import utils.FormatUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BaseObject {
	protected String message = "";
	protected String voucherCode;
	public XSSFWorkbook header;
	public XSSFWorkbook workbook;
	public XSSFSheet sheet = null;
	public String jsonInput = "";
	public String jsonExpect = "";
	JSONArray array = new JSONArray();
	JSONArray carriers = new JSONArray();
	JSONObject mObj = new JSONObject();
	String token;
	boolean isArray = false;
	String mainKey = "";
	String phoneNumber;
	public String orderNumber = null;
	public static String jsonActualResult ;
	// using in TC Order
	String strTokenRuntime;
	public String strIdOfOrder = "";
	String strIdOfOrder1 = "";
	String strIdOfOrder2 = "";
	String strOrderId1;
	String strOrderId2;
	String strOrderId3;
	String strLabelId = "";
	String strLabelId1 = "";
	String strLabelId2 = "";
	String strLabelId3 = "";
	String strLabelId4 = "";
	String strLabelId5 = "";
	String strId;
	public String strExternalId1;
	public String strExternalId2;

	// using in TC OMS
	String strNameWarehouse = "";
	String strOrderNumber = "";
	String strOrderNumber1 = "";
	String strOrderNumber2 = "";
	String strOrderNumber3 = "";
	String strDataId = "";

	//Using in Order
	String str_VersionNo1 = "";
	
	// using in TC SKU
	String strOrderId = "";
	String str_SeqNo = "";
	String strPrimaryTerm = "";
	String strDatePayLater = "";
	String str_VersionNo = "";
	String str_ProductId = "";
	public String response = "";
	String strSkuCurrentDate="";

	//Using in TC Promotion
	String str_PromotionId ="";

	//Use in Promotion test case
	String timeStampAdd1="";
	String timeStampAdd2="";
	String timeStampAdd3="";
	String timeStampAdd4="";
	public BaseObject() {
		System.out.println("File name: " + BaseTest.fileNameFromBaseTest);
		workbook = XLSWorker.getInternalWorkbook(Constants.INPUT_PATH + BaseTest.fileNameFromBaseTest);

		String headerFile;
		if (BaseTest.fileNameFromBaseTest.contains("sku"))
			headerFile = "header_sku.xlsx";
		else if (BaseTest.fileNameFromBaseTest.contains("shopconfig"))
			headerFile = "header_shopconfig.xlsx";
		else if (BaseTest.fileNameFromBaseTest.contains("order"))
			headerFile = "header_order.xlsx";
		else if (BaseTest.fileNameFromBaseTest.contains("shop"))
			headerFile = "header_shop.xlsx";
		else if (BaseTest.fileNameFromBaseTest.contains("oms"))
			headerFile = "header_oms.xlsx";
		else
			headerFile = "header_vas.xlsx";
		header = XLSWorker.getInternalWorkbook(Constants.INPUT_PATH + headerFile);
	}

	public String getSellerUrl(String value) {
		if (BaseTest.fileNameFromBaseTest.contains("staging")) {
			switch (value) {
			case "BASE_WEB_URL":
				return "https://ban-stg.sendo.vn";
			case "BASE_URL":
				return "https://seller-api-stg.sendo.vn";
			case "BASE_URL_AUTHEN":
				return "https://seller-api-stg.sendo.vn";
			case "BASE_URL_OAUTH":
				return "https://oauth-stg.sendo.vn";
			case "BASE_OMS_URL":
				return "https://business-api-opc-stg.sendo.vn";
			case "BASE_OPC_URL":
				return "https://static-opc-stg.sendo.vn";
			case "BASE_BUYER_URL":
				return "https://stg.sendo.vn";
			case "BASE_CHECKOUT_URL":
				return "https://checkout-stg.sendo.vn";
			case "BASE_MAPI_URL":
				return "https://mapi-stg.sendo.vn";
			case "BASE_SAPI_URL":
				return "https://sapi-stg.sendo.vn";
			case "BASE_VAS_URL":
				return "https://ad-stg.sendo.vn";
			case "BASE_SHOP_PARTNER_URL":
				return "https://seller-api-stg.sendo.vn";
			case "BASE_SHOP_PARTNER_URL_INSIDE":
				return "https://inside-api-seller-stg.sendo.vn";
			case "BASE_SELLER_PARTNER_URL":
				return "https://open-stg.sendo.vn";
			}
		} else if (BaseTest.fileNameFromBaseTest.contains("production")) {
			switch (value) {
			case "BASE_WEB_URL":
				return "https://ban.sendo.vn";
			case "BASE_URL":
				return "https://seller-api.sendo.vn";
			case "BASE_URL_AUTHEN":
				return "https://seller-api.sendo.vn";
			case "BASE_URL_OAUTH":
				return "https://oauth.sendo.vn";
			case "BASE_BUYER_URL":
				return "https://www.sendo.vn";
			case "BASE_CHECKOUT_URL":
				return "https://checkout.sendo.vn";
			case "BASE_MAPI_URL":
				return "https://mapi.sendo.vn";
			case "BASE_SAPI_URL":
				return "https://sapi.sendo.vn";
			case "BASE_VAS_URL":
				return "https://ad.sendo.vn";
			case "BASE_SELLER_PARTNER_URL":
				return "https://open.sendo.vn";
//                case "BASE_OMS_URL":
//                	return "http://oms-salesorder.sendo.idc";
			}
		} else if (BaseTest.fileNameFromBaseTest.contains("test")) {
			switch (value) {
			case "BASE_WEB_URL":
				return "http://ban.test.sendo.vn";
			case "BASE_URL":
				return "http://apipwa.seller.test.sendo.vn";
			case "BASE_URL_AUTHEN":
				return "http://apipwa.seller.test.sendo.vn";
			case "BASE_URL_OAUTH":
				return "http://oauth.test.sendo.vn";
			case "BASE_OMS_URL":
				return "http://businessapi.opc.test.sendo.vn";
			case "BASE_SAPI_URL":
				return "http://api.partner.seller.test.sendo.vn";
			case "BASE_INSIDEAPICORE_URL":
				return "http://insideapi-core.seller.test.sendo.vn";
			case "BASE_SHOPCONFIG0_URL":
				return "http://shop-config0.seller.test.sendo.vn";
			case "BASE_SALESORDER_URL":
				return "http://salesorder.seller.test.sendo.vn";
			case "BASE_SELLERSKU_URL":
				return "http://seller-sku.test.sendo.vn";
			case "BASE_BAPI_URL":
				return "http://bapi.test.sendo.vn";
			case "BASE_SMCPWA_URL":
				return "http://smcpwa.seller.test.sendo.vn";
			case "BASE_SELLER_PARTNER_URL":
				return "http://seller-partner.test.sendo.vn";			
			}
		}
		String baseApi = XLSWorker.getValueFromExcel(header, value, 1, "SellerUrl");
		String endPoint = XLSWorker.getValueFromExcel(header, value, 2, "SellerUrl");
		if (BaseTest.fileNameFromBaseTest.contains("staging")) {
			switch (baseApi) {
			case "BASE_WEB_URL":
				baseApi = "https://ban-stg.sendo.vn";
				break;
			case "BASE_URL":
				baseApi = "https://seller-api-stg.sendo.vn";
				break;
			case "BASE_URL_AUTHEN":
				baseApi = "https://seller-api-stg.sendo.vn";
				break;
			case "BASE_OMS_URL":
				baseApi = "https://business-api-opc-stg.sendo.vn";
				break;
			case "BASE_OPC_URL":
				baseApi = "https://static-opc-stg.sendo.vn";
				break;
			case "BASE_BUYER_URL":
				baseApi = "https://stg.sendo.vn";
				break;
			case "BASE_CHECKOUT_URL":
				baseApi = "https://checkout-stg.sendo.vn";
				break;
			case "BASE_MAPI_URL":
				baseApi = "https://mapi-stg.sendo.vn";
				break;
			case "BASE_SAPI_URL":
				baseApi = "https://sapi-stg.sendo.vn";
				break;
			case "BASE_SHOP_PARTNER_URL":
				baseApi = "https://seller-api-stg.sendo.vn";
				break;
			}
		} else if (BaseTest.fileNameFromBaseTest.contains("production")) {
			switch (baseApi) {
			case "BASE_WEB_URL":
				baseApi = "https://ban.sendo.vn";
				break;
			case "BASE_URL":
				baseApi = "https://seller-api.sendo.vn";
				break;
			case "BASE_URL_AUTHEN":
				baseApi = "https://seller-api.sendo.vn";
				break;
			case "BASE_BUYER_URL":
				baseApi = "https://www.sendo.vn";
				break;
			case "BASE_CHECKOUT_URL":
				baseApi = "https://checkout.sendo.vn";
				break;
			case "BASE_MAPI_URL":
				baseApi = "https://mapi.sendo.vn";
				break;
			case "BASE_SAPI_URL":
				baseApi = "https://sapi.sendo.vn";
				break;
			}
		} else if (BaseTest.fileNameFromBaseTest.contains("test")) {
			System.out.println(baseApi);
			switch (baseApi) {
			case "BASE_WEB_URL":
				baseApi = "http://ban.test.sendo.vn";
				break;
			case "BASE_URL":
				baseApi = "http://apipwa.seller.test.sendo.vn";
				break;
			case "BASE_URL_AUTHEN":
				baseApi = "http://apipwa.seller.test.sendo.vn";
				break;
			case "BASE_OMS_URL":
				baseApi = "http://businessapi.opc.test.sendo.vn";
				break;
			case "BASE_SAPI_URL":
				baseApi = "http://api.partner.seller.test.sendo.vn";
				break;
			case "BASE_INSIDEAPI_URL":
				baseApi = "http://insideapi-core.seller.test.sendo.vn";
				break;
			case "BASE_SHOPCONFIG0_URL":
				baseApi = "http://shop-config0.seller.test.sendo.vn";
				break;
			case "BASE_SALESORDER_URL":
				baseApi = "http://salesorder.seller.test.sendo.vn";
				break;
			case "BASE_SELLERSKU_URL":
				baseApi = "http://seller-sku.test.sendo.vn";
				break;
			case "BASE_BAPI_URL":
				baseApi = "http://bapi.test.sendo.vn";
				break;
			case "BASE_SMCPWA_URL":
				baseApi = "http://smcpwa.seller.test.sendo.vn";
				break;
			}
		}
		return baseApi + endPoint;
	}

	public String getSellerUrl(XSSFWorkbook header, String value) {
		if (BaseTest.fileNameFromBaseTest.contains("staging")) {
			switch (value) {
			case "BASE_WEB_URL":
				return "https://ban-stg.sendo.vn";
			case "BASE_URL":
				return "https://seller-api-stg.sendo.vn";
			case "BASE_URL_AUTHEN":
				return "https://seller-api-stg.sendo.vn";
			case "BASE_OMS_URL":
				return "https://business-api-opc-stg.sendo.vn";
			case "BASE_OPC_URL":
				return "https://static-opc-stg.sendo.vn";
			case "BASE_BUYER_URL":
				return "https://stg.sendo.vn";
			case "BASE_CHECKOUT_URL":
				return "https://checkout-stg.sendo.vn";
			case "BASE_MAPI_URL":
				return "https://mapi-stg.sendo.vn";
			case "BASE_SAPI_URL":
				return "https://sapi-stg.sendo.vn";
			case "BASE_SHOP_PARTNER_URL":
				return "https://seller-api-stg.sendo.vn";
			}
		} else if (BaseTest.fileNameFromBaseTest.contains("production")) {
			switch (value) {
			case "BASE_WEB_URL":
				return "https://ban.sendo.vn";
			case "BASE_URL":
				return "https://seller-api.sendo.vn";
			case "BASE_URL_AUTHEN":
				return "https://seller-api.sendo.vn";
			case "BASE_BUYER_URL":
				return "https://www.sendo.vn";
			case "BASE_CHECKOUT_URL":
				return "https://checkout.sendo.vn";
			case "BASE_MAPI_URL":
				return "https://mapi.sendo.vn";
			case "BASE_SAPI_URL":
				return "https://sapi.sendo.vn";
			}
		} else if (BaseTest.fileNameFromBaseTest.contains("test")) {
			switch (value) {
			case "BASE_WEB_URL":
				return "http://ban.test.sendo.vn";
			case "BASE_URL":
				return "http://apipwa.seller.test.sendo.vn";
			case "BASE_URL_AUTHEN":
				return "http://apipwa.seller.test.sendo.vn";
			case "BASE_OMS_URL":
				return "http://businessapi.opc.test.sendo.vn";
			case "BASE_SAPI_URL":
				return "http://api.partner.seller.test.sendo.vn";
			case "BASE_INSIDEAPICORE_URL":
				return "http://insideapi-core.seller.test.sendo.vn";
			case "BASE_SHOPCONFIG0_URL":
				return "http://shop-config0.seller.test.sendo.vn";
			case "BASE_SALESORDER_URL":
				return "http://salesorder.seller.test.sendo.vn";
			case "BASE_SELLERSKU_URL":
				return "http://seller-sku.test.sendo.vn";
			case "BASE_BAPI_URL":
				return "http://bapi.test.sendo.vn";
			case "BASE_SMCPWA_URL":
				return "http://smcpwa.seller.test.sendo.vn";
			}
		}
		String baseApi = XLSWorker.getValueFromExcel(header, value, 1, "SellerUrl");
		String endPoint = XLSWorker.getValueFromExcel(header, value, 2, "SellerUrl");
		if (BaseTest.fileNameFromBaseTest.contains("staging")) {
			switch (baseApi) {
			case "BASE_WEB_URL":
				baseApi = "https://ban-stg.sendo.vn";
				break;
			case "BASE_URL":
				baseApi = "https://seller-api-stg.sendo.vn";
				break;
			case "BASE_URL_AUTHEN":
				baseApi = "https://seller-api-stg.sendo.vn";
				break;
			case "BASE_OMS_URL":
				baseApi = "https://business-api-opc-stg.sendo.vn";
				break;
			case "BASE_OPC_URL":
				baseApi = "https://static-opc-stg.sendo.vn";
				break;
			case "BASE_BUYER_URL":
				baseApi = "https://stg.sendo.vn";
				break;
			case "BASE_CHECKOUT_URL":
				baseApi = "https://checkout-stg.sendo.vn";
				break;
			case "BASE_MAPI_URL":
				baseApi = "https://mapi-stg.sendo.vn";
				break;
			case "BASE_SAPI_URL":
				baseApi = "https://sapi-stg.sendo.vn";
				break;
			case "BASE_SHOP_PARTNER_URL":
				baseApi = "https://seller-api-stg.sendo.vn";
				break;
			}
		} else if (BaseTest.fileNameFromBaseTest.contains("production")) {
			switch (baseApi) {
			case "BASE_WEB_URL":
				baseApi = "https://ban.sendo.vn";
				break;
			case "BASE_URL":
				baseApi = "https://seller-api.sendo.vn";
				break;
			case "BASE_URL_AUTHEN":
				baseApi = "https://seller-api.sendo.vn";
				break;
			case "BASE_BUYER_URL":
				baseApi = "https://www.sendo.vn";
				break;
			case "BASE_CHECKOUT_URL":
				baseApi = "https://checkout.sendo.vn";
				break;
			case "BASE_MAPI_URL":
				baseApi = "https://mapi.sendo.vn";
				break;
			case "BASE_SAPI_URL":
				baseApi = "https://sapi.sendo.vn";
				break;
			}
		} else if (BaseTest.fileNameFromBaseTest.contains("test")) {
			switch (baseApi) {
			case "BASE_WEB_URL":
				baseApi = "http://ban.test.sendo.vn";
				break;
			case "BASE_URL":
				baseApi = "http://apipwa.seller.test.sendo.vn";
				break;
			case "BASE_URL_AUTHEN":
				baseApi = "http://apipwa.seller.test.sendo.vn";
				break;
			case "BASE_OMS_URL":
				baseApi = "http://businessapi.opc.test.sendo.vn";
				break;
			case "BASE_SAPI_URL":
				baseApi = "http://api.partner.seller.test.sendo.vn";
				break;
			case "BASE_INSIDEAPI_URL":
				baseApi = "http://insideapi-core.seller.test.sendo.vn";
				break;
			case "BASE_SHOPCONFIG0_URL":
				baseApi = "http://shop-config0.seller.test.sendo.vn";
				break;
			case "BASE_SALESORDER_URL":
				baseApi = "http://salesorder.seller.test.sendo.vn";
				break;
			case "BASE_SELLERSKU_URL":
				baseApi = "http://seller-sku.test.sendo.vn";
				break;
			case "BASE_BAPI_URL":
				baseApi = "http://bapi.test.sendo.vn";
				break;
			case "BASE_SMCPWA_URL":
				baseApi = "http://smcpwa.seller.test.sendo.vn";
				break;
			}
		}
		return baseApi + endPoint;
	}

	public String getRandom() {
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
		Date date = new Date();
		String output = formatter.format(date);
		return output;
	}

	public String getRandom2() {
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMHHmmss");
		Date date = new Date();
		return formatter.format(date);
	}

	public String getCurrentDateTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return formatter.format(date);
	}

	public String getCurrentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		return formatter.format(date);
	}

	public String getCurrentDate1() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 1);
		date = c.getTime();
		String output = formatter.format(date);
		return output;
	}

	public String getCurrentDateTimeUTC() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = new Date();
		return formatter.format(date);
	}

	public String getCurrentDateTimeUTC30() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 30);
		date = c.getTime();
		String output = formatter.format(date);
		return output;
	}

	public String getTomorrow() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 1);
		date = c.getTime();
		String output = formatter.format(date);
		return output;
	}

	public String getToday() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String output = formatter.format(date);
		return output;
	}

	public String getDays(int amount) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, amount);
		String output = formatter.format(cal.getTime());
		return output;
	}

	public String getDays(int amount, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, amount);
		String output = formatter.format(cal.getTime());
		return output;
	}

	public String getDays(int amount, String format, String timezone) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		formatter.setTimeZone(TimeZone.getTimeZone(timezone));
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, amount);
		String output = formatter.format(cal.getTime());
		return output;
	}

	public String getTimes(int amount, String format, String timezone) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		formatter.setTimeZone(TimeZone.getTimeZone(timezone));
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, amount);
		String output = formatter.format(cal.getTime());
		return output;
	}

	public String getPromotionDate(String date) {
		switch (date) {
		case "TODAY":
			return getToday();
		case "TOMORROW":
			return getTomorrow();
		case "GETDAYSBEFORE":
			return getDays(-7);

		default:
			return date;
		}
	}

	public long getDaysEpoch(int amount) {
		String str = getDays(amount);
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Date date;
		long epochTimestamp = 0;
		try {
			date = df.parse(str);
			epochTimestamp = (date.getTime())/1000;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return epochTimestamp;	
	}
	
	public long getCurrentTimestamp() {
		Date date = new Date();
		long epochTimestamp= (date.getTime())/1000;
		return epochTimestamp;
	}
	
	public long getDaysEpochGMT(int addDate) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, addDate);
		cal.add(Calendar.HOUR, 7);
		long epochTimestamp = (cal.getTimeInMillis())/1000;	
		return epochTimestamp;
	}

	
	public String getParamsFromExcel(String symbol, int row, String sheetName, JSONObject data) {
		int index1 = XLSWorker.getColumnNumByName("Token", sheetName);
		int index2 = XLSWorker.getColumnNumByName("StatusCode", sheetName);

		String param = "";
		if (symbol.equals("?")) {
			sheet = XLSWorker.getSheet(workbook, sheetName);
			Object[][] object = XLSWorker.getData(workbook, sheetName);
			if (object == null)
				return "";
			for (int j = index1 + 1; j < index2; j++) {
				String value = getValue(object[row][j].toString(), data);
				if (value != null) {
					object[row][j] = value;
				}
				if (!object[row][j].toString().equals("N/A")) {
					if (j != index1 + 1)
						param = param + "&";
					param = param + object[0][j].toString() + "=" + object[row][j].toString();
				}
			}
		} else {
			sheet = XLSWorker.getSheet(workbook, sheetName);
			Object[][] object = XLSWorker.getData(workbook, sheetName);
			if (object == null)
				return "";
			for (int j = index1 + 1; j < index2; j++) {
				String value = getValue(object[row][j].toString(), data);
				if (value != null) {
					object[row][j] = value;
				}
				if (!object[row][j].toString().equals("N/A")) {
					param = object[row][j].toString();
				}
			}
		}
		return param;
	}

	public String getValue(String value, JSONObject data) {
		switch (value) {
		case "GetSkuRandom":
			return "SKU" + getRandom();
		case "GetOrderRandom":
			if (orderNumber == null) {
				orderNumber = "OT" + getRandom();
			}
			return orderNumber;
		case "GetLabelRandom":
			return "L" + getRandom();
		case "GetCurrentDateTime":
			return getCurrentDateTime();
		case "Get48HoursAgo":
			return getDays(-2);
		case "TODAY":
			return getToday();
		case "TOMORROW":
			return getTomorrow();
		case "GETDAYSBEFORE":
			return getDays(-7);
		case "GetProductId":
			return data.getString("productId");
		case "GetOrderId":
			return data.getString("orderId");
		case "GetOrderNumber":
			return data.getString("order_number");
		case "GetOldOrderNumber":
			return data.getString("orderNumber");
		default:
			if (value.contains("CreateOrder"))
				return data.getString("orderNumber");
			else
				return null;
		}

	}

	public String getPayloadFromExcel(int row, String sheetName, JSONObject data) {
		System.out.println("Start get payload with sheetName: " + sheetName);
		JSONObject objectJson = new JSONObject();
		sheet = XLSWorker.getSheet(workbook, sheetName);
		Object[][] object = XLSWorker.getData(workbook, sheetName);

		int index1 = XLSWorker.getColumnNumByName("Token", sheetName);
		int index2 = XLSWorker.getColumnNumByName("StatusCode", sheetName);

		if (object == null)
			return "";
		for (int j = index1 + 1; j < index2; j++) {

			if (object[row][j].toString().equals("N/A"))
				continue;

			String value = getValue(object[row][j].toString(), data);
			if (value != null) {
				object[row][j] = value;
			}
			// Truyền nguyên body vào 1 cột trong file excel
			if (object[0][j].equals("Body")) {
				if (object[row][j].toString().startsWith("{") && object[row][j].toString().endsWith("}")) {
					objectJson = new JSONObject(object[row][j].toString());
				}
//				return Helper.decodeUnicodeString(objectJson.toString());
				continue;
			}
			/*************************/
			if (sheetName.equals("Register_Step1") && object[0][j].equals("phone")) {
				if (object[row][j].equals("GetPhoneNumber")) {
					object[row][j] = "079" + generateRandomIntIntRange(1000000, 9999999);
				}
				phoneNumber = object[row][j].toString();
			}

			if (sheetName.equals("Merge_Order")) {

				if (object[row][j].toString().startsWith("[") && object[row][j].toString().endsWith("]")) {
					object[row][j] = new JSONArray(object[row][j].toString());
				}
				if (object[row][j].toString().startsWith("{") && object[row][j].toString().endsWith("}")) {
					object[row][j] = new JSONObject(object[row][j].toString());
				}

				if (object[0][j].equals("SaleOrderNumberList") && object[row][j].equals("GetOrderList")) {
					JSONArray array = new JSONArray();
					array.put(data.get("orderNumber1"));
					array.put(data.get("orderNumber2"));
					objectJson.put(object[0][j].toString(), array);
					continue;
				}

				if (object[0][j].equals("UserMerge")) {
					objectJson.put("UserMerge", object[row][j]);
					continue;
				}

				mObj.put(object[0][j].toString(), object[row][j]);
				if (object[0][j].equals("IsBadBuyerOrder")) {
					objectJson.put("NewSaleOrderObj", mObj);
				}

				continue;

			}

			if (sheetName.equals("Split_Order")) {

				if (object[row][j].toString().startsWith("[") && object[row][j].toString().endsWith("]")) {
					object[row][j] = new JSONArray(object[row][j].toString());
				}
				if (object[row][j].toString().startsWith("{") && object[row][j].toString().endsWith("}")) {
					object[row][j] = new JSONObject(object[row][j].toString());
				}

				if (object[0][j].equals("ParentOrderNumber")) {
					objectJson.put(object[0][j].toString(), data.get("orderNumber1"));
					continue;
				}

				if (object[0][j].equals("UserSplit")) {
					objectJson.put("UserSplit", object[row][j]);
					continue;
				}

				mObj.put(object[0][j].toString(), object[row][j]);
				if (object[0][j].equals("ShipmentTransportType")) {
					objectJson.put("SalesOrder", mObj);
				}

				continue;

			}

			if (object[row][j].equals("GetVersionNo")) {
				object[row][j] = data.get("versionNo");
			}

			if (object[row][j].equals("GetProductId")) {
				object[row][j] = data.get("productId");
			}

			if (object[row][j].equals("GetOrderId")) {
				object[row][j] = data.get("orderId");
			}

			if (object[row][j].equals("GetTrackingNumber")) {
				object[row][j] = "SGT" + orderNumber;
			}

			if (object[row][j].equals("GetShopId")) {
				object[row][j] = data.get("shopId");
			}

			if (object[row][j].equals("GetBuyerId")) {
				object[row][j] = data.get("buyerId");
			}

			if (object[row][j].equals("GetOtp")) {
				object[row][j] = getOTP(phoneNumber);
			}

			if (object[row][j].equals("GetPageSize")) {
				object[row][j] = getPageSize();
			}

			if (sheetName.equals("Installment")) {
				if (object[0][j].equals("InstallmentTerms")) {
					array.put(new JSONObject(object[row][j].toString()));
					objectJson.put(object[0][j].toString(), array);
					continue;
				}
			}

			if (sheetName.equals("PaymentOnline")) {
				if (object[0][j].equals("SenpayLevel")) {
					array.put(new JSONObject(object[row][j].toString()));
					objectJson.put(object[0][j].toString(), array);
					continue;
				}
			}

			if (sheetName.equals("ShippingConfig")) {
				if (object[0][j].equals("Levels")) {
					array.put(new JSONObject(object[row][j].toString()));
					objectJson.put(object[0][j].toString(), array);
					continue;
				}
			}

			if (sheetName.equals("Carrier")) {
				array.put(new JSONObject(object[row][j].toString()));
				if (object[0][j].toString().contains("instant"))
					return array.toString();
			}

			if (sheetName.equals("DropOff")) {
				if (object[0][j].equals("Carriers")) {
					array.put(new JSONObject(object[row][j].toString()));
					objectJson.put(object[0][j].toString(), array);
					continue;
				}
			}

			if (sheetName.equals("NotiPrivateOffer")) {
				array.put(new JSONObject(object[row][j].toString()));
				if (object[0][j].toString().contains("Noti3"))
					return array.toString();
			}

			if (object[row][j].toString().startsWith("[") && object[row][j].toString().endsWith("]")) {
				objectJson.put(object[0][j].toString(), new JSONArray(object[row][j].toString()));
				continue;
			}
			if (object[row][j].toString().startsWith("{") && object[row][j].toString().endsWith("}")) {
				objectJson.put(object[0][j].toString(), new JSONObject(object[row][j].toString()));
				continue;
			}

			if (sheetName.contains("AddProduct") || sheetName.contains("EditProduct") || sheetName.contains("Draft")
					|| sheetName.contains("GTKTT")) {
				if (object[0][j].equals("ProductImage") && object[row][j].equals("GET")) {
					objectJson.put("ProductImage", data.get("productImage"));
					continue;
				}
				if (object[0][j].equals("Attributes")) {
					objectJson.put("Attributes", new JSONArray((String) data.get("attributes")));
					continue;
				}
				if (object[0][j].equals("BrandName") && !object[row][j].equals("")) {
					objectJson.put("BrandId", data.get("brandId"));
					continue;
				}

				if (object[0][j].equals("Category")) {
					objectJson.put("Cat2Id", readValueByJsonPath((String) data.get("cateResponse"),
							XLSWorker.getValueFromExcel(header, "Cate2Id", 1, "JsonPath")));
					objectJson.put("Cat3Id", readValueByJsonPath((String) data.get("cateResponse"),
							XLSWorker.getValueFromExcel(header, "Cate3Id", 1, "JsonPath")));
					objectJson.put("Cat4Id", readValueByJsonPath((String) data.get("cateResponse"),
							XLSWorker.getValueFromExcel(header, "Cate4Id", 1, "JsonPath")));
					continue;
				}

				if (object[0][j].equals("Cat4Id")) {
					objectJson.put("Cat2Id", data.get("Cat2Id"));
					objectJson.put("Cat3Id", data.get("Cat3Id"));
					objectJson.put("Cat4Id", data.get("Cat4Id"));
					continue;
				}

				if (object[0][j].toString().contains("Attr")) {
					continue;
				}

				if (object[0][j].equals("CertificateFileUpload") && !object[row][j].equals("")) {
					JSONArray certificates = (JSONArray) data.get("certificates");
					if (certificates.length() == 0) {
						JSONArray array = new JSONArray();
						JSONObject obj = new JSONObject();
						obj.put("FileName", "ABCD");
						obj.put("AttachmentUrl", "ABCD.docx");
						obj.put("IsImage", true);
						array.put(obj);
						objectJson.put(object[0][j].toString(), array);
					} else {
						objectJson.put(object[0][j].toString(), certificates);
					}
					continue;
				}
				if (object[0][j].equals("Variants")) {
					objectJson.put("IsConfigVariant", true);
					objectJson.put("Variants", data.get("variants"));
					continue;
				}
			}

			if (sheetName.equals("ShopInfo")) {
				if (object[0][j].equals("Id") && object[row][j].equals("GET")) {
					objectJson.put("id", data.get("shopId"));
					continue;
				}
				if (object[0][j].equals("StoreId") && object[row][j].equals("GET")) {
					objectJson.put("storeId", data.get("shopId"));
					continue;
				}
				if (object[0][j].equals("ShopWardId")) {
					objectJson.put("ShopRegionId", data.get("shopRegionId"));
					objectJson.put("ShopDistrictId", data.get("shopDistrictId"));
					objectJson.put("ShopWardId", data.get("shopWardId"));
					continue;
				}
				if (object[0][j].equals("WarehouseWardId")) {
					objectJson.put("WarehouseRegionId", data.get("warehouseRegionId"));
					objectJson.put("WarehouseDistrictId", data.get("warehouseDistrictId"));
					objectJson.put("WarehouseWardId", data.get("warehouseWardId"));
					System.out.println(data.get("shopId"));
					continue;
				}
			}

			if (sheetName.equals("ConfirmOrders")) {
				if (object[0][j].equals("orders")) {
					JSONObject obj = new JSONObject();
					obj.put("Id", data.get("orderId"));
					obj.put("VersionNo", data.get("versionNo"));
					JSONArray array = new JSONArray();
					array.put(obj);
					objectJson.put(object[0][j].toString(), array);
					continue;
				}
			}

			if (sheetName.equals("MergeOrder")) {
				if (object[0][j].equals("salesOrders")) {
					JSONArray array = new JSONArray();
					array.put(data.get("order_1"));
					array.put(data.get("order_2"));
					objectJson.put(object[0][j].toString(), array);
					continue;
				}
			}

			if (sheetName.contains("PrivateOffer")) {
				if (object[0][j].equals("PrivateOfferId") && object[row][j].equals("GET")) {
					objectJson.put("PrivateOfferId", data.get("privateOfferId"));
					continue;
				}

				if (object[0][j].equals("VoucherValue") && object[row][j].equals("GET")) {
					objectJson.put("VoucherValue", data.get("voucherValue"));
					continue;
				}
			}

			if (sheetName.equals("AddVoucher") || sheetName.equals("EditVoucher")) {
				if (object[0][j].equals("Code") && object[row][j].toString().contains("RANDOM")) {
					String prefix = object[row][j].toString().replace("RANDOM", "");
					String random = getRandom2();
					object[row][j] = prefix + random;
					objectJson.put("Code", object[row][j].toString());
					voucherCode = object[row][j].toString();
					continue;
				}
			}

			if (object[row][j].equals("false") || object[row][j].equals("true")) {
				objectJson.put(object[0][j].toString(), Boolean.valueOf(object[row][j].toString()));
				continue;
			}

			objectJson.put(object[0][j].toString(), object[row][j].toString());

		}
		if (sheetName.equals("QuickEdit"))
			return "[" + objectJson.toString() + "]";
		return Helper.decodeUnicodeString(objectJson.toString());
	}

	public String run(int row, String token, String sheetName, JSONObject data) {
		String method = XLSWorker.getValueFromExcel(row, "Method", sheetName);
		String source = getSellerUrl(XLSWorker.getValueFromExcel(row, "EndPoint", sheetName));
		String payload = "";
		if (source.endsWith("?")) {
			source = source + getParamsFromExcel("?", row, sheetName, data);
		} else if (source.endsWith("/")) {
			source = source + getParamsFromExcel("/", row, sheetName, data);
		} else {
			if (!method.equals("GET")) {
				payload = getPayloadFromExcel(row, sheetName, data);
			}
		}
		jsonInput = payload;
		return APIController.sendApiRequest(method, source, payload, token);
	}

	public String run(String method, String source, String payload, String token) {
		String result = APIController.sendApiRequest(method, source, payload, token);
		if (!result.contains("\"statusCode\":200") || result.contains("Id") || !result.contains("\"IsError\":false")
				|| result.contains("data") || result.contains("increment_id"))
			return result;
		return null;
	}

	public String runWithCustomHeader(String method, String source, String payload, String token, String header) {
		String result = APIController.sendApiRequestWithCustomHeader(method, source, payload, token, header);
		if (!result.contains("\"statusCode\":200") || result.contains("Id") || !result.contains("\"IsError\":false")
				|| result.contains("data") || result.contains("increment_id"))
			return result;
		return null;
	}
	public String runGetMultiLine(String method, String source, String payload, String token) {
		String result = APIController.sendApiRequestGetMultiline(method, source, payload, token);
		if (!result.contains("\"statusCode\":200") || result.contains("Id") || !result.contains("\"IsError\":false")
				|| result.contains("data") || result.contains("increment_id"))
			return result;
		return null;
	}	

	public String run(String method, String source, String payload, String token, String contentType) {
		String result = APIController.sendApiRequest(method, source, payload, token, contentType);
		if (!result.contains("\"statusCode\":200") || result.contains("Id") || !result.contains("\"IsError\":false")
				|| result.contains("data") || result.contains("increment_id"))
			return result;
		return null;
	}

	public List<String> getFormData(String name, String type, String chunk, String chunks, String albumid) {
		List<String> contents = new ArrayList<>();
		contents.add("START");
		contents.add("Content-Disposition: form-data; name=\"name\"");
		contents.add("CRLF");
		contents.add(name);
		if (!chunk.equals("")) {
			contents.add("START");
			contents.add("Content-Disposition: form-data; name=\"chunk\"");
			contents.add("CRLF");
			contents.add(chunk);
		}
		if (!chunks.equals("")) {
			contents.add("START");
			contents.add("Content-Disposition: form-data; name=\"chunks\"");
			contents.add("CRLF");
			contents.add(chunks);
		}
		if (!albumid.equals("")) {
			contents.add("START");
			contents.add("Content-Disposition: form-data; name=\"albumid\"");
			contents.add("CRLF");
			contents.add(albumid);
		}
		contents.add("START");
		contents.add("Content-Disposition: form-data; name=\"file\"; filename=\"" + name + "\"");
		contents.add("Content-Type: " + type);
		contents.add("CRLF");
		contents.add("DATA");
		contents.add("CRLF");
		contents.add("END");
		return contents;
	}

	public String upload(int row, String token, String sheetName, String fileName, String boundaryName,
			List<String> contents) {
		String source = getSellerUrl(XLSWorker.getValueFromExcel(row, "EndPoint", sheetName));
		return APIController.upload(fileName, boundaryName, contents, source, token);
	}
	
	public String upload2(String token, String fileName, String boundaryName, List<String> contents, String source) {		
		response = APIController.upload(fileName, boundaryName, contents, source, token);
		return response;
	}

	public String uploadWithCustomHeader(String fileName, String boundaryName, List<String> contents, String source, String header) {
		response = APIController.uploadWithCustomHeader(fileName, boundaryName, contents, source, header);
		return response;
	}

	public boolean verifyResponse(int row, String actualResponse, String sheetName) {
		JSONObject json = new JSONObject();
		int index1 = XLSWorker.getColumnNumByName("StatusCode", sheetName);
		int index2 = XLSWorker.getColumnNumByName("Actual Response", sheetName);
		String statusCode = "";
		for (int i = index1; i < index2; i++) {
			String expectResponseName = XLSWorker.getValueFromExcel(0, i, sheetName);
			if (expectResponseName.contains("Expect"))
				continue;
			String expectResponseValue = XLSWorker.getValueFromExcel(row, i, sheetName);
			if (expectResponseValue.equals("N/A")) {
				continue;
			}
			if (!statusCode.equals("200")) {
				System.out.println("Expected response result: " + expectResponseValue);
				String jsonPath = XLSWorker.getValueFromExcel(header, expectResponseName, 1, "JsonPath");
				System.out.println("Json path: " + jsonPath);
				json.put(jsonPath, expectResponseValue);
				jsonExpect = json.toString();
				String actualResponseValue = readValueByJsonPath(actualResponse, jsonPath);
				System.out.println("Actual response value: " + actualResponseValue);
				if (actualResponseValue == null) {
					jsonPath = XLSWorker.getValueFromExcel(header, "ErrorMessage", 1, "JsonPath");
					actualResponseValue = readValueByJsonPath(actualResponse, jsonPath);
					message = "Lỗi: " + actualResponseValue;
					return false;
				}
				if (expectResponseValue.equals("") || !actualResponseValue.contains(expectResponseValue)) {
					message = "Lỗi: " + actualResponseValue;
					System.out.println(message);
					return false;
				}
				if (expectResponseName.equals("StatusCode"))
					statusCode = actualResponseValue;
			}
		}
		return true;
	}

	public String getJsonExpectedResponse(int row, String sheetName) {
		int index1 = XLSWorker.getColumnNumByName("StatusCode", sheetName);
		int index2 = XLSWorker.getColumnNumByName("Actual Response", sheetName);
		JSONObject obj = new JSONObject();
		for (int i = index1; i < index2; i++) {
			String expectResponseName = XLSWorker.getValueFromExcel(0, i, sheetName);
			String expectResponseValue = XLSWorker.getValueFromExcel(row, i, sheetName);
			String jsonPath = XLSWorker.getValueFromExcel(expectResponseName, 1, "JsonPath");
			obj.put(jsonPath, expectResponseValue);
		}
		return obj.toString();
	}

	public String getToken(int row, String sheetName) {
		if (sheetName.equals("Login"))
			return "";
		String payloadToken = XLSWorker.getValueFromExcel(row, "Token", sheetName);
		String endPoint = XLSWorker.getValueFromExcel(row, "EndPoint", sheetName);

		switch (payloadToken) {
		case "WrongTokenPartner":
			String strWrongTokenPartner = "1eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJTdG9yZUlkIjoiNTcwNDYzIiwiVXNlck5hbWUiOiIiLCJTdG9yZVN0YXR1cyI6IjIiLCJTaG9wVHlwZSI6IjEiLCJTdG9yZUxldmVsIjoiMCIsImV4cCI6MTYxMDQ1MDY2OCwiaXNzIjoiNTcwNDYzIiwiYXVkIjoiNTcwNDYzIn0.v1siDHg-xXxBj_6i4FNXcMLp7M32h9vzjgWGlPN7NXs";
			return strWrongTokenPartner;
		case "ExpiredTokenPartner":
			String strExpiredTokenPartner = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJTdG9yZUlkIjoiNTcwNDYzIiwiVXNlck5hbWUiOiIiLCJTdG9yZVN0YXR1cyI6IjIiLCJTaG9wVHlwZSI6IjEiLCJTdG9yZUxldmVsIjoiMCIsImV4cCI6MTYxMDQ1MDY2OCwiaXNzIjoiNTcwNDYzIiwiYXVkIjoiNTcwNDYzIn0.v1siDHg-xXxBj_6i4FNXcMLp7M32h9vzjgWGlPN7NXs";
			return strExpiredTokenPartner;
		case "WrongToken":	
			String strWrongToken = "ayJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJTdG9yZUlkIjoiMTAwNDgyIiwiVG9rZW4iOiJBTlVabUZtRGJERVpzdnJqK0xQKzBhRk90Q1QrazdaeFdxWHhoenNFWXdUY2xCN2xqTDBCUTloam9QdzQzWFUxRlo5c3AvNUkrSUxmV1ZoSFhlbkJ0UDYvNkVuYmxPNW1tZCtwNTErUnlrRTFjblhJWUxDaUdKUHV4RnRvK0x6WGFJblN2MDlXcm5OeGNCUzVraTZSSzhKUzY1ZlFnMWxESE1ld0FWajBRWFk9IiwiVXNlck5hbWUiOiIyMDAwMDA0NDgxIiwiU3RvcmVTdGF0dXMiOiIxIiwiU2hvcFR5cGUiOiIyIiwiRW1haWwiOiJidXllcjEwQGdtYWlsLmNvbSIsIlN0b3JlRW1haWwiOiJkdG5hY2s5QGdtYWlsLmNvbSIsIkZ1bGxOYW1lIjoidGVzdCAiLCJJc0xvZ2luRnJvbU1vYmlsZUFwcCI6IkZhbHNlIiwiU2VucGF5SWQiOiIiLCJTdG9yZUxldmVsIjoiIiwiU3RvcmVOYW1lIjoiaG90IGNvbGQgd2FybSDhur8iLCJTdG9yZVVybCI6Imh0dHA6Ly9zZW5kby52bi9zaG9wL2hvdC1jb2xkLXdhcm0tZS00MDBmNDQ4MSIsIkFjdGl2aXR5UG9pbnQiOiIwLDAiLCJTZW5kb2lkUGhvbmUiOiIwMzg2MTE0MTI3IiwiU2VuZG9JZFBob25lSXNWZXJpZnkiOiJUcnVlIiwiTmV4dFN0ZXAiOiIiLCJXYWxsZXRJZCI6IiIsImV4cCI6MTYwMDc4ODQ3MiwiaXNzIjoiaHR0cHM6Ly9sb2NhbGhvc3Q6OTAwMC8iLCJhdWQiOiJodHRwczovL2xvY2FsaG9zdDo5MDAwLyJ9.vAv_L-GE060uj4y4aJZvb0_I2Dfkjnq1JMxWv-YSwtw";
			return strWrongToken;
		case "":
			return "";
		default:
			String baseUrl = getSellerUrl("BASE_URL_AUTHEN");
			String baseOauth = getSellerUrl("BASE_URL_OAUTH");
			String basePartnerUrl = getSellerUrl("BASE_SELLER_PARTNER_URL");
			String endPoint_auth;
			String endPoint_sendoid;
			String source0;
			String source;
			if (endPoint.contains("/mobile")) {
//				endPoint_token = "/api/Token/mobilev2";
//				source = baseUrl + endPoint_token;
//				return APIController.getAuthorizationCode(source, payloadToken);
				endPoint_sendoid = "/login/sendoid";
				source0 = baseOauth+endPoint_sendoid;
				
				String token0 = APIController.getAuthorizationCode(source0, payloadToken);				
				endPoint_auth ="/api/token/mobile/tokensendoid";
				source = baseUrl + endPoint_auth;
				return APIController.getAuthorizationCodeMobile(source, token0);
				
			} else if (endPoint.contains("partner")) {
				endPoint_auth = "/login";
				source = basePartnerUrl + endPoint_auth;
				return APIController.getAuthorizationCode(source, payloadToken);
			} else if(endPoint.contains("cart-service")||endPoint.contains("checkout")) {
				source0 = baseOauth+"/login/sendoid";
				return APIController.getAuthorizationCode(source0, payloadToken);					
			}

			else {
//				endPoint_token = "/api/token";
//				source = baseUrl + endPoint_token;
//				return APIController.getAuthorizationCode(source, payloadToken);
				endPoint_sendoid = "/login/sendoid";
				source0 = baseOauth+endPoint_sendoid;
				
				String token0 = APIController.getAuthorizationCode(source0, payloadToken);				
				endPoint_auth ="/api/token/web/tokensendoid";
				source = baseUrl + endPoint_auth;
				return APIController.getAuthorizationCodeWeb(source, token0);
				
			}
		}		
	}

	public String getTokenWeb(int row, String sheetName) {
		if (sheetName.equals("Login"))
			return "";
		String payloadToken = XLSWorker.getValueFromExcel(row, "Token", sheetName);
		String endPoint = XLSWorker.getValueFromExcel(row, "EndPoint", sheetName);

		switch (payloadToken) {
			case "WrongTokenPartner":
				String strWrongTokenPartner = "1eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJTdG9yZUlkIjoiNTcwNDYzIiwiVXNlck5hbWUiOiIiLCJTdG9yZVN0YXR1cyI6IjIiLCJTaG9wVHlwZSI6IjEiLCJTdG9yZUxldmVsIjoiMCIsImV4cCI6MTYxMDQ1MDY2OCwiaXNzIjoiNTcwNDYzIiwiYXVkIjoiNTcwNDYzIn0.v1siDHg-xXxBj_6i4FNXcMLp7M32h9vzjgWGlPN7NXs";
				return strWrongTokenPartner;
			case "ExpiredTokenPartner":
				String strExpiredTokenPartner = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJTdG9yZUlkIjoiNTcwNDYzIiwiVXNlck5hbWUiOiIiLCJTdG9yZVN0YXR1cyI6IjIiLCJTaG9wVHlwZSI6IjEiLCJTdG9yZUxldmVsIjoiMCIsImV4cCI6MTYxMDQ1MDY2OCwiaXNzIjoiNTcwNDYzIiwiYXVkIjoiNTcwNDYzIn0.v1siDHg-xXxBj_6i4FNXcMLp7M32h9vzjgWGlPN7NXs";
				return strExpiredTokenPartner;
			case "WrongToken":
				String strWrongToken = "ayJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJTdG9yZUlkIjoiMTAwNDgyIiwiVG9rZW4iOiJBTlVabUZtRGJERVpzdnJqK0xQKzBhRk90Q1QrazdaeFdxWHhoenNFWXdUY2xCN2xqTDBCUTloam9QdzQzWFUxRlo5c3AvNUkrSUxmV1ZoSFhlbkJ0UDYvNkVuYmxPNW1tZCtwNTErUnlrRTFjblhJWUxDaUdKUHV4RnRvK0x6WGFJblN2MDlXcm5OeGNCUzVraTZSSzhKUzY1ZlFnMWxESE1ld0FWajBRWFk9IiwiVXNlck5hbWUiOiIyMDAwMDA0NDgxIiwiU3RvcmVTdGF0dXMiOiIxIiwiU2hvcFR5cGUiOiIyIiwiRW1haWwiOiJidXllcjEwQGdtYWlsLmNvbSIsIlN0b3JlRW1haWwiOiJkdG5hY2s5QGdtYWlsLmNvbSIsIkZ1bGxOYW1lIjoidGVzdCAiLCJJc0xvZ2luRnJvbU1vYmlsZUFwcCI6IkZhbHNlIiwiU2VucGF5SWQiOiIiLCJTdG9yZUxldmVsIjoiIiwiU3RvcmVOYW1lIjoiaG90IGNvbGQgd2FybSDhur8iLCJTdG9yZVVybCI6Imh0dHA6Ly9zZW5kby52bi9zaG9wL2hvdC1jb2xkLXdhcm0tZS00MDBmNDQ4MSIsIkFjdGl2aXR5UG9pbnQiOiIwLDAiLCJTZW5kb2lkUGhvbmUiOiIwMzg2MTE0MTI3IiwiU2VuZG9JZFBob25lSXNWZXJpZnkiOiJUcnVlIiwiTmV4dFN0ZXAiOiIiLCJXYWxsZXRJZCI6IiIsImV4cCI6MTYwMDc4ODQ3MiwiaXNzIjoiaHR0cHM6Ly9sb2NhbGhvc3Q6OTAwMC8iLCJhdWQiOiJodHRwczovL2xvY2FsaG9zdDo5MDAwLyJ9.vAv_L-GE060uj4y4aJZvb0_I2Dfkjnq1JMxWv-YSwtw";
				return strWrongToken;
			case "":
				return "";
			default:
				String baseUrl = getSellerUrl("BASE_URL_AUTHEN");
				String baseOauth = getSellerUrl("BASE_URL_OAUTH");
				String basePartnerUrl = getSellerUrl("BASE_SELLER_PARTNER_URL");
				String endPoint_auth;
				String endPoint_sendoid;
				String source0;
				String source;
				if (endPoint.contains("partner")) {
					endPoint_auth = "/login";
					source = basePartnerUrl + endPoint_auth;
					return APIController.getAuthorizationCode(source, payloadToken);
				} else if(endPoint.contains("cart-service")||endPoint.contains("checkout")) {
					source0 = baseOauth+"/login/sendoid";
					return APIController.getAuthorizationCode(source0, payloadToken);
				}
				else {
					endPoint_sendoid = "/login/sendoid";
					source0 = baseOauth+endPoint_sendoid;

					String token0 = APIController.getAuthorizationCode(source0, payloadToken);
					endPoint_auth ="/api/token/web/tokensendoid";
					source = baseUrl + endPoint_auth;
					return APIController.getAuthorizationCodeWeb(source, token0);
				}
		}
	}

	public String getBuyerToken() {
		String payloadToken = "{\"password\":\"123456\",\"username\":\"newemail08@gmail.com\"}";
		String source = "https://mapi-stg.sendo.vn/mob/user/login";
		return APIController.getAuthorizationCode(source, payloadToken);
	}

	public String getResultMessage() {
		return message;
	}
	
	public String getJsonInput() {
		return jsonInput;
	}
	
	public String readValueByJsonPath(String response, String path) {
		try {
			return JsonPath.read(response, path).toString();
		} catch (Exception e) {
			return null;
		}
	}

	public static List<String> getValueOfJson(JSONObject input, String _key) {
		List<String> value = new ArrayList<>();
		Iterator<String> keys = input.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (input.get(key) instanceof JSONObject) {

				JSONObject object = input.getJSONObject(key);
				value.addAll(getValueOfJson(object, _key));

			} else {
				if (input.get(key) instanceof JSONArray) {
					JSONArray array = (JSONArray) input.get(key);
					for (int i = 0; i < array.length(); i++) {
						try {
							JSONObject object = (JSONObject) array.get(i);
							value.addAll(getValueOfJson(object, _key));

						} catch (ClassCastException e) {
							// if value in array is not key object, add value to List<String> value (e.g
							// [1,2,3])
							if (_key.contains(key) && key.contains(_key))
								value.add(input.get(key).toString());
						}

					}
				} else {
					if (_key.contains(key) && key.contains(_key))
						value.add(input.get(key).toString());
				}

			}
		}
		return value;
	}

	public static List<String> getValueOfArrayJson(JSONObject input, String _arrKey) {
		List<String> value = new ArrayList<>();
		Iterator<String> keys = input.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (input.get(key) instanceof JSONObject) {

				JSONObject object = input.getJSONObject(key);
				value.addAll(getValueOfArrayJson(object, _arrKey));

			} else {
				if (input.get(key) instanceof JSONArray) {
					JSONArray array = (JSONArray) input.get(key);
					for (int i = 0; i < array.length(); i++) {
						try {
							JSONObject object = (JSONObject) array.get(i);
							value.add(object.toString());
						} catch (ClassCastException e) {
							// if value in array is not key object, add value to List<String> value (e.g
							// [1,2,3])
							if (_arrKey.contains(key) && key.contains(_arrKey))
								value.add(input.get(key).toString());
						}
					}
				} else {
					if (_arrKey.contains(key) && key.contains(_arrKey))
						value.add(input.get(key).toString());
				}
			}
		}
		return value;
	}

	public JSONObject getPageSize() {
		JSONObject page = new JSONObject();
		JSONObject obj = new JSONObject();
		obj.put("CurrentPage", 1);
		obj.put("PageSize", 10);
		page.put("Page", obj);
		return obj;
	}

	public String getOTP(String phoneNumber) {
		String url = "http://bapi.sendo.aws/customer/send-otp";
		JSONObject obj = new JSONObject();
		obj.put("type", "register");
		obj.put("phone", phoneNumber);
		obj.put("message", "%s la ma xac thuc OTP de dat hang tren Sendo");
		String result = APIController.sendApiRequest("POST", url, obj.toString(), "");
		JSONObject token = new JSONObject(result);
		return (String) token.get("token");
	}

	public static int generateRandomIntIntRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	public boolean isObjectKeyExist(JSONObject obj, String _key) {
		Iterator<String> keys = obj.keys();

		while (keys.hasNext()) {
			String key = keys.next();
			if (key.contains(_key)) {
				return true;				
			}else {
				JSONObject object = obj.getJSONObject(key);
				isObjectKeyExist(object, _key);
			}
		}		
		return false;
	}

	public static String getRamdomString(int totalChar) {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < totalChar) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
	}
	
	public static String rtrim(String s) {
		int i = s.length() - 1;
		while (i >= 0 && Character.isWhitespace(s.charAt(i))) {
			i--;
		}
		return s.substring(0, i + 1);
	}

	public static String ltrim(String s) {
		int i = 0;
		while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
			System.out.println("s.charAt(i)  " + s.charAt(i));
			i++;
		}
		return s.substring(i);
	}

	public List<String> getListValue(String response, String key) {
		List<String> _value;
		try {
			JSONObject input = new JSONObject(response);
			_value = BaseObject.getValueOfJson(input, key);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			_value = null;			
		}		
		return _value;
	}
	
	public static String keyObj="";
	
	public List<String> getActualExpectFromObject(String strKeyWithValue, JSONObject objActual, String parentKey) {
		String strActual = "";
		String strExpect = "";
		List<String> listActualExpect = new ArrayList<String>();

		// In case object is array contain object. ex:
		// {"test":{"result":[{"name":"nmn","age":"123"}]}}
		if (strKeyWithValue.contains("\":[{")) {
			Pattern pattern = Pattern.compile("\"(.*?)\"");
			Matcher matcher = pattern.matcher(strKeyWithValue);

			if (matcher.find()) {
				String key = matcher.group(1);// 1: key without ""
				keyObj = key;
				strActual = objActual.getJSONObject(parentKey).getJSONArray(key).toString();
				try {
					JSONObject objExpect = new JSONObject("{" + strKeyWithValue); // convert expect in excel string to
																					// JSONobj
					strExpect = objExpect.getJSONArray(key).toString();
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("ERROR: Cannot convert String expect result to JSONobject");
				}
			}
			listActualExpect.add(strActual);
			listActualExpect.add(strExpect);
		}

		// In case object is array. ex: "result":[1,2,3]
		else if (strKeyWithValue.contains("\":[")) {
			String[] arrObjValue = strKeyWithValue.split(":\\[");
			keyObj = FormatUtils.removeSpecialChar(arrObjValue[0]);
//    		strActual = obj.getJSONObject(parentKey).getJSONArray(ltrim(arrObjValue[0]).replace("\"","")).toString();                      

			// List is empty
			if (getValueOfJson(objActual, keyObj).size() == 0) {
				strActual = "[]";
			} // List not empty
			else {
				strActual = getValueOfJson(objActual, keyObj).get(0);
			}
			strExpect = "[" + arrObjValue[1].replace("}", "");
			listActualExpect.add(strActual);
			listActualExpect.add(strExpect);
		} else {
			String[] arrObjValue = strKeyWithValue.split("\":");
			keyObj = FormatUtils.removeSpecialChar(arrObjValue[0]);
			JSONObject prevObj = objActual.getJSONObject(parentKey);

			// Get correct value of key "version_no" in API Order
			if (keyObj.contains("version_no")) {
				for (String item : getValueOfJson(prevObj, keyObj)) {
					if (!item.equals("null")) {
						strActual = item;
					}
				}
			} else {
				try {
					strActual = getValueOfJson(prevObj, keyObj).get(0).trim();
				} catch (Exception e) {
					// TODO: handle exception
					strActual = "Cannot find this key";
				}

			}
			if(arrObjValue[1].contains("\\\"")) {
				strExpect = arrObjValue[1].replace("\\\"", "").replace("}", "").trim();
			}else {
				strExpect = arrObjValue[1].replaceAll("[\"\\}]", "").trim();
			}
			// add actual and expect value into list
			listActualExpect.add(strActual);
			listActualExpect.add(strExpect);

		}

		return listActualExpect;
	}

	public List<String> getActualExpectFromObject(String strKeyWithValue, JSONObject objActual) {
		String strActual = "";
		String strExpect = "";
		List<String> listActualExpect = new ArrayList<String>();

		// In case object is array contain object. ex:
		// {"result":[{"name":"nmn","age":"123"}]}
		if (strKeyWithValue.contains("\":[{")) {

			Pattern pattern = Pattern.compile("\"(.*?)\"");
			Matcher matcher = pattern.matcher(strKeyWithValue);
			if (matcher.find()) {
				String key = matcher.group(1);// 1: key without ""
				keyObj = key;
				strActual = objActual.getJSONArray(key).toString();

				try {
					JSONObject objExpect = new JSONObject(strKeyWithValue); // convert expect in excel string to JSONobj
					strExpect = objExpect.getJSONArray(key).toString(); // get value of json expect
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("ERROR: Cannot convert String expect result to JSONobject");
				}

			}

			listActualExpect.add(strActual);
			listActualExpect.add(strExpect);
		}

		// In case object is array. ex: "result":[1,2,3]
		else if (strKeyWithValue.contains("\":[")) {
			String[] arrObjValue = strKeyWithValue.split(":\\[");
			keyObj = FormatUtils.removeSpecialChar(arrObjValue[0]);
//          strActual = obj.getJSONArray(ltrim(arrObjValue[0]).replace("\"","")).toString();    

			if (getValueOfJson(objActual, keyObj).size() == 0) {
				strActual = "[]";
			} else {
				strActual = getValueOfJson(objActual, keyObj).get(0);
			}
			strExpect = "[" + arrObjValue[1].replace("}$", "");

			// add actual and expect value into list
			listActualExpect.add(strActual);
			listActualExpect.add(strExpect);
		}

		else {
			String[] arrObjValue = strKeyWithValue.split("\":");
			keyObj = FormatUtils.removeSpecialChar(arrObjValue[0]);

			// Get correct value of key "version_no" in API Order
			if (keyObj.contains("version_no")) {
				for (String item : getValueOfJson(objActual, keyObj)) {
					if (!item.equals("null")) {
						strActual = item;
					}
				}
			} else {
				try {					
					strActual = getValueOfJson(objActual, keyObj).get(0).trim();
				
				} catch (Exception e) {
					// TODO: handle exception
					strActual = "Cannot find this key";
				}

			}

			strExpect = arrObjValue[1].replaceAll("[\"\\}]", "").trim();

			// add actual and expect value into list
			listActualExpect.add(strActual);
			listActualExpect.add(strExpect);
		}

		return listActualExpect;
	}
	private static boolean isContain(String source, String subItem){
		String pattern = "\\b"+subItem+"\\b";
		Pattern p=Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
		Matcher m=p.matcher(source);
		return m.find();
	}

	public String replaceTextDateTime(String excelbody,String datetime,int numDate, String format){
		if (isContain(excelbody,datetime)) {
			String sku_date= getDays(numDate,format);
			excelbody = excelbody.replaceFirst(datetime, sku_date);
		}
		return excelbody;
	}

	public String replaceTextDateTime(String excelbody,String datetime,int numTime, String format,String timezone){
		if (isContain(excelbody,datetime)) {
			String sku_date= getTimes(numTime,format,timezone);
			excelbody = excelbody.replaceFirst(datetime, sku_date);
		}
		return excelbody;
	}
	public String getResponse(String url, String method, String token, String excelbody) {
		try {	
			switch (method) {
			case "POST":
				if (excelbody.contains("NameWarehouse")) {
					strNameWarehouse = getRandom();
					excelbody = excelbody.replace("NameWarehouse", strNameWarehouse);
				}

				// Create random OrderNumber in TC_OMS, StoreSKU in TC_SKU
				if (excelbody.contains("OrderRandom1")) {
					strOrderNumber1 = getRandom();
					strOrderId1 = strOrderNumber1;
					excelbody = excelbody.replace("OrderRandom1", strOrderNumber1);
					System.out.println("********Tạo Order Number 1 :" + strOrderNumber1 + "**********");
				}
				if (excelbody.contains("OrderRandom2")) {
					strOrderNumber2 = getRandom();
					strOrderId2 = strOrderNumber2;
					excelbody = excelbody.replace("OrderRandom2", strOrderNumber2);
					System.out.println("********Tạo Order Number 2 :" + strOrderNumber2 + "**********");
				}
				if (excelbody.contains("OrderRandom3")) {
					strOrderNumber3 = getRandom();
					strOrderId3 = strOrderNumber3;
					excelbody = excelbody.replace("OrderRandom3", strOrderNumber3);
					System.out.println("********Tạo Order Number 3 :" + strOrderNumber3 + "**********");
				}
				if (excelbody.contains("OrderRandom") || excelbody.contains("StoreSKU")) {
					strOrderNumber = getRandom();
					strOrderId = strOrderNumber;
					excelbody = excelbody.replace("OrderRandom", strOrderNumber);
					excelbody = excelbody.replace("StoreSKU", strOrderNumber);
					System.out.println("********Tạo Order Number || StoreSKU Random:" + strOrderNumber + "**********");
				}


				//Using in shopconfig
				excelbody = replaceTextDateTime(excelbody,"SHOP_DATETIME_ADD3",10,"yyyy-MM-dd'T'HH:mm:ss");

//				excelbody = replaceTextDateTime(excelbody,"SKU_DATE_SUB1",-1,"yyyy-MM-dd");
//				excelbody = replaceTextDateTime(excelbody,"SKU_DATE",0,"yyyy-MM-dd");
//				excelbody = replaceTextDateTime(excelbody,"SKU_DATE_ADD1",1,"yyyy-MM-dd");
//				excelbody = replaceTextDateTime(excelbody,"SKU_DATE_ADD181",181,"yyyy-MM-dd");
//				excelbody = replaceTextDateTime(excelbody,"SKU_DATE_ADD35",35,"yyyy-MM-dd");


				//Using in TC_SKU
				if (excelbody.contains("SKUCurrentDate+181")) {
					String sku_date= getDays(181,"yyyy-MM-dd");
					excelbody = excelbody.replace("SKUCurrentDate+181", sku_date);
				}
				if (excelbody.contains("SKUCurrentDate+30")) {
					String sku_date= getDays(30,"yyyy-MM-dd");
					excelbody = excelbody.replace("SKUCurrentDate+30", sku_date);
				}
				if (excelbody.contains("SKUCurrentDate+35")) {
					String sku_date= getDays(35,"yyyy-MM-dd");
					excelbody = excelbody.replace("SKUCurrentDate+35", sku_date);
				}
				if (excelbody.contains("SKUCurrentDate+2")) {
					String sku_date= getDays(2,"yyyy-MM-dd");
					excelbody = excelbody.replace("SKUCurrentDate+2", sku_date);
				}

				if (excelbody.contains("SKUCurrentDate+1")) {
					String sku_date= getDays(1,"yyyy-MM-dd");
					excelbody = excelbody.replace("SKUCurrentDate+1", sku_date);
				}
				if (excelbody.contains("SKUCurrentDate-1")) {
					String sku_date= getDays(-1,"yyyy-MM-dd");
					excelbody = excelbody.replace("SKUCurrentDate-1", sku_date);
				}
				if (excelbody.contains("SKUCurrentDate")) {
					String sku_date= getCurrentDate();
					excelbody = excelbody.replace("SKUCurrentDate", sku_date);
				}

										
				// Using in TC Order
				if (excelbody.contains("OrderCurrentDate+4")) {
					String order_date = getDays(+4, "yyyy-MM-dd");
					excelbody = excelbody.replace("OrderCurrentDate+4", order_date);
				}
				
				if (excelbody.contains("OrderCurrentDate+1")) {
					String order_date = getDays(+1, "yyyy-MM-dd");
					excelbody = excelbody.replace("OrderCurrentDate+1", order_date);
				}
										
				if (excelbody.contains("CurrentDay-14")) {
					String str_Today_Past14 = getDays(-14, "yyyy-MM-dd");
					excelbody = excelbody.replace("CurrentDay-14", str_Today_Past14);
				}

				if (excelbody.contains("OrderCurrentDate")) {
					String date = getDays(0, "dd-MM-yyyy");
					excelbody = excelbody.replace("OrderCurrentDate", date);
				}

				// Using in TC OMS
				if (excelbody.contains("CurrentDate-1")) {
					String str_Today = getDays(-1, "yyyy-MM-dd HH:mm:ss");
					excelbody = excelbody.replace("CurrentDate-1", str_Today);
				}
				if (excelbody.contains("CurrentDate+1")) {
					String str_Today = getDays(1, "yyyy-MM-dd HH:mm:ss");
					excelbody = excelbody.replace("CurrentDate+1", str_Today);
				}
				// Tự lấy ngày hiện tại theo định dạng ("yyyy-MM-dd HH:mm:ss") cho API của OMS
				if (excelbody.contains("CurrentDate")) {
					String str_Today = getCurrentDateTime();
					excelbody = excelbody.replace("CurrentDate", str_Today);
				}
				// Lấy mã đơn hàng vừa tạo cho API của TC OMS
				if (excelbody.contains("GetOrderNumber1")) {
					excelbody = excelbody.replace("GetOrderNumber1", strOrderNumber1);
				}
				if (excelbody.contains("GetOrderNumber2")) {
					excelbody = excelbody.replace("GetOrderNumber2", strOrderNumber2);
				}
				if (excelbody.contains("GetOrderNumber")) {
					excelbody = excelbody.replace("GetOrderNumber", strOrderNumber);
				}

				// app, Hình thức thanh toán trực tuyến
				if (excelbody.contains("SetSeqNo")) {
					excelbody = excelbody.replace("SetSeqNo", str_SeqNo);
				}

				if (excelbody.contains("SetPrimaryTerm")) {
					excelbody = excelbody.replace("SetPrimaryTerm", strPrimaryTerm);
				}

				// Using in TC Order
				if (excelbody.contains("GetVersionNo")) {
					excelbody = excelbody.replace("GetVersionNo", str_VersionNo);
				}
				if (excelbody.contains("GetOrderID")) {
					excelbody = excelbody.replace("GetOrderID", strOrderId);
				}
				if (excelbody.contains("SKUDuplicate")) {
					excelbody = excelbody.replace("SKUDuplicate", strOrderNumber);
				}

				// Lấy ngày hiện tai format dd/mm/yyyy
				if (excelbody.contains("CurrentOnlyDate")) {
					String date = getDays(0, "dd/MM/yyyy");
					excelbody = excelbody.replace("CurrentOnlyDate", date);
				}

				if (excelbody.contains("Today")) {
					String str_Today = getDays(0, "yyyy-MM-dd");
					excelbody = excelbody.replace("Today", str_Today);
				}

				// Using in TC Order
				if (excelbody.contains("SetTokenRuntime")) {
					excelbody = excelbody.replace("SetTokenRuntime", strTokenRuntime);
				}
				if (excelbody.contains("GetID2")) {
					excelbody = excelbody.replace("GetID2", strIdOfOrder2);
				}

				if (excelbody.contains("GetID1")) {
					excelbody = excelbody.replace("GetID1", strIdOfOrder1);
				}

				if (excelbody.contains("GetID")) {
					excelbody = excelbody.replace("GetID", strIdOfOrder);
				}
				
				if (excelbody.contains("SetVersionNo1")) {
					excelbody = excelbody.replace("SetVersionNo1", str_VersionNo1);
				}
				
				if (excelbody.contains("SetVersionNo")) {
					excelbody = excelbody.replace("SetVersionNo", str_VersionNo);
				}

				if (excelbody.contains("GetExternalID1")) {
					excelbody = excelbody.replace("GetExternalID1", strExternalId1);
				}

				if (excelbody.contains("GetExternalID2")) {
					excelbody = excelbody.replace("GetExternalID2", strExternalId2);
				}
				
				if (excelbody.contains("SetLabelID")) {
					excelbody = excelbody.replace("SetLabelID", strLabelId);
				}
				
								
				// USing in SKU
				if (excelbody.contains("100000Char")) {
					excelbody = excelbody.replace("100000Char", FormatUtils.getSaltString(100001));
				}

				if (excelbody.contains("100001Char")) {
					excelbody = excelbody.replace("100001Char", FormatUtils.getSaltString(100002));
				}
				
				
				//Set value epoch time								
				if (excelbody.contains("CurrentEpochDate-1")) {
					String date = String.valueOf(getDaysEpoch(-1));
					excelbody = excelbody.replace("CurrentEpochDate-1", date);
				}
				
				if (excelbody.contains("CurrentEpochDate-2")) {
					String date = String.valueOf(getDaysEpoch(-2));
					excelbody = excelbody.replace("CurrentEpochDate-2", date);
				}
				
				if (excelbody.contains("CurrentEpochDate-3")) {
					String date = String.valueOf(getDaysEpoch(-3));
					excelbody = excelbody.replace("CurrentEpochDate-3", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+900")) {
					String date = String.valueOf(getDaysEpoch(900));
					excelbody = excelbody.replace("CurrentEpochDate+900", date);
				}
					
				if (excelbody.contains("CurrentEpochDate+160")) {
					String date = String.valueOf(getDaysEpoch(160));
					excelbody = excelbody.replace("CurrentEpochDate+160", date);
				}
				if (excelbody.contains("CurrentEpochDate+161")) {
					String date = String.valueOf(getDaysEpoch(161));
					excelbody = excelbody.replace("CurrentEpochDate+161", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+181")) {
					String date = String.valueOf(getDaysEpoch(181));
					excelbody = excelbody.replace("CurrentEpochDate+181", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+90")) {
					String date = String.valueOf(getDaysEpoch(90));
					excelbody = excelbody.replace("CurrentEpochDate+90", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+91")) {
					String date = String.valueOf(getDaysEpoch(91));
					excelbody = excelbody.replace("CurrentEpochDate+91", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+1")) {
					String date = String.valueOf(getDaysEpoch(1));
					excelbody = excelbody.replace("CurrentEpochDate+1", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+2")) {
					String date = String.valueOf(getDaysEpoch(2));
					excelbody = excelbody.replace("CurrentEpochDate+2", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+3")) {
					String date = String.valueOf(getDaysEpoch(3));
					excelbody = excelbody.replace("CurrentEpochDate+3", date);
				}

				if (excelbody.contains("CurrentEpochDate+4")) {
					String date = String.valueOf(getDaysEpoch(4));
					excelbody = excelbody.replace("CurrentEpochDate+4", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+6")) {
					String date = String.valueOf(getDaysEpoch(6));
					excelbody = excelbody.replace("CurrentEpochDate+6", date);
				}
								
				
				if (excelbody.contains("CurrentEpochDate")) {
					String date = String.valueOf(getDaysEpoch(0));
					excelbody = excelbody.replace("CurrentEpochDate", date);
				}

				if (excelbody.contains("CurrentEpochTime+3m")) {
					long timestamp = getCurrentTimestamp()+180;
					timeStampAdd4 = String.valueOf(timestamp);
					excelbody = excelbody.replace("CurrentEpochTime+1m", timeStampAdd4);
				}

				if (excelbody.contains("CurrentEpochTime+1")) {
					long timestamp = getCurrentTimestamp()+3600;
					timeStampAdd1 = String.valueOf(timestamp);
					excelbody = excelbody.replace("CurrentEpochTime+1", timeStampAdd1);
				}
				if (excelbody.contains("CurrentEpochTime-1")) {
					long timestamp = getCurrentTimestamp() - 3600;
					timeStampAdd2 = String.valueOf(timestamp);
					excelbody = excelbody.replace("CurrentEpochTime-1", timeStampAdd2);
				}
				
				if (excelbody.contains("CurrentEpochTime+2")) {
					long timestamp = getCurrentTimestamp()+7200;
					timeStampAdd3 = String.valueOf(timestamp);
					excelbody = excelbody.replace("CurrentEpochTime+2", timeStampAdd3);
				}
				if (excelbody.contains("CurrentEpochTime+48")) {
					long timestamp = getCurrentTimestamp()+172800;
					timeStampAdd4 = String.valueOf(timestamp);
					excelbody = excelbody.replace("CurrentEpochTime+48", timeStampAdd4);
				}

				if (excelbody.contains("CurrentEpochTime")) {
					long timestamp = getCurrentTimestamp()+60;
					timeStampAdd4 = String.valueOf(timestamp);
					excelbody = excelbody.replace("CurrentEpochTime", timeStampAdd4);
				}


				
				if (excelbody.contains("SetEpochTime+1")) {
					excelbody = excelbody.replace("SetEpochTime+1", timeStampAdd1);					
				}
				
				if (excelbody.contains("SetEpochTime+2")) {
					excelbody = excelbody.replace("SetEpochTime+2", timeStampAdd2);					
				}

				if (excelbody.contains("SetProductId")) {
					excelbody = excelbody.replace("SetProductId", str_ProductId);
				}
				// Lấy mã đơn hàng vừa tạo, gắn vào url cho tính năng Báo cáo xấu

				if (url.contains("GetOrderID")) {
					url = url.replace("GetOrderID", strOrderId);
				}

				if (url.contains("GetProductId")) {
					url = url.replace("GetProductId", str_ProductId);
				}
				if (token==null) {
					System.out.println("Sai thông tin đăng nhập");
					jsonActualResult = "Sai thông tin đăng nhập";
					return "Invalid Credetial";
				}	
				jsonInput = excelbody;
				response = run("POST", url, excelbody, token);
				break;
			case "GET":
				// Get value param in response refer to URL
				if (url.contains("GetProductId")) {
					url = url.replace("GetProductId", str_ProductId);

				}
				// Using in Order TC
				if (url.contains("GetOrderID1")) {
					url = url.replace("GetOrderID1", strOrderId1);
				}
				if (url.contains("GetOrderID2")) {
					url = url.replace("GetOrderID2", strOrderId2);
				}
				if (url.contains("GetOrderID")) {
					url = url.replace("GetOrderID", strOrderId);
				}

				if (url.contains("GetOrderNumber1")) {
					url = url.replace("GetOrderNumber1", strOrderNumber1);
				}
				if (url.contains("GetOrderNumber")) {
					url = url.replace("GetOrderNumber", strOrderNumber);
				}
				if (url.contains("GetDataId")) {
					url = url.replace("GetDataId", strDataId);
				}

				// Using in Order 
				if (url.contains("GetID")) {
					url = url.replace("GetID", strIdOfOrder);
				}
								
				if (url.contains("SetProductId")) {
					url = url.replace("SetProductId", str_ProductId);
				}

				//using in Promotion
				if (url.contains("SetPromotionId")) {
					url = url.replace("SetPromotionId", str_PromotionId);
				}
				response = run("GET", url, "N/A", token);
				break;
			case "PUT":
				if (excelbody.contains("SetStoreSKU")) {
					strOrderNumber = getRandom();
					excelbody = excelbody.replace("SetStoreSKU", strOrderNumber);
				}

				if (excelbody.contains("SetVersionNo")) {
					excelbody = excelbody.replace("SetVersionNo", str_VersionNo);
				}
				if (excelbody.contains("SetProductId")) {
					excelbody = excelbody.replace("SetProductId", str_ProductId);
				}
				
				if (excelbody.contains("SetLabelID")) {
					excelbody = excelbody.replace("SetLabelID", strLabelId);
				}
				if (excelbody.contains("CurrentEpochDate+181")) {
					String date = String.valueOf(getDaysEpoch(181));
					excelbody = excelbody.replace("CurrentEpochDate+181", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+160")) {
					String date = String.valueOf(getDaysEpoch(160));
					excelbody = excelbody.replace("CurrentEpochDate+160", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+90")) {
					String date = String.valueOf(getDaysEpoch(90));
					excelbody = excelbody.replace("CurrentEpochDate+90", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+91")) {
					String date = String.valueOf(getDaysEpoch(91));
					excelbody = excelbody.replace("CurrentEpochDate+91", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+2")) {
					String date = String.valueOf(getDaysEpoch(2));
					excelbody = excelbody.replace("CurrentEpochDate+2", date);
				}
				
				if (excelbody.contains("CurrentEpochDate+4")) {
					String date = String.valueOf(getDaysEpoch(4));
					excelbody = excelbody.replace("CurrentEpochDate+4", date);
				}	
				if (excelbody.contains("CurrentEpochDate+1")) {
					String date = String.valueOf(getDaysEpoch(1));
					excelbody = excelbody.replace("CurrentEpochDate+1", date);
				}
				
				if (excelbody.contains("CurrentEpochDate-4")) {
					String date = String.valueOf(getDaysEpoch(-4));
					excelbody = excelbody.replace("CurrentEpochDate-4", date);
				}
				
				if (excelbody.contains("CurrentEpochDate-1")) {
					String date = String.valueOf(getDaysEpoch(-1));
					excelbody = excelbody.replace("CurrentEpochDate-1", date);
				}
				
				if (excelbody.contains("CurrentEpochDate")) {
					String date = String.valueOf(getDaysEpoch(0));
					excelbody = excelbody.replace("CurrentEpochDate", date);
				}

				response = run("PUT", url, excelbody, token);
				break;
			case "DELETE":
				if (url.contains("GetLabelID1")) {
					url = url.replace("GetLabelID1", strLabelId1);
				}
				if (url.contains("GetLabelID2")) {
					url = url.replace("GetLabelID2", strLabelId2);
				}
				
				if (url.contains("GetLabelID3")) {
					url = url.replace("GetLabelID3", strLabelId3);
				}
				if (url.contains("GetLabelID4")) {
					url = url.replace("GetLabelID4", strLabelId4);
				}
				if (url.contains("GetLabelID5")) {
					url = url.replace("GetLabelID5", strLabelId5);
				}
				if (url.contains("GetLabelID")) {
					url = url.replace("GetLabelID", strLabelId);
				}
							
				response = run("DELETE", url, excelbody, token);
				break;
			default:
				break;
			}				
		} catch (Exception ex) {
			jsonActualResult = ex.toString();
			response = ex.toString();
			System.out.print("Error send request: " + jsonActualResult);
		}
		return response;
	}

	public String getResponse(String url, String method, String token, String excelbody, String header) {
		try {
			switch (method) {
				case "POST":
					if (token==null) {
						System.out.println("Sai thông tin đăng nhập");
						jsonActualResult = "Sai thông tin đăng nhập";
						return "Invalid Credetial";
					}
					jsonInput = excelbody;
					response = runWithCustomHeader("POST", url, excelbody, token, header);
					break;
				case "GET":
					response = runWithCustomHeader("GET", url, "N/A", token,header);
					break;
				case "PUT":
					response = run("PUT", url, excelbody, token);
					break;
				case "DELETE":
					response = run("DELETE", url, excelbody, token);
					break;
				default:
					break;
			}
		} catch (Exception ex) {
			jsonActualResult = ex.toString();
			response = ex.toString();
			System.out.print("Error send request: " + jsonActualResult);
		}
		return response;
	}

	public String replaceValueExpect(String expectedResult) {				
		
		//Using in TC_Order
		if (expectedResult.contains("SetID")) {
			expectedResult = expectedResult.replace("SetID", strIdOfOrder);
		}
		if (expectedResult.contains("SetLabelID1")) {
			expectedResult = expectedResult.replace("SetLabelID1", strLabelId1);
		}
		
		if (expectedResult.contains("SetLabelID")) {
			expectedResult = expectedResult.replace("SetLabelID", strLabelId);
		}
		
		// Lấy mã đơn hàng vừa tạo, so sánh với kết quả response trả về và kết quả trong excel
		if (expectedResult.contains("GetOrderID")) {
			expectedResult = expectedResult.replace("GetOrderID", strOrderId);
		}
		// Chuyển ngày trong kết quả excel theo định dạng ("yyyy-MM-dd")
		if (expectedResult.contains("CurrentDate30")) {
			// String str_Today30 = getCurrentDate30();
			String str_Today30 = getDays(30, "yyyy-MM-dd");
			expectedResult = expectedResult.replace("CurrentDate30", str_Today30);
		}
		// Chuyển ngày trong kết quả excel theo định dạng ("dd/MM/yyyy")
		if (expectedResult.contains("CurrentDate1")) {
			// String str_Today1 = getCurrentDate1();
			String str_Today1 = getDays(1, "dd/MM/yyyy");
			expectedResult = expectedResult.replace("CurrentDate1", str_Today1);
		}

		// 7 days before today
		if (expectedResult.contains("CurrentDay-7")) {
			String str_Today_Past14 = getDays(-7, "yyyy-MM-dd");
			expectedResult = expectedResult.replace("CurrentDay-7", str_Today_Past14);
		}

		if (expectedResult.contains("Today")) {
			String str_Today = getDays(0, "yyyy-MM-dd");
			expectedResult = expectedResult.replace("Today", str_Today);
		}

		// Chuyển ngày trong kết quả excel theo định dạng("yyyy-MM-dd")
		if (expectedResult.contains("CurrentDate")) {
			String str_Today = getCurrentDate();
			expectedResult = expectedResult.replace("CurrentDate", str_Today);
		}
		if (expectedResult.contains("CurrentEpochGMT+1")) {
			String date = String.valueOf(getDaysEpochGMT(1));
			expectedResult = expectedResult.replace("CurrentEpochGMT+1", date);
		}
		
		if (expectedResult.contains("CurrentEpochGMT")) {
			String date = String.valueOf(getDaysEpochGMT(0));
			expectedResult = expectedResult.replace("CurrentEpochGMT", date);
		}
		
		if (expectedResult.contains("CurrentEpochDate")) {
			String date = String.valueOf(getDaysEpoch(0));
			expectedResult = expectedResult.replace("CurrentEpochDate", date);
		}

		if (expectedResult.contains("SetSeqNo+1")) {
			int SeqNoAdd1 = Integer.valueOf(str_SeqNo) + 1;
			expectedResult = expectedResult.replace("SetSeqNo+1", String.valueOf(SeqNoAdd1));
		}

		if (expectedResult.contains("SetPrimaryTerm")) {
			expectedResult = expectedResult.replace("SetPrimaryTerm", strPrimaryTerm);
		}

		// Using in Shopconfig-TraGop TC
		if (expectedResult.contains("SetTimeInstallment")) {
			String strTime = strDatePayLater.substring(11, 19);
			SimpleDateFormat df = new SimpleDateFormat("HH:m");
			Calendar c = Calendar.getInstance();
			try {
				c.setTime(df.parse(strTime));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("Wrong format time");
				e.printStackTrace();
			}
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int min = c.get(Calendar.MINUTE);
			expectedResult = expectedResult.replace("SetTimeInstallment", hour + ":" + min);
		}

		// Using in Shopconfig-TraGop TC
		if (expectedResult.contains("SetDateInstallment")) {
			String strDate = strDatePayLater.substring(0, 10);
			String strTime = strDatePayLater.substring(11, 19);
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat newdf = new SimpleDateFormat("dd/MM/yyyy");
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			try {
				cal.setTime(df.parse(strDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("Wrong format date");
				e.printStackTrace();
			}
			cal.add(Calendar.DATE, 7);
			date = cal.getTime();
			String output = newdf.format(date);
			expectedResult = expectedResult.replace("SetDateInstallment", output + " " + strTime);
		}

		// Using in Paylater TC
		if (expectedResult.contains("SetTimePayLater")) {
			String strDate = strDatePayLater.substring(11, 16);
			SimpleDateFormat df = new SimpleDateFormat("HH:mm");
			Calendar c = Calendar.getInstance();
			try {
				c.setTime(df.parse(strDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("Wrong format time");
				e.printStackTrace();
			}
			c.add(Calendar.HOUR, 7);
			String newStrTime = df.format(c.getTime());
			expectedResult = expectedResult.replace("SetTimePayLater", newStrTime);
		}

		// Using in Paylater TC
		if (expectedResult.contains("SetDatePayLater")) {
			String strDate = strDatePayLater.substring(0, 10);
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat newdf = new SimpleDateFormat("dd/MM/yyyy");
			Calendar cal = Calendar.getInstance();
			Date date = new Date();
			try {
				cal.setTime(df.parse(strDate));				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("Wrong format date");
				e.printStackTrace();
			}
			cal.add(Calendar.DATE, 7);
			date = cal.getTime();
			String output = newdf.format(date);
//			String output = getDays(7, "dd/MM/yyyy");
			expectedResult = expectedResult.replace("SetDatePayLater", output);
		}

		// Using shopconfig TC
		if (expectedResult.contains("SetTimeCarrier")) {
			String strTime = strDatePayLater.substring(11, 19);
			SimpleDateFormat df = new SimpleDateFormat("HH:mm");
			Calendar c = Calendar.getInstance();
			try {
				c.setTime(df.parse(strTime));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("Wrong format time");
				e.printStackTrace();
			}
			c.add(Calendar.MINUTE, 30);
			String newStrTime = df.format(c.getTime());
			expectedResult = expectedResult.replace("SetTimeCarrier", newStrTime);
		}

		// Using shopconfig TC
		if (expectedResult.contains("SetDateCarrier")) {
			String strDate = strDatePayLater.substring(0, 10);
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat newdf = new SimpleDateFormat("dd/MM/yyyy");
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			try {
				cal.setTime(df.parse(strDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("Wrong format date");
				e.printStackTrace();
			}
			date = cal.getTime();
			String output = newdf.format(date);
			expectedResult = expectedResult.replace("SetDateCarrier", output);
		}

		// Using shopconfig TC - Sendo Carrier
		if (expectedResult.contains("SetTimeSendoCarrier")) {
			String strTime = strDatePayLater.substring(11, 19);
			SimpleDateFormat df = new SimpleDateFormat("HH:mm");
			Calendar c = Calendar.getInstance();
			try {
				c.setTime(df.parse(strTime));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("Wrong format time");
				e.printStackTrace();
			}
			String newStrTime = df.format(c.getTime());
			expectedResult = expectedResult.replace("SetTimeSendoCarrier", newStrTime);
		}

		// Using shopconfig TC -SendoCarrier
		if (expectedResult.contains("SetDateSendoCarrier")) {
			String strDate = strDatePayLater.substring(0, 10);
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			try {
				cal.setTime(df.parse(strDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				System.out.println("Wrong format date");
				e.printStackTrace();
			}
			String output = getDays(1, "dd/MM/yyyy");
			expectedResult = expectedResult.replace("SetDateSendoCarrier", output);
		}

		return expectedResult;

	}

	public String getThenReplaceValueExpect(String expectResult, String actualResult) {		
		// Using in TC Order
		if (ltrim(expectResult).equals("GetOrderNumber1")) {
			strOrderNumber1 = actualResult;
			expectResult = expectResult.replace("GetOrderNumber1", strOrderNumber1);
		}
		
		if (ltrim(expectResult).equals("GetOrderNumber")) {
			strOrderNumber = actualResult;
			expectResult = expectResult.replace("GetOrderNumber", strOrderNumber);
		}
		
		if (ltrim(expectResult).equals("GetID1")) {
			strIdOfOrder1 = actualResult;
			expectResult = expectResult.replace("GetID1", strIdOfOrder1);
		}

		if (ltrim(expectResult).equals("GetID2")) {
			strIdOfOrder2 = actualResult;
			expectResult = expectResult.replace("GetID2", strIdOfOrder2);
		}

		if (ltrim(expectResult).equals("GetID")) {
			strIdOfOrder = actualResult;
			expectResult = expectResult.replace("GetID", strIdOfOrder);
		}

		if (ltrim(expectResult).equals("GetProductId")) {
			str_ProductId = actualResult;
			expectResult = expectResult.replace("GetProductId", str_ProductId);
		}

		if (ltrim(expectResult).equals("GetPrimaryTerm")) {
			strPrimaryTerm = actualResult;
			if (expectResult.contains("GetPrimaryTerm")) {
				expectResult = expectResult.replace("GetPrimaryTerm", strPrimaryTerm);
			}
		}
		
		if (ltrim(expectResult).equals("GetSeqNo")) {
			str_SeqNo = actualResult;
			if (expectResult.contains("GetSeqNo")) {
				expectResult = expectResult.replace("GetSeqNo", str_SeqNo);
			}
		}
		
		if (ltrim(expectResult).equals("GetLabelID1")) {
			strLabelId1 = actualResult;
			expectResult = expectResult.replace("GetLabelID1", strLabelId1 );
		}
		
		if (ltrim(expectResult).equals("GetLabelID2")) {
			strLabelId2 = actualResult;
			expectResult = expectResult.replace("GetLabelID2", strLabelId2 );
		}
		
		if (ltrim(expectResult).equals("GetLabelID3")) {
			strLabelId3 = actualResult;
			expectResult = expectResult.replace("GetLabelID3", strLabelId3 );
		}
		
		if (ltrim(expectResult).equals("GetLabelID4")) {
			strLabelId4 = actualResult;
			expectResult = expectResult.replace("GetLabelID4", strLabelId4 );
		}
		
		if (ltrim(expectResult).equals("GetLabelID5")) {
			strLabelId5 = actualResult;
			expectResult = expectResult.replace("GetLabelID5", strLabelId5 );
		}

		
		if (ltrim(expectResult).equals("GetLabelID")) {
			strLabelId = actualResult;
			expectResult = expectResult.replace("GetLabelID", strLabelId );
		}

		// Using in TC ShopConfig
		if (ltrim(expectResult).equals("GetDatePayLater")) {
			strDatePayLater = actualResult;
			if (expectResult.contains("GetDatePayLater")) {
				expectResult = expectResult.replace("GetDatePayLater", strDatePayLater);
			}
		}

		// Using in Order
		if (ltrim(expectResult).equals("GetVersionNo")) {
			str_VersionNo = actualResult;
			expectResult = expectResult.replace("GetVersionNo", str_VersionNo);
		}
		
		if (ltrim(expectResult).equals("GetVersionNo1")) {
			str_VersionNo1 = actualResult;
			expectResult = expectResult.replace("GetVersionNo1", str_VersionNo1);
		}

		if (ltrim(expectResult).equals("GetPromotionId")) {
			str_PromotionId = actualResult;
			expectResult = expectResult.replace("GetPromotionId", str_PromotionId);
		}

		return expectResult;
	}

	public boolean verifyReponse(String expectedResult, String jsobj) { // kiểm tra kết quả mong muốn, status code mà
																		// response trả về
		try {
			String str_arrexpectResult = "";
			message = "";
//			jsonActualResult = "";
			// Xử lý khoảng trắng giữa các object
			if (expectedResult.contains("; \"") || expectedResult.contains(": {") || expectedResult.contains("\": ")) {
				expectedResult = expectedResult.replace("; \"", ";\"").replace(": {", ":{").replace("\": ", "\":");
			}

			// Set value of key into expected result
			expectedResult = replaceValueExpect(expectedResult);
			
			JSONObject obj = new JSONObject(response);										
			// Lấy nguyên kết quả của respone
			if (jsobj == "") {
				jsonActualResult = response;
				keyObj = "Respone";
				// Using in test script OMS
				if (expectedResult.contains("GetDataId")) {
					try {
						strDataId = getValueOfJson(obj, "Data").get(0);
						expectedResult = expectedResult.replace("GetDataId", strDataId);
					} catch (Exception e) {
						// TODO: handle exception
						jsonActualResult = response;
					}

				}

				// Using in test script OMS
				if (expectedResult.contains("GetOrderNumber1")) {
					expectedResult = expectedResult.replace("GetOrderNumber1", strOrderNumber1);
				}
				
				if (expectedResult.contains("GetOrderNumber")) {
					expectedResult = expectedResult.replace("GetOrderNumber", strOrderNumber);
				}

				// Using in test script Order
				if (expectedResult.contains("GetID1")) {
					expectedResult = expectedResult.replace("GetID1", strIdOfOrder1);
				}
				
				if (expectedResult.contains("GetID")) {
					expectedResult = expectedResult.replace("GetID", strIdOfOrder);
				}
				
				System.out.println("Expected result: "+expectedResult);

			}
			// Lấy 1 phần kết quả trong result/data response
			else {
				// Nếu ExpectedResult có phân cấp (Ví dụ: result:{list_event})
				if (expectedResult.contains("\":{\"")) {
					
					System.out.println("CHECK CASE:CHILD OBJECT IN RESPONSE");
					// Lấy obj cần check trong response (Ví dụ: result, data,...)
					String[] arrjsobj = expectedResult.split("\":\\{", 2);
					jsobj = FormatUtils.removeSpecialChar(arrjsobj[0]);
					String[] arrJson;

					// Check từng kết quả trong ExpectResult
					if (arrjsobj[1].contains(";\"")) {
						int i_check = 0;
						// Cắt chuỗi để check từng kết quả trong ExpectedResult
						String[] arrExpectedResult = arrjsobj[1].split(";\"");
						for (int i = 0; i < arrExpectedResult.length; i++) {
							List<String> ActualExpectvalue = getActualExpectFromObject(arrExpectedResult[i], obj,jsobj);
							jsonActualResult = ActualExpectvalue.get(0);							
							str_arrexpectResult = ActualExpectvalue.get(1);
							
							arrJson = arrExpectedResult[i].split("\":");
							arrJson[0] = FormatUtils.removeSpecialChar(arrJson[0]);

							// Get dynamic value in response replace value in expected result
							str_arrexpectResult = getThenReplaceValueExpect(str_arrexpectResult, jsonActualResult);

							System.out.println("***************Key:*******************Row:" + i + "******"
									+ arrJson[0].toString());
							System.out.println(
									"***************ActualResult:***********Row:" + i + "*****" + jsonActualResult);
							System.out.println(
									"***************ExpectedResult:*********Row:" + i + "*****" + str_arrexpectResult);
							if (str_arrexpectResult.contains("[a-zA-Z0-9]]")) {// if string contain "[char] + ]"
								if (jsonActualResult.equals(str_arrexpectResult)) {
									i_check = 0;
								} else {
									i_check = 1;
									return false;
								}
							} else {
								if (jsonActualResult.equals(ltrim(str_arrexpectResult))) {
									i_check = 0;
								} else {
									i_check = 1;
									return false;
								}
							}
						}
						// Nếu tất cả kết quả trong ExpectedResult đúng hết thì return true
						if (i_check == 0) {
							return true;
						}
					}
					// Check 1 kết quả trong ExpectResult
					else {
						List<String> ActualExpectvalue = getActualExpectFromObject(arrjsobj[1], obj, jsobj);
						jsonActualResult = ActualExpectvalue.get(0);
						str_arrexpectResult = ActualExpectvalue.get(1);
						
						arrJson = arrjsobj[1].split("\":");
						arrJson[0] = FormatUtils.removeSpecialChar(arrJson[0]);

						// Get dynamic value in response replace value in expected result
						str_arrexpectResult = getThenReplaceValueExpect(str_arrexpectResult, jsonActualResult);

						System.out.println("***************Key:" + arrJson[0].toString());
						System.out.println("***************ActualResult:" + jsonActualResult);
						System.out.println("***************ExpectedResult:" + str_arrexpectResult);

						if (str_arrexpectResult.contains("[a-zA-Z0-9]]")) {
							if (jsonActualResult.equals(str_arrexpectResult)) {
								return true;
							} else {
								message = keyObj + "="+ jsonActualResult;
								return false;
							}
						} else {
							if (jsonActualResult.equals(ltrim(str_arrexpectResult))) {
								return true;
							} else {
								message = keyObj + "="+ jsonActualResult;
								return false;
							}
						}
					}
				}
				// Nếu ExpectedResult không có phân cấp, lấy trực tiếp object
				else {
					System.out.println("CHECK CASE:DIRECT RESPONSE");
					String[] arrJson;
					// Check từng kết quả trong ExpectResult
					if (expectedResult.contains(";\"")) {
						int i_check = 0;
						// Cắt chuỗi để check từng kết quả trong ExpectedResult
						String[] arrExpectedResult = expectedResult.split(";\"");
						for (int i = 0; i < arrExpectedResult.length; i++) {

							List<String> ActualExpectvalue = getActualExpectFromObject(arrExpectedResult[i], obj);
							jsonActualResult = ActualExpectvalue.get(0);
							str_arrexpectResult = ActualExpectvalue.get(1);

							arrJson = arrExpectedResult[i].split("\":");
							arrJson[0] = FormatUtils.removeSpecialChar(arrJson[0]);

							// Get dynamic value in response replace value in expected result
							str_arrexpectResult = getThenReplaceValueExpect(str_arrexpectResult, jsonActualResult);

							System.out.println("***************Key:*******************Row:" + i + "******"
									+ arrJson[0].toString());
							System.out.println(
									"***************ActualResult:***********Row:" + i + "******" + jsonActualResult);
							System.out.println(
									"***************ExpectedResult:*********Row:" + i + "******" + str_arrexpectResult);

							if (str_arrexpectResult.contains("[a-zA-Z0-9]]")) {
								if (jsonActualResult.equals("[" + str_arrexpectResult)) {
									i_check = 0;
								} else {
									i_check = 1;
									return false;
								}
							} else {
								if (jsonActualResult.equals(ltrim(str_arrexpectResult))) {
									i_check = 0;
								} else {
									i_check = 1;
									return false;
								}
							}
						}
						// Nếu tất cả kết quả trong ExpectedResult đúng hết thì return true
						if (i_check == 0) {
							return true;
						}
					}
					// Check 1 kết quả trong ExpectResult
					else {
						List<String> ActualExpectvalue = getActualExpectFromObject(expectedResult, obj);
						jsonActualResult = ActualExpectvalue.get(0);
						str_arrexpectResult = ActualExpectvalue.get(1);
						arrJson = expectedResult.split("\":");
						arrJson[0] = FormatUtils.removeSpecialChar(arrJson[0]);

						// Get dynamic value in response replace value in expected result
						str_arrexpectResult = getThenReplaceValueExpect(str_arrexpectResult, jsonActualResult);

						System.out.println("***************Key:" + arrJson[0].toString());
						System.out.println("***************ActualResult:" + jsonActualResult);
						System.out.println("***************ExpectedResult:" + str_arrexpectResult);

						if (str_arrexpectResult.contains("[a-zA-Z0-9]]")) {
							if (jsonActualResult.equals("[" + str_arrexpectResult)) {
								return true;
							} else {
								message = keyObj + "="+ jsonActualResult;
								return false;
							}
						} else {
							if (jsonActualResult.equals(ltrim(str_arrexpectResult))) {
								return true;
							} else {
								message = keyObj + "="+ jsonActualResult;
								return false;
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			jsonActualResult = ex.toString();
		}

		if (jsonActualResult.equals(expectedResult)) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean verifyFullResponse(String expectedResult) {
		jsonActualResult = "";
		message = "";
		expectedResult = replaceValueExpect(expectedResult);
		keyObj = "Respone";
		if(!response.contains("{")){ //In case response is not a jsonobject type
			jsonActualResult = response;
			if(jsonActualResult.equals(expectedResult))
				return true;
			else
				message = jsonActualResult;
				return false;
		}else {
			JSONObject obj = new JSONObject(response);
			if (expectedResult.contains("GetDataId")) {
				try {
					strDataId = getValueOfJson(obj, "Data").get(0);
					expectedResult = expectedResult.replace("GetDataId", strDataId);
				} catch (Exception e) {
					// TODO: handle exception
					jsonActualResult = response;
				}
			}
			// Using in test script OMS
			if (expectedResult.contains("GetOrderNumber1")) {
				expectedResult = expectedResult.replace("GetOrderNumber1", strOrderNumber1);
			}

			if (expectedResult.contains("GetOrderNumber")) {
				expectedResult = expectedResult.replace("GetOrderNumber", strOrderNumber);
			}

			// Using in test script Order
			if (expectedResult.contains("GetID1")) {
				expectedResult = expectedResult.replace("GetID1", strIdOfOrder1);
			}

			if (expectedResult.contains("GetID")) {
				expectedResult = expectedResult.replace("GetID", strIdOfOrder);
			}

			try {
				JSONAssert.assertEquals(expectedResult,response, JSONCompareMode.LENIENT);
				return true;
			} catch (AssertionError e) {
				message = e.getMessage();
				return false;
			}
		}
	}

	public boolean verifyResponse1(String actual, String expect) {
		keyObj = "Respone";	
		if(actual.equals(expect))
			return true;
		else 
			jsonActualResult = actual;
			return false;
	}

	public boolean verifyResponsesEqual(String expectResponse, String actualResponse){
		try {
			JSONAssert.assertEquals(expectResponse,actualResponse, JSONCompareMode.LENIENT);
			return true;
		} catch (AssertionError e) {
			message = e.toString();
			return false;
		}
	}

	public boolean verifyValueExistInList(JSONObject obj, String expectResult) {		
		String[] arrExpect = expectResult.split("\":\"");
		arrExpect[0] = arrExpect[0].replace("\"", "");		
		arrExpect[1] = arrExpect[1].replace("\"", "");		
		
		List<String> lst = getValueOfJson(obj,arrExpect[0]);
		arrExpect[1] = replaceValueExpect(arrExpect[1]);
			
		System.out.println("Value check in list: "+arrExpect[1]);
		boolean result = false;
		for (int i = 0; i < lst.size(); i++) {
			if(arrExpect[1].equals(lst.get(i))) {
				result=true;
				break;
			}
		}
		if (!result) {
			message = "Cannot find value: \""+arrExpect[1]+"\" in list \""+arrExpect[0]+"\"";
			return false;
		}
		return result;
	}

	public boolean verifyJSONArray(JSONArray arr1, JSONArray arr2){
		return JSON.compareJSONArrays(arr1,arr2);
	}

	public boolean verifyJSONArrayHaveElement(JSONObject jsonObject,String key){
		List<String> values = getValueOfArrayJson(jsonObject, key);
		if(values.size()>0)
			return true;
		else {
			message = "Array "+key+" do not have any value";
			return false;
		}
	}

	public String readValueByJsonPath(Object objJson, String key){
		try{
			return JsonPath.read(objJson,key).toString();
		}catch (PathNotFoundException e){
			e.printStackTrace();
			return null;
		}
	}

	public boolean verifyResponseNew(String expectResult){
		boolean result = false;
		message = "";
		Object objResponse = Configuration.defaultConfiguration().jsonProvider().parse(response);

		// Check multi keys in ExpectedResult cell
		if (expectResult.contains(";")){
			int i_check = 0;
			String [] arrKeyVerify = expectResult.split(";");
			for(int i = 0; i< arrKeyVerify.length;i++){
				if(arrKeyVerify[i].contains("!=")) {
					String[] arrExpectResult = arrKeyVerify[i].split("!=");
					String key = arrExpectResult[0].trim();
					String expectValue = arrExpectResult[1];
					String actualValue = readValueByJsonPath(objResponse,key);
					System.out.println("Key: "+key);
					System.out.println("Expect: "+expectValue);
					System.out.println("Actual: "+actualValue);
					if(!expectValue.equals(actualValue))
						i_check=0;
					else{
						i_check=1;
						message = key + " - expect actual value is differ with value "+expectValue;
						return result;}
				}else if(arrKeyVerify[i].contains("==")){
					String[] arrExpectResult = arrKeyVerify[i].split("==");
					String key = arrExpectResult[0].trim();
					String expectValue = arrExpectResult[1];
					String actualValue = readValueByJsonPath(objResponse,key);
					expectValue = replaceDynamicExpectValue(expectValue,actualValue);
					System.out.println("Key: "+key);
					System.out.println("Expect: "+expectValue);
					System.out.println("Actual: "+actualValue);
					if(expectValue.equals(actualValue))
						i_check=0;
					else{
						i_check=1;
						message = key + " - expect: "+expectValue+" but found: "+actualValue;
						return result;}
				}
			}
			if(i_check==0){
				result = true;
			}
		}
		// Check one key in ExpectedResult cell
		else{
			if(expectResult.contains("!=")) {
				String[] arrExpectResult = expectResult.split("!=");
				String key = arrExpectResult[0];
				String expectValue = arrExpectResult[1];
				String actualValue = readValueByJsonPath(objResponse,key);
				System.out.println("Key: "+key);
				System.out.println("Expect: "+expectValue);
				System.out.println("Actual: "+actualValue);
				if(!expectValue.equals(actualValue))
					result = true;
				else{
					result = false;
					message = key + " - expect actual value is differ with "+expectValue;}
			}else if(expectResult.contains("==")){
				String[] arrExpectResult = expectResult.split("==");
				String key = arrExpectResult[0];
				String expectValue = arrExpectResult[1];
				String actualValue = readValueByJsonPath(objResponse,key);
				System.out.println("Key: "+key);
				System.out.println("Expect: "+expectValue);
				System.out.println("Actual: "+actualValue);
				if(expectValue.equals(actualValue))
					result = true;
				else{
					result = false;
					message = key + " - expect: "+expectValue+" but found: "+actualValue;}
			} else if(expectResult.contains(">")){
				String[] arrExpectResult = expectResult.split(">");
				String key = arrExpectResult[0];
				int expectValue = Integer.valueOf(arrExpectResult[1].trim());
				int actualValue = Integer.valueOf(readValueByJsonPath(objResponse,key));
				System.out.println("Key: "+key);
				System.out.println("Expect: "+expectValue);
				System.out.println("Actual: "+actualValue);
				if(actualValue>expectValue)
					result = true;
				else{
					result = false;
					message = key + " - "+actualValue+" is not larger than "+expectValue; }
			} else if(expectResult.contains("<=")){
				String[] arrExpectResult = expectResult.split("<=");
				String key = arrExpectResult[0];
				int expectValue = Integer.valueOf(arrExpectResult[1].trim());
				int actualValue = Integer.valueOf(readValueByJsonPath(objResponse,key));
				System.out.println("Key: "+key);
				System.out.println("Expect: "+expectValue);
				System.out.println("Actual: "+actualValue);
				if(actualValue<=expectValue)
					result = true;
				else{
					result = false;
					message = key + " - "+actualValue+" is larger than "+expectValue; }
			} else if(expectResult.contains("<<")){
				String[] arrExpectResult = expectResult.split("<<");
				String key = arrExpectResult[0];
				String expectValue = arrExpectResult[1];
				String actualValue = readValueByJsonPath(objResponse,key);
				System.out.println("Key: "+key);
				System.out.println("Expect value in list: "+expectValue);
				System.out.println("List all values: "+actualValue);
				if(actualValue.contains(expectValue))
					result = true;
				else
					result = false;
					message = "Value: "+expectValue+" is not exist in list";
			}
		}
		return result;
	}

	public String replaceDynamicExpectValue(String expectDynamicValue, String actualResult) {
		switch (expectDynamicValue){
			case "ESSEARCH_TOKEN":
				strTokenRuntime = actualResult;
				expectDynamicValue = expectDynamicValue.replace("ESSEARCH_TOKEN", strTokenRuntime);
				break;
			case "LOG_DATA_WAREHOUSE":
				strDataId = actualResult;
				expectDynamicValue = expectDynamicValue.replace("LOG_DATA_WAREHOUSE", strDataId);
				break;
			case "LOG_NAME_WAREHOUSE":
				expectDynamicValue = expectDynamicValue.replace("LOG_NAME_WAREHOUSE", strNameWarehouse);
				break;
			default:
				break;
		}
		return expectDynamicValue;
	}


}
