package object;

import automationLibrary.actions.ActionGen;
import automationLibrary.data.Data;
import automationLibrary.data.TestCase;
import automationLibrary.database.Database;
import automationLibrary.initiations.Configurations;
import automationLibrary.initiations.ConstantLib;
import com.relevantcodes.extentreports.LogStatus;
import logger.MyLogger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.ITestContext;
import org.testng.annotations.*;
import runner.Runner;
import utils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BaseTest{
	public static int totalFailCase = 0;
	public static int totalPassCase = 0;
	
    protected XSSFWorkbook workbook;
    protected String sheetName;
    protected XSSFSheet sheet;
    public static String fileNameFromBaseTest;

    public static String environment = null;
    public static String os = System.getProperty("os.name");

    public static boolean isLocal = true;
    public static boolean realTimeRun = Configurations.realTimeRun;
    public long startTime = 0;
    public long endTime = 0;
    
    String resultPath = null;

    @BeforeSuite
    public void beforeSuite(ITestContext testContext) {
        if(Network.getIpAddress().contains("192.168.1.47")){
            realTimeRun = true;
        }
        System.out.println("Realtime: " + realTimeRun);
        startTime = System.currentTimeMillis();
        environment = System.getProperty("environment");
        if (environment == null) {
            environment = Configurations.environment;
        }
//        if(isLocal) {
//            environment = Configurations.environment;
//        }
    }

    @Parameters("fileName")
    @BeforeMethod
    public void initExcel(String fileName){
        System.out.println("Environment: " + environment);
        if(environment!=null){
            fileName = fileName + "_" + environment + ".xlsx";
        }else{
            fileName = fileName + "_" + "staging" + ".xlsx";
        }
        System.out.println(Constants.INPUT_PATH + fileName);

        workbook = XLSWorker.getWorkbook2(Constants.INPUT_PATH + fileName, sheetName);
        sheet = XLSWorker.getSheet(workbook,sheetName);
        fileNameFromBaseTest = fileName;
    }

//    public void reportToExcel(ITestResult iResult){
//        XSSFSheet sheet = XLSWorker.getSheet(workbook,sheetName);
//        if (iResult.getStatus() == ITestResult.FAILURE || iResult.getStatus() == ITestResult.SKIP) {
//            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet) - 1, "");
//            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet), "FAILED");
//        } else if (iResult.getStatus() == ITestResult.SUCCESS) {
//            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet), "PASSED");
//        }
//    }

    public void reportToExcel(Boolean result, int row, String errorMessage){
        if (!result) {
            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet) - 1, errorMessage);
            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet), "FAILED");
        } else {
            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet), "PASSED");
        }
    }

    public void reportToExcel(Boolean result, int row, String refs, String errorMessage){
    	XLSWorker.setHeightRow(sheet, row);
        
    	MyLogger.info("Report: " + "Result: " + result + " - " + "Ref: " + refs + " - " + "ErrorMessage: " + errorMessage);
        String testResult; 
        
        //hide password token on result
        String token_data = XLSWorker.getValueFromExcel(row, "Token",sheet.getSheetName());
        try {
        	if(token_data.contains("\"password\"")) {
          	  String token_pass = token_data.substring(token_data.indexOf("rd\":\"")+5, token_data.indexOf("\"}"));
                XLSWorker.updateExcel(sheet, row, 4, token_data.replace(token_pass, "XXXXX"));
          }
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Cannot hide password of token");
		}
        
      
        if (!result) {
            testResult = "FAILED";
//            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet) - 1, errorMessage);
//            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet), testResult);
            XLSWorker.updateExcelByColumnName(sheet, row, "Result", testResult);
            XLSWorker.updateExcelByColumnName(sheet, row, "Actual Response", errorMessage);
        } else {
            testResult = "PASSED";
//            XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet), testResult);
            XLSWorker.updateExcelByColumnName(sheet, row, "Result", testResult);
            errorMessage = " ";   
        }
//        if (os.contains("Linux")) {
//            for (int i = 0; i < Data.testCaseList.size(); i++) {
//                TestCase currentTestCase = Data.testCaseList.get(i);
//                System.out.println(currentTestCase.getRefs() + " - " + refs);
//                if(currentTestCase.getRefs().equals(refs)){
//                    currentTestCase.setTestResult(testResult);
//                    currentTestCase.setComment(errorMessage);
//                }
//            }
//        }
    }

    public void reportToExcel2(Boolean result, int row, String jsonInput, String jsonExpect){
        XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet) - 1, jsonExpect);
    }
    
    public void reportJsonInputToExcel(String method, Boolean result, int row, String jsonInput){
    	if(method.equals("POST")||method.equals("PUT"))
    		XLSWorker.updateExcel(sheet, row, XLSWorker.getLastCellNum(sheet) - 4, jsonInput);
    }
    
   
    @Parameters("fileName")
    @AfterTest
    public void writeExcel(String fileName){
        if(environment!=null){
            fileName = fileName + "_" + environment + ".xlsx";
        }else{
            fileName = fileName + "_" + "staging" + ".xlsx";
        }
        File theDir = new File(Constants.OUTPUT_PATH);
        if (!theDir.exists()){
            theDir.mkdirs();
        }
        XLSWorker.writeExcel(Constants.OUTPUT_PATH + "result_" + fileName , workbook);            
       
		/* get path file result to Drive */
        resultPath = Constants.OUTPUT_PATH + "result_" + fileName;       
    }

//    @AfterSuite
    public void afterSuite(ITestContext testContext) {
        if (os.contains("Linux")) {
            String testClassName = Data.getClassName();
            System.out.println("Updating result for '" + testClassName + "'...");
            for (int i = 0; i < Data.testCaseList.size(); i++) {
                TestCase currentTestCase = Data.testCaseList.get(i);
                String testCaseId = currentTestCase.getTestCaseId();
                String testRunId = currentTestCase.getTestRunId();
                String testResult = currentTestCase.getTestResult();
                int priority = currentTestCase.getPriority();
                ActionGen.countPriorityResult(testResult, priority);
                System.out.println("TestcaseId: " + testCaseId + ", Result: " + testResult);
                String logResult = "Case " + (i + 1) + ":" + "<br>&emsp;" + "Test case Id: " + testCaseId + "<br>&emsp;" + "Test result: " + testResult;
                if(testResult.equals("PASSED")) {
                    ActionGen.addReportLog(LogStatus.PASS, logResult);
                } else {
                    ActionGen.addReportLog(LogStatus.FAIL, logResult);
                }
                System.out.println("ADD LOG DONE");
                if(true) {
                    List<String> columns = new ArrayList<String>();
                    columns.add("result" + ConstantLib.SpecialCharacter.COMMA_WITH_SPACE + testResult);
                    System.out.println("result" + ConstantLib.SpecialCharacter.COMMA_WITH_SPACE + testResult);
                    columns.add("environment" + ConstantLib.SpecialCharacter.COMMA_WITH_SPACE + Runner.getEnvironment());
                    System.out.println("environment" + ConstantLib.SpecialCharacter.COMMA_WITH_SPACE + Runner.getEnvironment());
                    List<String> conditions = new ArrayList<String>();
                    conditions.add("testRunId" + ConstantLib.SpecialCharacter.COMMA_WITH_SPACE + testRunId);
                    System.out.println("testRunId" + ConstantLib.SpecialCharacter.COMMA_WITH_SPACE + testRunId);
                    conditions.add("testCaseId" + ConstantLib.SpecialCharacter.COMMA_WITH_SPACE + testCaseId);
                    System.out.println("testCaseId" + ConstantLib.SpecialCharacter.COMMA_WITH_SPACE + testCaseId);
                    Database.updateDatabase(Configurations.updatedTable, columns, conditions);
                }
            }

            String testClassResult = Data.getTestSuiteResult();
            System.out.println("testClassResult: " + testClassResult);
            if(testClassResult.equals("PASSED")) {
                Runner.passedSuite++;
            } else {
                Runner.failedSuite++;
            }

            if(realTimeRun) {
                System.out.println("Update result for Realtime run");
                endTime = System.currentTimeMillis();

                long duration = endTime - startTime;
                long second = (duration / 1000) % 60;
                long minute = (duration / (1000 * 60)) % 60;
                long hour = (duration / (1000 * 60 * 60)) % 24;
                String durationTime = String.format("%02d:%02d:%02d", hour, minute, second, duration);
                String testSuiteName = Data.getFeatureName();
                String testMethodName = Data.getMethodName();
                String executionID = ActionGen.getCurrentTimeByTimezoneOffset(Integer.valueOf(Configurations.timezoneOffset), "ddMMyyHHmmssSSS");
                String executionTime = ActionGen.getCurrentTimeByTimezoneOffset(Integer.valueOf(Configurations.timezoneOffset), "yyyy-MM-dd");
                String executionTimeLong = ActionGen.getCurrentTimeByTimezoneOffset(Integer.valueOf(Configurations.timezoneOffset), "yyyy-MM-dd HH:mm:ss");
                String environment = "API - " + Runner.getEnvironment().substring(0, 1).toUpperCase() + Runner.getEnvironment().substring(1,  Runner.getEnvironment().length()) ;
                List<String> values = new ArrayList<String>();
                values.add(0, executionID);
                values.add(1, environment);
                values.add(2, testSuiteName);
                values.add(3, testClassName);
                values.add(4, testMethodName);
                values.add(5, executionTime);
                values.add(6, executionTimeLong);
                values.add(7, durationTime);
                values.add(8, testClassResult);
                values.add(9, testClassResult);
                values.add(10, testClassResult);
                values.add(11, Runner.extentReportUrl);

                List<String> columns = Arrays.asList("Run_ID", "Environment", "Test_suite", "Test_class", "Test_method", "Execution_time", "Execution_time_long", "Duration_time", "First_run", "Second_run", "Final_result", "Report");

                Database.getConnection(Configurations.databaseUrl47, Configurations.databaseUsername, Configurations.databasePassword);
                Database.useDatabase(Configurations.realTimeDatabase);
                Database.insertIntoDatabase(Configurations.realTimeTable, columns, values);
            }

            if (testClassResult.equals("PASSED")) {
                ActionGen.addReportLog(LogStatus.PASS, testClassName + " <span class='test-status label right outline capitalize pass' style='align:left'>PASSED</span>");
            } else {
                ActionGen.addReportLog(LogStatus.FAIL, testClassName + " <span class='test-status label right outline capitalize fail' style='align:left'>FAILED</span>");
            }

            System.out.println("Finished!");
        }
      
    }
    
    @AfterSuite
    public void afterSuite2(ITestContext testContext) {
//    	sendNotificationHangsOut(testContext,resultPath);
    }

    public void sendNotificationHangsOut(ITestContext testContext, String resultPath) {
        System.out.println("-- Start to reporting --");
        String suiteName = testContext.getCurrentXmlTest().getSuite().getName();
        String currentTime = ActionGen.getCurrentTimeByTimezoneOffset(Integer.valueOf(Configurations.timezoneOffset), "yyyy-MM-dd HH:mm:ss");

        //Upload file result to Drive
        String fileUrl = "https://drive.google.com/open?id=" + GoogleDrive.uploadExcelFile(resultPath);
//        GoogleDrive.setExcelPermission();

        //Send message to GG Chat
        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append("*API Seller Automation - " + BaseTest.environment.toUpperCase() + " - " + suiteName + " - " + currentTime + "*\n");
        messageBuilder.append("- Passed case: " + BaseTest.totalPassCase+ "\n");
        messageBuilder.append("- Failed case: " + BaseTest.totalFailCase + "\n");
        messageBuilder.append("Detail report: " + "<" + fileUrl + "|Click here>\n");
        messageBuilder.append("=======================================================");

        com.google.api.services.chat.v1.model.Message hangoutsMessage = new com.google.api.services.chat.v1.model.Message();
        hangoutsMessage.setText(messageBuilder.toString());
        if(System.getProperty("os.name").startsWith("Linux"))
            GoogleHangouts.sendHangoutsMessage("AAAAesPLayE",hangoutsMessage);
        else
            GoogleHangouts.sendHangoutsMessage("AAAAae_3nLw", hangoutsMessage); //Use for test noti thread
    }
}
