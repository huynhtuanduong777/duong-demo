package automationLibrary.data;

import org.json.JSONObject;

public class TestCase {
	public String feature;
	public String className;
	public String methodName;
	public String testCaseId;
	public String testRunId;
	public String refs;
	public String comment;
	public JSONObject input;
	public int priority;
	public String expectedResult;
	public String testResult = "NOTRUN";

	public void setFeatureName(String feature) {
		this.feature = feature;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public void setTestRunId(String testRunId) {
		this.testRunId = testRunId;
	}

	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}

	public void setInput(JSONObject input) {
		this.input = input;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

	public void setTestResult(String result) {
		this.testResult = result;
	}

	public String getFeatureName() {
		return feature;
	}

	public String getClassName() {
		return className;
	}

	public String getMethodName() {
		return methodName;
	}

	public String getTestCaseId() {
		return testCaseId;
	}

	public String getTestRunId() {
		return testRunId;
	}

	public JSONObject getInput() {
		return input;
	}

	public int getPriority() {
		return priority;
	}

	public String getExpectedResult() {
		return expectedResult;
	}

	public String getTestResult() {
		return testResult;
	}

	public void setComment(String comment){
		this.comment = comment;
	}

	public void setRefs(String refs){
		this.refs = refs;
	}

	public String getRefs(){
		return refs;
	}

	public String getComment(){
		return comment;
	}

}
