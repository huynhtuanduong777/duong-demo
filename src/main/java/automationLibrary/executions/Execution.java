package automationLibrary.executions;

public class Execution {
	public static boolean result;

    public static void setTestFail(String log) {
        System.out.println(log);
        result = false;
    }

    public static boolean getTestResult() {
        return result;
    }
}
