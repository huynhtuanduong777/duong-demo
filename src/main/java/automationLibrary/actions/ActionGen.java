package automationLibrary.actions;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import object.BaseTest;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.TestNG;

import com.relevantcodes.extentreports.LogStatus;

import automationLibrary.data.TestRun;
import automationLibrary.data.TestSuite;
import automationLibrary.database.Database;
import automationLibrary.executions.Execution;
import automationLibrary.initiations.Configurations;
import automationLibrary.initiations.ConstantLib;
import runner.Runner;

public class ActionGen {


	/**
	 * Get the value of a key in a property file
	 **/

	public static String getPropertyFileValue(String filename, String key) {
		Properties prop = new Properties();
		InputStream input = null;
		String value = "";
		try {
			input = new FileInputStream(filename);
			prop.load(new InputStreamReader(input, Charset.forName("UTF-8")));
			value = prop.getProperty(key);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}

	/**
	 * Verify test
	 **/
	public static void verifyTest() {
		if (!Execution.result) {
			Assert.assertTrue(true);
		} else {
			Assert.assertTrue(true);
		}
	}

	/**
	 * Compare if int value are equal
	 **/
	public static void verifyValueEqual(int actualResult, int expectedResult) {
		try {
			Assert.assertEquals(expectedResult, actualResult);
		} catch (AssertionError e) {
			Execution.setTestFail("Not equal! Actual result: " + actualResult + ", expected result: " + expectedResult);
			e.printStackTrace();
		}
	}

	/**
	 * Compare if String value are equal
	 **/
	public static void verifyValueEqual(String actualResult, String expectedResult) {
		try {
			Assert.assertEquals(expectedResult, actualResult);
		} catch (AssertionError e) {
			Execution.setTestFail("Not equal! Actual result: " + actualResult + ", expected result: " + expectedResult);
			e.printStackTrace();
		}
	}

	/**
	 * Get current time with timezone offset
	 **/
	public static String getCurrentTimeByTimezoneOffset(int timezoneOffset, String format) {
		ZoneOffset offset = ZoneOffset.ofHours(timezoneOffset);
		OffsetDateTime odt = OffsetDateTime.now(offset);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		String output = odt.format(formatter);
		return output;
	}

	/**
	 * Execute GET request
	 **/

	public static HttpResponse executeGetRequest(String url, String token) {

		HttpUriRequest request = new HttpGet(url);
		HttpResponse httpResponse = null;

		List<String[]> headers = getRequestHeader(token);
		for (int i = 0; i < headers.size(); i++) {
			String key = headers.get(i)[0];
			String value = headers.get(i)[1];
			request.setHeader(key, value);
		}

		try {
			httpResponse = HttpClientBuilder.create().build().execute(request);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return httpResponse;
	}

	/**
	 * Execute POST request
	 **/
	public static HttpResponse executePostRequest(String url, JSONObject jsonObject, String token) {
		HttpUriRequest request = new HttpPost(url);
		HttpEntity entity = null;
		HttpResponse httpResponse = null;
		try {
			entity = new StringEntity(jsonObject.toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		((HttpPost) request).setEntity(entity);

		List<String[]> headers = getRequestHeader(token);
		for (int i = 0; i < headers.size(); i++) {
			String key = headers.get(i)[0];
			String value = headers.get(i)[1];
			request.setHeader(key, value);
		}

		try {
			httpResponse = HttpClientBuilder.create().build().execute(request);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return httpResponse;
	}

	/**
	 * Execute PUT request
	 **/
	public static HttpResponse executePutRequest(String url, JSONObject jsonObject, String token) {
		HttpUriRequest request = new HttpPut(url);
		HttpEntity entity = null;
		HttpResponse httpResponse = null;
		try {
			entity = new StringEntity(jsonObject.toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		((HttpPut) request).setEntity(entity);

		List<String[]> headers = getRequestHeader(token);
		for (int i = 0; i < headers.size(); i++) {
			String key = headers.get(i)[0];
			String value = headers.get(i)[1];
			request.setHeader(key, value);
		}

		try {
			httpResponse = HttpClientBuilder.create().build().execute(request);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return httpResponse;
	}

	/**
	 * Execute DEL request
	 **/
	public static HttpResponse executeDelRequest(String url, String token) {
		HttpUriRequest request = new HttpDelete(url);

		HttpResponse httpResponse = null;

		List<String[]> headers = getRequestHeader(token);
		for (int i = 0; i < headers.size(); i++) {
			String key = headers.get(i)[0];
			String value = headers.get(i)[1];
			request.setHeader(key, value);
		}

		try {
			httpResponse = HttpClientBuilder.create().build().execute(request);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return httpResponse;
	}

	public static String getStringResponse(HttpResponse response) {
		String body = null;
		try {
			body = EntityUtils.toString(response.getEntity());

		} catch (IOException e) {
			e.printStackTrace();
		}

		return body;
	}

	public static String verifyResult(String expectedResult, HttpResponse response) {
		boolean result = true;

		if (response == null)
			result = false;
		else {
			String actual_result = getStringResponse(response);

			if (actual_result == null)
				result = false;
			else {
				String[] list = expectedResult.split("<br>");
				for (String s : list) {
					if (s.contains("CODE")) {
						int actual_status = response.getStatusLine().getStatusCode();
						String expected_status = s.replace("CODE:", "").trim();
						if (!expected_status.contains(String.valueOf(actual_status)))
							result = false;

					} else {
						if (s.contains("NOTCONTAIN")) {
							if (match(s.replace("NOTCONTAIN:", ""), actual_result))
								result = false;
						} else {
							if (!match(s, actual_result))
								result = false;
						}
					}
				}
			}
		}
		if (result)
			return "PASSED";
		else
			return "FAILED";
	}

	public static boolean match(String regex, String input) {
		String[] lst = regex.split("\\*");
		String regex_ = "";
		for (String s : lst)
			regex_ = regex_ + "(" + s + ")(.*?)";

		regex_ = regex_.replace("{", "\\{");
		regex_ = regex_.replace("}", "\\}");
		regex_ = regex_.replace("[", "\\[");
		regex_ = regex_.replace("]", "\\]");
		regex_ = regex_.replace("\"/", "\"\\\\\\/");
		regex_ = regex_.replace("/\"", "\\\\\\/");

		System.out.println("regex : " + regex_);

		Pattern pattern = Pattern.compile(regex_);
		Matcher matcher = pattern.matcher(input);

		if (matcher.find())
			return true;
		else
			return false;
	}

	public static String verifyKeyValue(JSONObject actualResult, JSONObject expectedResult) {
		String result = "PASSED";
		Iterator<String> keys = expectedResult.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (!key.contains("verify_key")) {
				List<String> actual_value = getValueOfJson(actualResult, key);
				List<String> expected_value = Arrays.asList(expectedResult.get(key).toString().split(";"));
				if (actual_value.size() < expected_value.size())
					result = "FAILED";
				else {
					for (String a : expected_value) {
						if (!actual_value.contains(a))
							result = "FAILED";
					}
				}
			}
		}
		System.out.println(result);
		return result;
	}

	public static List<String> getValueOfJson(JSONObject input, String _key) {
		List<String> value = new ArrayList<>();
		Iterator<String> keys = input.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (input.get(key) instanceof JSONObject) {
				JSONObject object = input.getJSONObject(key);
				value.addAll(getValueOfJson(object, _key));

			} else {
				if (input.get(key) instanceof JSONArray) {
					JSONArray array = (JSONArray) input.get(key);
					for (int i = 0; i < array.length(); i++) {
						try {
							JSONObject object = (JSONObject) array.get(i);
							value.addAll(getValueOfJson(object, _key));
						} catch (ClassCastException e) {

						}

					}
				} else {
					if (_key.contains(key) && key.contains(_key))
						value.add(input.get(key).toString());
				}

			}
		}
		return value;
	}

	public static List<String[]> getRequestHeader(String accessToken) {
		List<String[]> headers = new ArrayList<String[]>();
		String accept[] = { "Accept", "application/json" };
		String contentType[] = { "Content-type", "application/json" };
		headers.add(accept);
		headers.add(contentType);
		if (accessToken != null) {
			String authorization[] = { "Authorization", accessToken };
			headers.add(authorization);
		}
		return headers;
	}

	public static String randomAlphaNumeric(int count) {
		String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	public static JSONArray getJsonData(String dataFilePath) {
		String jsonData = "";
		BufferedReader br = null;
		try {
			String line;
			br = new BufferedReader(new InputStreamReader(new FileInputStream(dataFilePath), "UTF-8"));
			while ((line = br.readLine()) != null) {
				jsonData += line + "\n";
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		JSONArray obj = new JSONArray(jsonData);
		return obj;
	}

	public static String getTestDataConfigValue(String keyValue) {
		keyValue = keyValue.replace("${", "").replace("}", "");
		System.out.println(keyValue);
		String keyValueSplit[] = keyValue.split("\\.");
		String featureName = keyValueSplit[0];
		String value = keyValueSplit[1];
		JSONObject dataObject = getJsonData(Configurations.jsonFile).getJSONObject(0);
		if (BaseTest.environment.equals(ConstantLib.Environment.PRODUCTION)) {
			JSONObject featureObject = dataObject.getJSONObject(ConstantLib.Environment.PRODUCTION);
			JSONObject valueObject = featureObject.getJSONObject(featureName);
			return valueObject.get(value).toString();
		} else if (BaseTest.environment.equals(ConstantLib.Environment.STAGING)) {
			JSONObject featureObject = dataObject.getJSONObject(ConstantLib.Environment.STAGING);
			JSONObject valueObject = featureObject.getJSONObject(featureName);
			return valueObject.get(value).toString();
		} else if (BaseTest.environment.equals(ConstantLib.Environment.PILOT)) {
			JSONObject featureObject = dataObject.getJSONObject(ConstantLib.Environment.PILOT);
			JSONObject valueObject = featureObject.getJSONObject(featureName);
			return valueObject.get(value).toString();
		} else {
			JSONObject featureObject = dataObject.getJSONObject(ConstantLib.Environment.TEST);
			JSONObject valueObject = featureObject.getJSONObject(featureName);
			return valueObject.get(value).toString();
		}
	}

	public static Object getExpectedResultConfigValue(String keyValue) {
		keyValue = keyValue.replace("${", "").replace("}", "");
		String keyValueSplit[] = keyValue.split("\\.");
		String featureName = keyValueSplit[0];
		String value = keyValueSplit[1];
		JSONObject dataObject = getJsonData(Configurations.jsonFile).getJSONObject(0);
		if (BaseTest.environment.equals(ConstantLib.Environment.PRODUCTION)) {
			JSONObject featureObject = dataObject.getJSONObject(ConstantLib.Environment.PRODUCTION);
			JSONObject valueObject = featureObject.getJSONObject(featureName);
			return valueObject.get(value);
		} else if (BaseTest.environment.equals(ConstantLib.Environment.STAGING)) {
			JSONObject featureObject = dataObject.getJSONObject(ConstantLib.Environment.STAGING);
			JSONObject valueObject = featureObject.getJSONObject(featureName);
			return valueObject.get(value);
		} else if (BaseTest.environment.equals(ConstantLib.Environment.PILOT)) {
			JSONObject featureObject = dataObject.getJSONObject(ConstantLib.Environment.PILOT);
			JSONObject valueObject = featureObject.getJSONObject(featureName);
			return valueObject.get(value);
		} else {
			JSONObject featureObject = dataObject.getJSONObject(ConstantLib.Environment.TEST);
			JSONObject valueObject = featureObject.getJSONObject(featureName);
			return valueObject.get(value);
		}
	}

	public static String getFinalizedExpectedResult(String expectedResult) {
		String temp[] = expectedResult.split(",");
		for (int i = 0; i < temp.length; i++) {
			if (temp[i].contains("${")) {
				String key = temp[i].substring(temp[i].indexOf("${") + 2, temp[i].indexOf("}"));
				Object value = getExpectedResultConfigValue(key);
				key = "${" + key + "}";
				if (value instanceof Integer || value instanceof Boolean) {
					key = "\"" + key + "\"";
				}
				expectedResult = expectedResult.replace(key, value.toString());
			}
		}
		return expectedResult;
	}
	
	public static List<TestRun> getTestRunList(String testPlanId) {
		String selectTestRunQuery = "SELECT * FROM testruns WHERE testruns.testPlanId = '" + testPlanId + "'";
		ResultSet testRunResultSet = Database.select(selectTestRunQuery);
		List<TestRun> testRunList = new ArrayList<TestRun>();
		try {
			while (testRunResultSet.next()) {
				TestRun testRun = new TestRun();
				testRun.setTestRunId(testRunResultSet.getInt("id"));
				testRun.setTestRunName(testRunResultSet.getString("testRun"));
				testRunList.add(testRun);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return testRunList;
	}
	
	public static List<TestSuite> getTestSuiteList(List<TestRun> testRunList) {
		List<TestSuite> testSuiteList = new ArrayList<TestSuite>();
		for(int i = 0; i < testRunList.size(); i++) {
			int testRunId = testRunList.get(i).testRunId;
			String selectTestSuiteQuery = "SELECT * FROM testcases JOIN testexecutions ON testcases.id = testexecutions.testCaseID JOIN testsuites ON testsuites.id = testcases.testSuiteID JOIN features ON features.id = testsuites.featureID WHERE testexecutions.testRunID = '" + testRunId + "' GROUP BY testsuites.id";
			ResultSet testSuiteResultSet = Database.select(selectTestSuiteQuery);
			try {
				while (testSuiteResultSet.next()) {
					TestSuite testSuite = new TestSuite();
					testSuite.setTestRunId(testSuiteResultSet.getInt("testRunID"));
					testSuite.setProductId(testSuiteResultSet.getInt("productID"));
					testSuite.setFeatureId(testSuiteResultSet.getInt("featureID"));
					testSuite.setTestSuiteName(testSuiteResultSet.getString("testSuite"));
					testSuiteList.add(testSuite);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return testSuiteList;
	}

	public static void executeTestSuiteList(List<TestSuite> testSuiteList) {
		for (int i = 0; i < testSuiteList.size(); i++) {
			int testRunId = testSuiteList.get(i).getTestRunId();
			int productId = testSuiteList.get(i).getProductId();
			int featureId = testSuiteList.get(i).getFeatureId();
			String testSuiteName = testSuiteList.get(i).getTestSuiteName();
			System.out.println("Test Run ID: " + testRunId);
			System.out.println("Test Suite Name: " + testSuiteName);
			String classPath = null;
			String featureName = null;
			String className = null;
			String methodName = null;
			String packageName = null;

			if (productId == ConstantLib.Product.BUYER) {
				if (featureId == ConstantLib.Feature.Buyer.CED) {
					featureName = "CED";
					classPath = "buyer.ced." + testSuiteName;
				} else if (featureId == ConstantLib.Feature.Buyer.PAYMENT) {
					featureName = "Payment";
					classPath = "buyer.payment." + testSuiteName;
				} else if (featureId == ConstantLib.Feature.Buyer.ACCOUNT) {
					featureName = "Account";
					classPath = "buyer.account." + testSuiteName;
				} else if (featureId == ConstantLib.Feature.Buyer.COMMUNICATION) {
					featureName = "Communication";
					classPath = "buyer.communication." + testSuiteName;
				} else if (featureId == ConstantLib.Feature.Buyer.PRODUCT_DISCOVERY) {
					featureName = "Product Discovery";
					classPath = "buyer.productDiscovery." + testSuiteName;
				}
			} else if (productId == ConstantLib.Product.SELLER) {
				if (featureId == ConstantLib.Feature.Seller.ORDER) {
					featureName = "Order";
					packageName = "order";
					classPath = "order." + "TC_" + testSuiteName;
				} else if (featureId == ConstantLib.Feature.Seller.SHOP) {
					featureName = "Shop";
					packageName = "shop";
					classPath = "shop." + "TC_" + testSuiteName;
				} else if (featureId == ConstantLib.Feature.Seller.SHOP_CONFIG) {
					featureName = "Shop Config";
					packageName = "shopconfig";
					classPath = "shopconfig." + "TC_" + testSuiteName;
				} else if (featureId == ConstantLib.Feature.Seller.SKU) {
					featureName = "SKU";
					packageName = "sku";
					classPath = "sku." + "TC_" + testSuiteName;
				}
			}

			className = "TC_" + testSuiteName;
			methodName = testSuiteName.substring(0, 1).toLowerCase() + testSuiteName.substring(1, testSuiteName.length());

//			TestNG execution = new TestNG();
//			Class<?>[] executionClasses = new Class<?>[1];
//			try {
//				Class<?> executionClass = Class.forName(classPath);
//				executionClasses[0] = executionClass;
//			} catch (ClassNotFoundException e) {
//				e.printStackTrace();
//			}
//			execution.setTestClasses(executionClasses);
			TestNG execution = new TestNG();
			String testngSuite = System.getProperty("user.dir")+"/testSuites/seller/" + packageName + "/" + testSuiteName + ".xml";
			execution.setTestSuites(Arrays.asList(testngSuite));
			Database.getConnection(Configurations.databaseUrl26, Configurations.databaseUsername, Configurations.databasePassword);
			Database.useDatabase(Configurations.databaseName);
			Configurations.initTestData(String.valueOf(testRunId), testSuiteName, featureName, className, methodName);
			execution.run();
		}
	}
	
	public static void countPriorityResult(String testResult, int priority) {
		if(testResult.equals("PASSED")) {
			switch(priority) {
			case 1:
				Runner.passedP1++;
				break;
			case 2:
				Runner.passedP2++;
				break;
			case 3:
				Runner.passedP3++;
				break;
			case 4:
				Runner.passedP4++;
				break;
			}
		} else {
			switch(priority) {
			case 1:
				Runner.failedP1++;
				break;
			case 2:
				Runner.failedP2++;
				break;
			case 3:
				Runner.failedP3++;
				break;
			case 4:
				Runner.failedP4++;
				break;
			}
		}
	}

	public static void notifyResult(String featureName, String environment, int totalTestSuite, int totalTestCase, String reportUrl, double maxFailedP1Percent, double maxFailedP2Percent, double maxFailedP3Percent, double maxFailedP4Percent) {
		double failedP1Percent = 0.0;
		double failedP2Percent = 0.0;
		double failedP3Percent = 0.0;
		double failedP4Percent = 0.0;
		if(Runner.failedP1 > 0) {
			failedP1Percent = (double) Runner.failedP1 / (double) (Runner.failedP1 + Runner.passedP1) * 100;
		}

		if(Runner.failedP2 > 0) {
			failedP2Percent = (double) Runner.failedP2 / (double) (Runner.failedP2 + Runner.passedP2) * 100;
		}

		if(Runner.failedP3 > 0) {
			failedP3Percent = (double) Runner.failedP3 / (double) (Runner.failedP3 + Runner.passedP3) * 100;
		}

		if(Runner.failedP4 > 0) {
			failedP4Percent = (double) Runner.failedP4 / (double) (Runner.failedP4 + Runner.passedP4) * 100;
		}

		String datetime = getCurrentTimeByTimezoneOffset(Configurations.timezoneOffset, "dd/MM/yyyy HH:mm");
		environment = environment.replace("API - ", "").trim();
		String mailTitle = "API Automation" + " - " + featureName + " - " + environment + " - " + datetime;

		StringBuilder emailMessage = new StringBuilder();
		emailMessage.append("<p>Hi all,</p>");
		emailMessage.append("<p>This is the result for API Automation Report:</p>");
		emailMessage.append("<p><b>Total test suites: " + totalTestSuite + ", total test cases: " + totalTestCase + "</b></p>");
		emailMessage.append("<table style=\"width:50%; table-layout: fixed; background-color:#e8feff; border-collapse: collapse;\" border=\"1\"><tr><th>PRIORITY</th><th>FAILED PERCENT</th></tr>");

		if (failedP1Percent >= maxFailedP1Percent) {
			System.out.println("Notify for P1, also show other priority");
			emailMessage.append("<tr><td align='center'><b>P1</b></td><td align='center'><b color='red'>" + failedP1Percent + "%</b></td></tr>");
			emailMessage.append("<tr><td align='center'>P2</td><td align='center'>" + failedP2Percent + "%</td></tr>");
			emailMessage.append("<tr><td align='center'>P3</td><td align='center'>" + failedP3Percent + "%</td></tr>");
			emailMessage.append("<tr><td align='center'>P4</td><td align='center'>" + failedP4Percent + "%</td></tr>");
		} else {
			if (failedP2Percent >= maxFailedP2Percent) {
				System.out.println("Notify for P2, also show other priority");
				emailMessage.append("<tr><td align='center'>P1</td><td align='center'>" + failedP1Percent + "%</td></tr>");
				emailMessage.append("<tr><td align='center'><b>P2</b></td><td align='center'><b color='red'>" + failedP2Percent + "%</b></td></tr>");
				emailMessage.append("<tr><td align='center'>P3</td><td align='center'>" + failedP3Percent + "%</td></tr>");
				emailMessage.append("<tr><td align='center'>P4</td><td align='center'>" + failedP4Percent + "%</td></tr>");
			} else {
				if (failedP3Percent >= maxFailedP3Percent) {
					System.out.println("Notify for P3, also show other priority");
					emailMessage.append("<tr><td align='center'>P1</td><td align='center'>" + failedP1Percent + "%</td></tr>");
					emailMessage.append("<tr><td align='center'>P2</td><td align='center'>" + failedP2Percent + "%</td></tr>");
					emailMessage.append("<tr><td align='center'><b>P3</b></td><td align='center'><b color='red'>" + failedP3Percent + "%</b></td></tr>");
					emailMessage.append("<tr><td align='center'>P4</td><td align='center'>" + failedP4Percent + "%</td></tr>");
				} else {
					if (failedP4Percent >= maxFailedP4Percent) {
						System.out.println("Notify for P4, also show other priority");
						emailMessage.append("<tr><td align='center'>P1</td><td align='center'>" + failedP1Percent + "%</td></tr>");
						emailMessage.append("<tr><td align='center'>P2</td><td align='center'>" + failedP2Percent + "%</td></tr>");
						emailMessage.append("<tr><td align='center'>P3</td><td align='center'>" + failedP3Percent + "%</td></tr>");
						emailMessage.append("<tr><td align='center'><b>P4</b></td><td align='center'><b color='red'>" + failedP4Percent + "%</b></td></tr>");
					}
				}
			}
		}
		emailMessage.append("</table>");
		emailMessage.append("<p>Detail report: <a href='" + reportUrl + "'>Click here</a></p>");
		emailMessage.append("<br>");
		emailMessage.append("<p>Thanks,</p>");
		emailMessage.append("<p>QC Team</p>");
		sendMail(mailTitle, emailMessage.toString(), null);
	}

	public static void sendMail(String subject, String mailMessage, List<String> files) {
		String username = Configurations.userMail;
		String password = Configurations.passwordMail;

		Properties props = new Properties();
		//props.put("proxySet", true);
		//props.setProperty("socksProxyHost","111.65.244.252");
		//props.setProperty("socksProxyPort","80");
		props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "465");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		String[] recipients = Configurations.recipientMail.split(",");
		InternetAddress[] address = new InternetAddress[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			try {
				address[i] = new InternetAddress(recipients[i]);
			} catch (AddressException e) {
				e.printStackTrace();
			}
		}

		try {
			BodyPart messageBodyPart = new MimeBodyPart();
			Multipart multipart = new MimeMultipart();
			messageBodyPart.setContent(mailMessage, "text/html; charset=utf-8");
			multipart.addBodyPart(messageBodyPart);
			if(files != null) {
				for(int i = 0; i < files.size(); i++) {
					messageBodyPart = new MimeBodyPart();
					DataSource source = new FileDataSource(files.get(i));
					messageBodyPart.setDataHandler(new DataHandler(source));
					messageBodyPart.setFileName(files.get(i));
					multipart.addBodyPart(messageBodyPart);
				}
			}

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Configurations.userMail));
			message.setRecipients(Message.RecipientType.TO, address);
			message.setSubject(subject);
			//message.setContent(mailMessage, "text/html; charset=utf-8");
			message.setContent(multipart);
			System.out.println("Sending mail...");
			Transport.send(message);
			System.out.println("Sent successfully!!!");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public static void addReportLog(LogStatus logStatus, String text) {
		System.out.println(text);
		//Runner.test.log(logStatus, text);
	}
	
	public static boolean checkWordExistence(String word, String sentence) {
	    if (sentence.contains(word)) {
	        int start = sentence.indexOf(word);
	        int end = start + word.length();

	        boolean valid_left = ((start == 0) || (sentence.charAt(start - 1) == ' '));
	        boolean valid_right = ((end == sentence.length()) || (sentence.charAt(end) == ' '));

	        return valid_left && valid_right;
	    }
	    return false;
	}
}
