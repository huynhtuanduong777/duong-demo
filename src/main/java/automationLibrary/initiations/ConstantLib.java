package automationLibrary.initiations;

public class ConstantLib {

	public static class SpecialCharacter {
		public static final String COMMA_WITH_SPACE = " , ";
		public static final String FORWARD_SLASH = "/";
	}
	
	public static class Environment {
		public static final String PRODUCTION = "production";
		public static final String STAGING = "staging";
		public static final String PILOT = "pilot";
		public static final String TEST = "test";
	}
	
	public static class Priority {
		public static final int P1 = 1;
		public static final int P2 = 2;
		public static final int P3 = 3;
		public static final int P4 = 4;
	}
	
	public static class Product {
		public static final int BUYER = 10;
		public static final int SELLER = 12;
	}
	
	public static class Feature {
		public static class Buyer {
			public static final int PAYMENT = 349;
			public static final int CED = 360;
			public static final int PRODUCT_DISCOVERY = 359;
			public static final int COMMUNICATION = 353;
			public static final int ACCOUNT = 348;
		}
		
		public static class Seller {
			public static final int ORDER = 356;
			public static final int SHOP = 357;
			public static final int SHOP_CONFIG = 354;
			public static final int SKU = 352;
		}
	}
}
