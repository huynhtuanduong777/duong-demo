package runner;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.ITestContext;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import automationLibrary.actions.ActionGen;
import automationLibrary.data.Data;
import automationLibrary.data.TestRun;
import automationLibrary.data.TestSuite;
import automationLibrary.database.Database;
import automationLibrary.initiations.Configurations;
import automationLibrary.initiations.ConstantLib;
import object.BaseTest;
import utils.Constants;
import utils.EmailSender;
import utils.FormatUtils;
import utils.GoogleDrive;
import utils.GoogleHangouts;


public class Runner {
	public static int passedP1 = 0;
	public static int failedP1 = 0;
	public static int passedP2 = 0;
	public static int failedP2 = 0;
	public static int passedP3 = 0;
	public static int failedP3 = 0;
	public static int passedP4 = 0;
	public static int failedP4 = 0;
	public static String environment;
	public static int passedSuite = 0;
	public static int failedSuite = 0;
	public static List<String> realtimeTableColumns = Arrays.asList("testSuite", "Passed", "Failed", "ExecutionTime", "environment");
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String extentReportUrl;
	public static boolean checkFailSheet = false;
	public static int totalFailSheet = 0;
	public static int totalPassSheet = 0;
	public static void run() {
		BaseTest.isLocal = false;
		environment = System.getProperty("environment");
		if (environment == null) {
			environment = Configurations.environment;
		}
		//Init URL
		
		//Connect DB and get testsuite list
		String executionTime = ActionGen.getCurrentTimeByTimezoneOffset(Integer.valueOf(Configurations.timezoneOffset), "MM-dd HH:mm:ss");
		String environment = "API - " + getEnvironment().substring(0, 1).toUpperCase() + getEnvironment().substring(1,  getEnvironment().length());
		Database.getConnection(Configurations.databaseUrl26, Configurations.databaseUsername, Configurations.databasePassword);
		Database.useDatabase(Configurations.databaseName);
		String testPlanId = System.getProperty("testPlanId");
		List<TestRun> testRunList = ActionGen.getTestRunList(testPlanId);
		List<TestSuite> testSuiteList = ActionGen.getTestSuiteList(testRunList);
		
		//Generate Extent Report
		String featureName = testSuiteList.get(0).getFeatureName();
		String reportDateTime = ActionGen.getCurrentTimeByTimezoneOffset(Configurations.timezoneOffset, "dd-MM-yyyy HH.mm.ss");
		String extentReportPath = Configurations.reportPath + featureName + " - " + reportDateTime + ".html";
		extentReportUrl = Configurations.reportUrl + featureName + " - " + reportDateTime + ".html";
		extent = new ExtentReports(extentReportPath, true);
		extent.loadConfig(new File(System.getProperty("user.dir") + "//extent-config.xml"));
		
		//Execute testsuites list
		ActionGen.executeTestSuiteList(testSuiteList);
		
		//Insert DB if running realtime
		if(BaseTest.realTimeRun) {
			List<String> result = new ArrayList<String>();
			result.add(0, Data.getFeatureName());
			result.add(1, String.valueOf(passedSuite));
			result.add(2, String.valueOf(failedSuite));
			result.add(3, executionTime);
			result.add(4, environment);
			Database.getConnection(Configurations.databaseUrl45, Configurations.databaseUsername, Configurations.databasePassword);
			Database.useDatabase(Configurations.realTimeDatabase);
			Database.insertIntoDatabase(Configurations.realTimeProductionTable, realtimeTableColumns, result);
		}
		
		extent.flush();
		int totalTestSuite = passedSuite + failedSuite;
		int totalTestCase = passedP1 + failedP1 + passedP2 + failedP2 +  passedP3 + failedP3 +  passedP4 + failedP4; 
		//ActionGen.notifyResult(featureName, environment, totalTestSuite, totalTestCase, extentReportUrl, Configurations.maxFailedP1Percentage, Configurations.maxFailedP2Percentage, Configurations.maxFailedP3Percentage, Configurations.maxFailedP4Percentage);
		Database.closeConnection();
		
		String logPath = Constants.LOG_PATH + "SellerTestLog.log";
		String logUrl = "https://drive.google.com/open?id=" + GoogleDrive.uploadLog(logPath);
		String subject = "Daily Automation API Seller - Log -  " + FormatUtils.getToday();
		String to1 = "tanhp@sendo.vn";
		String emailMessage = "Seller automation API log " + FormatUtils.getToday() + ": " + logUrl;
		EmailSender.sendNotificationMail(subject,to1,emailMessage,null);
	}
	
	public static String getEnvironment() {
		return environment;
	}
	
	public static boolean getStatusSheet() {
		return checkFailSheet;
	}
	
	public static void setStatusSheet(boolean value) {
		checkFailSheet=value;
	}

	public static int getTotalFailSheet() {
		return totalFailSheet;
	}
	
	public static void setTotalFailSheet() {	
		totalFailSheet=totalFailSheet+1;
	}
	
	public static int getTotalPassSheet() {
		return totalPassSheet;
	}
	
	public static void setTotalPassSheet() {	
		totalPassSheet=totalPassSheet+1;
	}

}
